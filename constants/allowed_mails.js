const ALLOWED_MAILS = [
	"gmail.com",
	"yahoo.com",
	"hotmail.com",
	"aol.com",
	"msn.com",
	"rediffmail.com",
	"ymail.com",
	"outlook.com",
	"icloud.com",
];

module.exports = ALLOWED_MAILS;
