const companyAuth = {
	rolePrefix: "Company.",
	permissionPrefix: "company.",
	rolePermissions: [
		{
			role: "Company.Owner",
			permissions: [
				"company.jobs",
				"company.jobs-widget",
				"company.profile",
				"company.billing-and-legals",
				"company.manage-vendors",
				"company.manage-referrals",
				"company.can-take-interview",
				"company.manage-users",
				"company.manage-email-reports",
				"company.manage-opening-groups",
				"company.manage-whitelisted-domains",
			],
		},
		{
			role: "Company.Jobs-Manager",
			permissions: [
				"company.jobs"
			],
		},
		{
			role: "Company.Share-Jobs-Manager",
			permissions: [
				"company.jobs-widget"
			],
		},
		{
			role: "Company.Profile-Manager",
			permissions: [
				"company.profile"
			],
		},
		{
			role: "Company.Billing-Manager",
			permissions: [
				"company.billing-and-legals",
				"company.manage-transactions",
			],
		},
		{
			role: "Company.Vendors-Manager",
			permissions: [
				"company.manage-vendors"
			],
		},
		{
			role: "Company.Referral-Manager",
			permissions: [
				"company.manage-referrals"
			],
		},
		{
			role: "Company.Interviewer",
			permissions: [
				"company.can-take-interview"
			],
		},
		{
			role: "Company.User-Manager",
			permissions: [
				"company.manage-users"
			],
		},
		{
			role: "Company.Email-Report-Manager",
			permissions: [
				"company.manage-email-reports"
			],
		},
		{
			role: "Company.JD-Group-Manager",
			permissions: [
				"company.manage-opening-groups"
			],
		},
		{
			role: "Company.Domain-Manager",
			permissions: [
				"company.manage-whitelisted-domains"
			]
		},
	],
};

const openingAuth = {
	rolePrefix: "Opening.",
	permissionPrefix: "opening.",
	rolePermissions: [
		{
			role: "Opening.JD-Owner",
			permissions: [
				"opening.manage-jd"
			],
		},
		{
			role: "Opening.Pending-Candidate-Approver",
			permissions: [
				"opening.can-approve-pending-candidate"
			],
		},
		{
			role: "Opening.Candidate-Interview-Withdrawer",
			permissions: [
				"opening.can-mark-as-withdrawn"
			],
		},
		{
			role: "Opening.Reviewer",
			permissions: [
				"opening.can-rate-interview"
			],
		},
		{
			role: "Opening.Interviewer",
			permissions: [
				"opening.can-take-interview"
			],
		},
		{
			role: "Opening.Interview-Coordinator",
			permissions: [
				"opening.can-schedule-interview"
			],
		},
		{
			role: "Opening.Email-Report-Manager",
			permissions: [
				"opening.manage-email-reports"
			],
		},
		{
			role: "Opening.Vendors-Manager",
			permissions: [
				"opening.manage-vendors",
			],
		},
		{
			role: "Opening.Reinvite-Manager",
			permissions: [
				"opening.can-reinvite-for-interview",
			],
		},
		{
			role: "Opening.Invite-Manager",
			permissions: [
				"opening.can-invite-for-interview",
			],
		},
	],
};

module.exports = { companyAuth, openingAuth };