const AUTH_DB_KEYS = {
	EMAIL: "email",
	PHONE: "phone",
	PASSWORD: "password",
	OTP: "otp",
	COUNTRY_PHONE_CODE_ID: "country_phone_code_id"
};

module.exports = AUTH_DB_KEYS;
