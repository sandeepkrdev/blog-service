const COMMON_DB_KEYS = {
	CREATED_AT: "createdAt",
	CREATED_BY: "created_by",
	DATE: "date",
	EXPIRY: "expiry",
	ID: "id",
	NAME: "name",
	REF_ID: "ref_id",
	REF_TYPE: "ref_type",
	SERVICE_ACTION: "service_action",
	SERVICE_MODEL_ID: "service_model_id",
	STATUS: "status",
	TYPE: "type",
	UPDATED_AT: "updatedAt",
	USER_ID: "user_id"
};

module.exports = COMMON_DB_KEYS;
