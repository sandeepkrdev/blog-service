const AUTH = require("./auth");
const COMMON = require("./common");
const USERS = require("./users");

module.exports = {
	AUTH,
	COMMON,
	USERS
};
