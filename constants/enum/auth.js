const AUTH_ENUM = {
	REF_TYPE: {
		AUTH_EMAIL: "auth_email",
		AUTH_PHONE: "auth_phone",
		OTP: "otp"
	}
};

module.exports = AUTH_ENUM;
