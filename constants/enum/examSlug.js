const EXAM_SLUG_ENUM = {
	TECH_CATEGORY: {
		value: "tech_category",
		url: "/"
	},
	TECHNOLOGY: {
		value: "technology",
		url: "/technology/"
	},
	EXAM: {
		value: "exam",
		url: "/certification/"
	}
};

module.exports = EXAM_SLUG_ENUM;
