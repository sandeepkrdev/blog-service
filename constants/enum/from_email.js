const FROM_EMAIL = {
	WELCOME: "welcome",
	SUPPORT: "support",
	INVITE: "invite",
	INTERVIEW: "interview",
	NOREPLY: "noreply",
	REPORTS: "reports",
};

module.exports = FROM_EMAIL;
