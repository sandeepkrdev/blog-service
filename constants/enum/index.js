const AUTH = require("./auth");
const SLUG = require("./slug");
const EXAM_SLUG = require("./examSlug");
const FROM_EMAIL = require("./from_email");
const INDEXING = require("./indexing");

module.exports = {
	AUTH,
	SLUG,
	EXAM_SLUG,
	FROM_EMAIL,
	INDEXING
};
