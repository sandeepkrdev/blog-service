const SLUG_ENUM = {
	CANDIDATE: {
		value: "candidate",
		url: "/profile/"
	},
	COMPANY: {
		value: "company",
		url: "/company/"
	},
	OPENING: {
		value: "opening",
		url: "/openings/"
	},
	SKILL_GROUP: {
		value: "skill_group",
		url: "/"
	}
};

module.exports = SLUG_ENUM;
