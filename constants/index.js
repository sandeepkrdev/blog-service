const MAIL = require("./mail");
const PDF = require("./pdf");
const ENUM = require("./enum");
const MODELS = require("./models");
const DB_KEYS = require("./db_keys");
const SERVICES = require("./services");
const MISCELLANEOUS = require("./misc");
const SETTING = require("./setting");
const ACTIONS = require("./actions");
const WHATSAPP = require("./whatsapp");
const RCHILLI_SCORE_TYPES = require("./rchilli_score_types");
const INTERVIEW_STATUS = require("./interview_status");
const REJECTED_REASONS = require("./rejected_reasons");

module.exports = {
	MAIL,
	ENUM,
	MODELS,
	DB_KEYS,
	SERVICES,
	MISCELLANEOUS,
	SETTING,
	ACTIONS,
	PDF,
	WHATSAPP,
	RCHILLI_SCORE_TYPES,
	INTERVIEW_STATUS,
	REJECTED_REASONS,
};
