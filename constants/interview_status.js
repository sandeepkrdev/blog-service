const INTERVIEW_STATUS = {
	INPROGRESS: "Inprogress",
	WITHDRAWN: "Withdrawn"
};

module.exports = INTERVIEW_STATUS;
