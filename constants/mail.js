/**
 * Template names from the mail-templates folder.
 */
const { config } = require(process.cwd() + "/utils/env");

const TEMPLATE = {
	WELCOME: "welcome.template.handlebars",
	OPPORTUNITY: "opportunity.template.handlebars",
	APPLICATION_RECEIVED_FOR_JD: "applicationReceivedForJD.template.handlebars",
	INTERVIEW_INVITATION: "interviewInvitation.template.handlebars",
	INTERVIEW_ASSIGNMENT_INVITATION: "interviewAssignmentInvitation.template.handlebars",
	INTERVIEW_SHORTLISTED_NEXT_ROUND: "interviewShortlistedNextRound.template.handlebars",
	INTERVIEW_SHORTLISTED_ASSIGNMENT_ROUND: "interviewShortlistedAssignmentRound.template.handlebars",
	INTERVIEW_REJECTED: "interviewRejected.template.handlebars",
	INTERVIEW_NOT_APPROVED: "interviewNotApproved.template.handlebars",
	INTERVIEW_FAILED: "interviewFailed.template.handlebars",
	INTERVIEW_PASSED: "interviewPassed.template.handlebars",
	NEW_MEETING: "newMeeting.template.handlebars",
	CANCELLED_MEETING: "cancelledMeeting.template.handlebars",
	SAMPLE_MAIL: "sampleMail.template.handlebars",
	PENDING_INTERVIEW_REMINDER: "pendingInterviewReminder.template.handlebars",
	VENDOR_INVITATION: "vendorInvitation.template.handlebars",
	PENDING_COMPANY_APPROVAL: "pendingCompany.template.handlebars",
	PENDING_COMPANY_APPROVED: "pendingCompanyApproved.template.handlebars",
	RESET_PASSWORD: "resetPassword.template.handlebars",
	PASSWORD_RESET_SUCCESSFULLY: "passwordResetSuccessfully.template.handlebars",
	OTP: "otp.template.handlebars",
	SUMMARY_DAILY_ACTIVITY: "summaryDailyActivity.template.handlebars",
	SUMMARY_DAILY_ACTIVITY_OPENING_VENDOR: "summaryDailyActivityOpeningVendor.template.handlebars",
	SUMMARY_DAILY_ACTIVITY_COMPANY_OPENINGS: "summaryDailyActivityCompanyOpenings.template.handlebars",
	CORPORATE_CANDIDATE_INTERVIEW_INVITATION: "corporateCandidateInterviewInvitation.template.handlebars",
	SUMMARY_EXPIRED_INTERVIEW: "summaryExpiredInterview.template.handlebars",
	USER_REFERRAL_INVITATION: "userReferralInvitation.template.handlebars",
	CONFIRM_AVAILABLE_SLOT: "confirmAvailableSlot.template.handlebars",
	ARTICLE_APPROVED: "articleApproved.template.handlebars",
	ARTICLE_REJECTED: "articleRejected.template.handlebars",
	RESCHEDULE_MEETING: "rescheduleMeeting.template.handlebars",
	EXAM_CANDIDATE_INTERVIEW_INVITATION: "examCandidateInterviewInvitation.template.handlebars",
	REFERRAL_CANDIDATE_INTERVIEW_INVITE: "referralCandidateInterviewInvite.handlebars",
	ACCOUNT_DEACTIVATED: "accountDeactivated.template.handlebars",
	ACCOUNT_REACTIVATED: "accountReactivated.template.handlebars",
	USER_REWARD_CLAIM_REQUESTED: "userRewardClaimRequested.template.handlebars",
	USER_REWARD_CLAIM_APPROVED: "userRewardClaimApproved.template.handlebars",
	APPLICATION_WITHDRAWN_FOR_JD: "applicationWithdrawnForJD.template.handlebars",
	REQUEST_FOR_ADDITIONAL_JD_INFO: "requestForAdditionalJDInfo.template.handlebars",
	PENDING_REQUEST_FOR_ADDITIONAL_JD_INFO_REMINDER: "pendingRequestForAdditionalJdInfoReminder.template.handlebars",
	INCOMING_EMAIL_MISSING_RESUME: "incomingEmailMissingResume.template.handlebars",
	CUSTOM_REJECTION: "interviewRejectedCustom.template.handlebars"
};

const TITLE = {
	SAMPLE_MAIL: function() {
		return `${config("app.name")}`;
	},
	RESET_PASSWORD: function() {
		return `Reset Password - ${config("app.name")}`;
	},
	PASSWORD_RESET_SUCCESSFULLY: function() {
		return `Reset Password - ${config("app.name")}`;
	},
	WELCOME: function() {
		return `Welcome to ${config("app.name")}`;
	},
	OTP: function() {
		return `One Time Password - ${config("app.name")}`;
	},
	VENDOR_INVITATION: function() {
		return `${config("app.name")} - Invitation for Vendor Services`;
	},
	CORPORATE_CANDIDATE_INTERVIEW_INVITATION: function(openingTitle, companyName) {
		return `Interview Invitation for ${openingTitle} at ${companyName}`;
	},
	INTERVIEW_SHORTLISTED_NEXT_ROUND: function() {
		return `${config("app.name")} - Shortlisted for next round`;
	},
	INTERVIEW_SHORTLISTED_ASSIGNMENT_ROUND: function() {
		return `${config("app.name")} - Shortlisted for next round`;
	},
	INTERVIEW_REJECTED: function(openingTitle) {
		return `${config("app.name")} - ${openingTitle} Update`;
	},
	INTERVIEW_NOT_APPROVED: function(openingTitle) {
		return `${config("app.name")} - ${openingTitle} Update`;
	},
	NEW_MEETING: function() {
		return `${config("app.name")} - Meeting Invitation`;
	},
	CANCELLED_MEETING: function() {
		return `${config("app.name")} - Canceled Meeting`;
	},
	OPPORTUNITY: function() {
		return `${config("app.name")} - Opportunity Matched`;
	},
	PENDING_INTERVIEW_REMINDER: function() {
		return `${config("app.name")} - Pending Interview Reminder`;
	},
	PENDING_COMPANY_APPROVAL: function(companyName) {
		return `${config("app.name")} - Pending Approval | ` + companyName;
	},
	PENDING_COMPANY_APPROVED: function(companyName) {
		return `${config("app.name")} - Sucessful Registration | ` + companyName;
	},
	SUMMARY_DAILY_ACTIVITY: function(openingTitle) {
		return `Summary Report For ${openingTitle}`;
	},
	SUMMARY_DAILY_ACTIVITY_OPENING_VENDOR: function(openingTitle) {
		return `Summary Report For ${openingTitle}`;
	},
	SUMMARY_DAILY_ACTIVITY_COMPANY_OPENINGS: function(companyName) {
		return `Summary Report For ${companyName} Openings`;
	},
	SUMMARY_EXPIRED_INTERVIEW: function(companyName) {
		return `Summary Report Of Interviews Going To Expire For ${companyName}`;
	},
	USER_REFERRAL_INVITATION: function() {
		return `Invitation to join ${config("app.name")}`;
	},
	CONFIRM_AVAILABLE_SLOT: function() {
		return `${config("app.name")} - Meeting Time Slot Confirmation`;
	},
	EXAM_CANDIDATE_INTERVIEW_INVITATION: function(examTitle) {
		return `${config("app.name")} - Certification Exam Invitation for ${examTitle}`;
	},
	ARTICLE_APPROVED: function(articleTitle) {
		return `${config("app.name")} - ${articleTitle} Published`;
	},
	ARTICLE_REJECTED: function(articleTitle) {
		return `${config("app.name")} - ${articleTitle} Update`;
	},
	RESCHEDULE_MEETING: function() {
		return `${config("app.name")} - Rescheduled Meeting`;
	},
	INTERVIEW_FAILED: function(examTitle) {
		return `${config("app.name")} - ${examTitle} Update`;
	},
	INTERVIEW_PASSED: function(examTitle) {
		return `${config("app.name")} - ${examTitle} Certification`;
	},
	REFERRAL_CANDIDATE_INTERVIEW_INVITE: function(openingTitle, companyName) {
		return `Interview Invitation for ${openingTitle} at ${companyName}`;
	},
	ACCOUNT_DEACTIVATED: function() {
		return `${config("app.name")} - Account Deactivation`;
	},
	ACCOUNT_REACTIVATED: function() {
		return `${config("app.name")} - Account Reactivation`;
	},
	USER_REWARD_CLAIM_REQUESTED: function() {
		return `${config("app.name")} reward claim`;
	},
	USER_REWARD_CLAIM_APPROVED: function() {
		return `${config("app.name")} reward claimed`;
	},
	APPLICATION_RECEIVED_FOR_JD: function(openingTitle, companyName) {
		return `Thank you for applying for ${openingTitle} at ${companyName}`;
	},
	INTERVIEW_INVITATION: function(openingTitle, companyName) {
		return `Interview Invitation for ${openingTitle} at ${companyName}`;
	},
	INTERVIEW_ASSIGNMENT_INVITATION: function(openingTitle, companyName) {
		return `Interview Invitation for ${openingTitle} at ${companyName}`;
	},
	APPLICATION_WITHDRAWN_FOR_JD: function(openingTitle, companyName) {
		return `Application Withdrawn from ${openingTitle} at ${companyName}`;
	},
	REQUEST_FOR_ADDITIONAL_JD_INFO: function() {
		return "Additional Information Required";
	},
	PENDING_REQUEST_FOR_ADDITIONAL_JD_INFO_REMINDER: function() {
		return "Additional Information Required";
	},
	INCOMING_EMAIL_MISSING_RESUME: function(openingTitle, companyName) {
		return `Share your Resume for ${openingTitle} at ${companyName}`;
	},
	CUSTOM_REJECTION: function(openingTitle) {
		return `${config("app.name")} - ${openingTitle} Update`;
	}
};

const SUBJECT = {
	SAMPLE_MAIL: function() {
		return "Sample Mail";
	},
	RESET_PASSWORD: function() {
		return "Reset Password";
	},
	PASSWORD_RESET_SUCCESSFULLY: function() {
		return "Password Reset Successfully";
	},
	WELCOME: function() {
		return `Welcome to ${config("app.website")}!`;
	},
	OTP: function() {
		return `One Time Password - ${config("app.website")}`;
	},
	VENDOR_INVITATION: function(companyName) {
		return `Invitation for Vendor Services at ${companyName}`;
	},
	CORPORATE_CANDIDATE_INTERVIEW_INVITATION: function(openingTitle, companyName) {
		return `Interview Invitation for ${openingTitle} at ${companyName}`;
	},
	INTERVIEW_SHORTLISTED_NEXT_ROUND: function(openingTitle, companyName) {
		return `Invitation for the next round for ${openingTitle} at ${companyName}`;
	},
	INTERVIEW_SHORTLISTED_ASSIGNMENT_ROUND: function(openingTitle, companyName) {
		return `Invitation for the next round for ${openingTitle} at ${companyName}`;
	},
	INTERVIEW_REJECTED: function(openingTitle, companyName) {
		return `Application status for the position of ${openingTitle}, ${companyName}`;
	},
	INTERVIEW_NOT_APPROVED: function(openingTitle, companyName) {
		return `Application status for the position of ${openingTitle}, ${companyName}`;
	},
	NEW_MEETING: function(openingTitle, roundName) {
		return `${config("app.name")} - Meeting Invitation for ${openingTitle} for ${roundName}`;
	},
	CANCELLED_MEETING: function(openingTitle, roundName) {
		return `${config("app.name")} - Canceled Meeting for ${openingTitle} for ${roundName}`;
	},
	OPPORTUNITY: function(openingTitle, companyName) {
		return `Opportunity Matched for ${openingTitle} at ${companyName}`;
	},
	PENDING_INTERVIEW_REMINDER: function(openingTitle, companyName) {
		return `[Reminder] [${companyName}] Interview for ${openingTitle}`;
	},
	PENDING_COMPANY_APPROVAL: function(companyName) {
		return `Thank you, we have received your registration`;
	},
	PENDING_COMPANY_APPROVED: function(companyName) {
		return `Welcome, ${companyName} on ${config("app.name")}`;
	},
	SUMMARY_DAILY_ACTIVITY: function(date, openingTitle, companyName) {
		return `${date}: Summary For ${openingTitle} - ${companyName}`;
	},
	SUMMARY_DAILY_ACTIVITY_OPENING_VENDOR: function(date, openingTitle, companyName) {
		return `${date}: Summary For ${openingTitle} - ${companyName}`;
	},
	SUMMARY_DAILY_ACTIVITY_COMPANY_OPENINGS: function(date, companyName) {
		return `${date}: Summary For ${companyName} Openings`;
	},
	SUMMARY_EXPIRED_INTERVIEW: function(date, companyName) {
		return `${date}: Summary Report Of Interviews Going To Expire For ${companyName}`;
	},
	USER_REFERRAL_INVITATION: function() {
		return `Invitation to join ${config("app.website")}!`;
	},
	CONFIRM_AVAILABLE_SLOT: function(openingTitle, roundName) {
		return `${config("app.name")} - Meeting Request for ${openingTitle} for ${roundName}`;
	},
	EXAM_CANDIDATE_INTERVIEW_INVITATION: function(examTitle) {
		return `${config("app.name")} - Certification Exam Invitation for ${examTitle}`;
	},
	ARTICLE_APPROVED: function(articleTitle) {
		return `Article published - ${articleTitle}`;
	},
	ARTICLE_REJECTED: function(articleTitle) {
		return `Article status of ${articleTitle}`;
	},
	RESCHEDULE_MEETING: function(openingTitle, roundName) {
		return `${config("app.name")} -  Rescheduled Meeting for ${openingTitle} for ${roundName}`;
	},
	INTERVIEW_FAILED: function(examTitle, technologyTitle) {
		return `Certification result of ${examTitle}, ${technologyTitle}`;
	},
	INTERVIEW_PASSED: function(examTitle, technologyTitle) {
		return `Certification result of ${examTitle}, ${technologyTitle}`;
	},
	REFERRAL_CANDIDATE_INTERVIEW_INVITE: function(openingTitle, companyName) {
		return `Your friend referred you for ${openingTitle} at ${companyName}`;
	},
	ACCOUNT_DEACTIVATED: function() {
		return `${config("app.name")} - Account Deactivated`;
	},
	ACCOUNT_REACTIVATED: function() {
		return `${config("app.name")} - Account Reactivated`;
	},
	USER_REWARD_CLAIM_REQUESTED: function() {
		return `${config("app.name")} T-shirt Request`;
	},
	USER_REWARD_CLAIM_APPROVED: function() {
		return `${config("app.name")} T-shirt is on the Way!`;
	},
	APPLICATION_RECEIVED_FOR_JD: function(openingTitle, companyName) {
		return `Thank you for applying for ${openingTitle} at ${companyName}`;
	},
	INTERVIEW_INVITATION: function(openingTitle, companyName) {
		return `Interview Invitation for ${openingTitle} at ${companyName}`;
	},
	INTERVIEW_ASSIGNMENT_INVITATION: function(openingTitle, companyName) {
		return `Interview Invitation for ${openingTitle} at ${companyName}`;
	},
	APPLICATION_WITHDRAWN_FOR_JD: function(openingTitle, companyName) {
		return `Application Withdrawn from ${openingTitle} at ${companyName}`;
	},
	REQUEST_FOR_ADDITIONAL_JD_INFO: function(openingTitle, companyName) {
		return `${companyName} - Complete your application for ${openingTitle}`;
	},
	PENDING_REQUEST_FOR_ADDITIONAL_JD_INFO_REMINDER: function(openingTitle, companyName) {
		return `[Reminder] [${companyName}] Complete your application for ${openingTitle}`;
	},
	INCOMING_EMAIL_MISSING_RESUME: function(openingTitle, companyName) {
		return `[${companyName}] Share your Resume for ${openingTitle}`;
	},
	CUSTOM_REJECTION: function(openingTitle, companyName) {
		return `Application status for the position of ${openingTitle}, ${companyName}`;
	},
};

module.exports = { TEMPLATE, TITLE, SUBJECT };
