const MODELS = {
	AUTH: "auth",
	AUTH_EMAIL: "auth_email",
	AUTH_PHONE: "auth_phone",
	OTP: "otp",
	FILE: "file",
	FILE_S3: "file_s3",
	FILE_MINIO: "file_minio",
	USER: "user",
	EMAIL_OTP: "email_otp"
};

module.exports = MODELS;
