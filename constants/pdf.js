const TEMPLATE = {
	JOB_DESCRIPTION: {
		template: "jobDescription.template.handlebars",
		params: [
			"opening_id"
		]
	}
};

module.exports = { TEMPLATE };