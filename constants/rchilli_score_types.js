const RCHILLI_SCORE_TYPES = {
	Percentage: "Percentage",
	GPA: "Grade Point Average (GPA)"
};

module.exports = RCHILLI_SCORE_TYPES;
