const REJECTED_REASONS = {
	APPLIED_FOR_SIMILAR_JD: "Candidate has already applied for similar JD",
	EDUCATION_NOT_MATCHED: "Candidate Education not matched",
	EXPERIENCE_NOT_MATCHED: "Candidate Experience not matched",
	SKILLS_NOT_MATCHED: "Candidate Skills not matched",
	CTC_NOT_MATCHED: "Candidate CTC not matched",
	NOTICE_PERIOD_NOT_MATCHED: "Candidate Notice period not matched",
	PREFERRED_WORKPLACE_NOT_MATCHED: "Candidate Preferred Workplace not matched",
};

module.exports = REJECTED_REASONS;