const SERVICES = {
	AUTH_SERVICE: "auth-service",
	USERS_SERVICE: "users-service",
	MAIL_SERVICE: "mail-service",
	OPENINGS_SERVICE: "openings-service",
	SMS_SERVICE: "sms-service",
	TWILIO_SERVICE: "twilio-service"
};

module.exports = SERVICES;
