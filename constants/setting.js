const MISCELLANEOUS = {
	INTERVIEW_NOTIFICATION_CHANNEL_ID: "InterviewNotificationChannelId",
	INTERVIEW_AGENT_CHANNEL_ID: "InterviewAgentChannelId",
	SLACK_ANY_MEMBER_IS_ACTIVE: "SlackAnyMemberIsActive",
	SLACK_MEMBERS_ARE_AWAY: "SlackMembersAreAway",
	MIN_QUESTION_ATTEMPT_TIME: "MinQuestionAttemptTime",
	MIN_QUESTION_LEAVE_TIME: "MinQuestionLeaveTime",
	INTERVIEW_APPLY_WARNING_MESSAGE: "InterviewApplyWarningMessage",
	INTERVIEW_TIME_LIMIT: "InterviewTimeLimit", // in milliseconds"
	INTERVIEW_MAX_CONCURRENT_COUNT: "InterviewMaxConcurrentCount",
	RESET_PASSWORD_EXPIRY_IN_MINUTES: "ResetPasswordExpiryInMinutes",
	INTERVIEW_INVITATION_EXPIRY_IN_DAYS: "InterviewInvitationExpiryInDays",
	INTERVIEW_ENDED_RETRY_VIDEO_UPLOAD_COUNT: "InterviewEndedRetryVideoUploadCount",
	MEDIA_SERVER_UPDATED_AT_MAX_TIME_IN_MINUTES: "MediaServerUpdatedAtMaxTimeInMinutes",
	INTERVIEW_INVITATION_RECRUITER_EXPIRY_IN_DAYS: "InterviewInvitationRecruiterExpiryInDays",
	PHONE_OTP_REQUEST_TIMEOUT: "PhoneOTPRequestTimeout", // in seconds
	EMAIL_OTP_REQUEST_TIMEOUT: "EmailOTPRequestTimeout", // in seconds
	CALENDAR_TIMESLOT_BOOKING_IN_DAYS: "CalendarTimeSlotBookingInDays",
	ARCHIVE_VIDEOS_AFTER_IN_DAYS: "ArchiveVideosAfterInDays",
	ARCHIVE_VIDEO_RETRY_COUNT: "ArchiveVideoRetryCount",
	WHATSAPP_MESSAGE_CHANNEL_ID: "WhatsAppMessageChannelId",
	USER_INVITATION_LIMIT: "UserInvitationLimit",
	BLOG_MESSAGE_CHANNEL_ID: "BlogMessageChannelId",
	REWARD_MESSAGE_CHANNEL_ID: "RewardMessageChannelId",
	REWARD_CLAIM_COUNT: "RewardClaimCount",
	MATCH_CANDIDATE_WITH_JD_QUEUE_DELAY_IN_MS: "MatchCandidateWithJdQueueDelayInMilliSeconds",
	ASSIST_START_TIME: "AssistStartTime",
	ASSIST_END_TIME: "AssistEndTime",
	ASSIST_WEEKEND_DAYS: "AssistWeekendDays",
	SUPPORT_TICKET_MESSAGE_CHANNEL_ID: "SupportTicketMessageChannelId",
	JD_FORM_LINK_TIME_LIMIT_IN_HOURS: "JDFormLinkTimeLimitInHours",
	MAIL_LAST_VIEWED_IN_DAYS: "MailLastViewedInDays",
	INTERVIEW_INSTRUCTIONS: "interview.instructions",
	CORPORATE_NOTIFY_MESSAGE_CHANNEL_ID: "CorporateNotifyMessageChannelId",
};

module.exports = MISCELLANEOUS;
