const STAT_FILTERS = [
	{
		name: "all" ,
		filters: {},
	},
	{
		name: "not_approved" ,
		filters: {
			approval_status: ["rejected"],
			is_withdrawn: false,
		},
	},
	{
		name: "not_started_logged_in" ,
		filters: {
			approval_status: ["accepted"],
			interview_result: ["pending"],
			start_timestamp_is_null: true,
			candidate_phone_verified_at_is_null: false,
			is_withdrawn: false,
		},
	},
	{
		name: "not_started_not_logged_in" ,
		filters: {
			approval_status: ["accepted"],
			interview_result: ["pending"],
			start_timestamp_is_null: true,
			candidate_phone_verified_at_is_null: true,
			is_withdrawn: false,
		},
	},
	{
		name: "withdrawn" ,
		filters: {
			is_withdrawn: true,
		},
	},
	{
		name: "ongoing" ,
		filters: {
			approval_status: ["accepted"],
			start_timestamp_is_null: false,
			end_timestamp_is_null: true,
			is_withdrawn: false,
		},
	},
	{
		name: "under_processing" ,
		filters: {
			approval_status: ["accepted"],
			start_timestamp_is_null: false,
			end_timestamp_is_null: false,
			interview_result: ["pending"],
			is_video_processed: false,
			is_withdrawn: false,
		},
	},
	{
		name: "partial" ,
		filters: {
			approval_status: ["accepted"],
			start_timestamp_is_null: false,
			end_timestamp_is_null: false,
			is_partial: true,
			interview_result: ["pending"],
			is_video_processed: true,
			is_withdrawn: false,
		},
	},
	{
		name: "unreviewed" ,
		filters: {
			approval_status: ["accepted"],
			start_timestamp_is_null: false,
			end_timestamp_is_null: false,
			is_partial: false,
			interview_result: ["pending"],
			is_video_processed: true,
			is_reviewed: false,
			is_withdrawn: false,
		},
	},
	{
		name: "reviewed" ,
		filters: {
			approval_status: ["accepted"],
			start_timestamp_is_null: false,
			end_timestamp_is_null: false,
			is_partial: false,
			is_reviewed: true,
			interview_result: ["pending"],
			is_withdrawn: false,
		},
	},
	{
		name: "rejected" ,
		filters: {
			approval_status: ["accepted"],
			interview_result: ["rejected"],
			is_withdrawn: false,
		},
	},
	{
		name: "pending_approval" ,
		filters: {
			interview_result: ["pending"],
			approval_status: ["pending"],
			is_withdrawn: false,
		},
	},
	{
		name: "shortlisted" ,
		filters: {
			approval_status: ["accepted"],
			interview_result: ["shortlisted"],
			is_withdrawn: false,
		},
	},
];

module.exports = STAT_FILTERS;