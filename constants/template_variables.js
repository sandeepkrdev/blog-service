const TEMPLATE_VARIABLES = [
	"email",
	"phone",
	"user.name",
	"user.referral_code",
	"user.source",
	"candidate.website",
	"candidate.bio",
	"candidate.marital_status",
	"candidate.work_history",
	"candidate.desired_role",
	"candidate.gender",
	"slugs.url",
	"unsubscribe.url",
];

module.exports = TEMPLATE_VARIABLES;
