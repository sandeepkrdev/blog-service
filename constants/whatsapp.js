const WHATSAPP = {
	WHATSAPP_PREFIX: "whatsapp:",
	WHATSAPP_DEFAULT_PROFILE: "topgeek",
	WHATSAPP_TEMPLATES: [
		"Hi, I am {{ user.name }} and I am here to assist you.",
		"Thank you for writing to us.",
		"Please send a message to resume this conversation.",
		"This conversation has been marked as closed. If you have any other queries please write to us in this chat.",
		"Please take topgeek device test using this link: https://interview.topgeek.io/device-test",
		`Please share your details in the mentioned context and we will send you a re-invite if the job is still active on our portal:
		Name:
		Email Address:
		Phone Number:
		Profile that you have applied for:`,
		"The new link has been re-shared, 48 hrs extension has been approved. Please take the interview as soon as possible.",
		"This interview is only supported on laptop and has to be accessed via chrome browser only.",
		"Please take a screenshot and share with us.",
		"Your profile is currently under review. You'll get the feedback by this week.",
		"Unfortunately, the position has been filled. Please stay connected with us and check on https://topgeek.io/openings for new roles that can suit your profile",
		"Unfortunately, your profile has not been shortlisted with us. Please stay connected and check on https://topgeek.io/openings for new roles that can suit your profile.",
		"Due to some internal glitch previously the interview link was available. As our recruitment will start from the month of November therefore we will contact you once we initiate the process.",
		"We have got a lot of profiles in the review/interview stage and are continuously processing it. You can expect a delay in our response. Please be patient with us. Also, please share your Notice Period or Last Working Day if you are already serving.",
		"The process will start in the month of Nov-Dec and you will be notified of the process then.",
	],
	TEMPLATE: {
		SAMPLE_WHATSAPP: "sampleWhatsApp.template.handlebars",
		PENDING_INTERVIEW_REMINDER: "pendingInterviewReminder.template.handlebars",
		UNREGISTERED_AUTO_REPLY: "unregisteredAutoReply.template.handlebars",
	},
	NOT_INTERESTED_TEXT: "I'm not interested",
	ERROR_CODES: {
		INVALID_NUMBER: "63003"
	},
	TWILIO_SEND_QUEUE: "whatsapp.twilio.send",
};

module.exports = WHATSAPP;
