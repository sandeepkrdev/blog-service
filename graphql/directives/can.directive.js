const { SchemaDirectiveVisitor } = require("moleculer-apollo-server");
const { defaultFieldResolver } = require("graphql");
const { MoleculerClientError } = require("moleculer").Errors;
const {hasPermission} = require(process.cwd() + "/utils/helpers");

class CanDirective extends SchemaDirectiveVisitor {
	visitFieldDefinition(field) {
		const { resolve = defaultFieldResolver } = field;
		const permission = this.args.permission;
		field.resolve = async function (...args) {
			const result = await resolve.apply(this, args);
			return result;
			// if (await hasPermission(args[2].ctx, permission)) return result;
			// throw new MoleculerClientError(`Must have permission: ${permission}`, 400, "permission");
		};
	}
}

module.exports = CanDirective;
