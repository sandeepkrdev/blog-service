const CanDirective = require("./directives/can.directive");

module.exports = {
	can: CanDirective
};
