"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable("articles_draft", {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			article_id: {
				type: Sequelize.INTEGER,
				allowNull: false,
				unique: true,
				references: {
					model: "articles",
					key: "id"
				}
			},
			title: {
				type: Sequelize.STRING,
				allowNull: false
			},
			subtitles: {
				type: Sequelize.STRING,
				allowNull: true
			},
			cover_image_url: {
				type: Sequelize.STRING,
				allowNull: false
			},
			cover_image_position: {
				type: Sequelize.STRING,
				allowNull: false
			},
			blog_content: {
				type: Sequelize.TEXT("long"),
				allowNull: true
			},
			seo_og_image_url: {
				type: Sequelize.STRING,
				allowNull: true
			},
			seo_title: {
				type: Sequelize.STRING,
				allowNull: true
			},
			seo_description: {
				type: Sequelize.STRING,
				allowNull: true
			},
			original_url: {
				type: Sequelize.STRING,
				allowNull: true
			},
			publishing_date: {
				type: Sequelize.DATE,
				allowNull: true
			},
			allow_comments: {
				type: Sequelize.BOOLEAN,
				allowNull: false,
				defaultValue: 1
			},
			createdAt: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			},
			updatedAt: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			}
		});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable("articles_draft");
	}
};
