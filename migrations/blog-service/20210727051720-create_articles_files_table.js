"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable(
			"article_files",
			{
				id: {
					type: Sequelize.INTEGER,
					primaryKey: true,
					autoIncrement: true
				},
				article_id: {
					type: Sequelize.INTEGER,
					allowNull: false,
					references: {
						model: "articles",
						key: "id"
					}
				},
				file_url: {
					type: Sequelize.STRING,
					allowNull: false
				},
				is_file_used: {
					type: Sequelize.BOOLEAN,
					allowNull: false,
					defaultValue: 1
				},
				createdAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				},
				updatedAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				}
			},
			{
				uniqueKeys: {
					article_file_unique: {
						fields: ["article_id", "file_url"]
					}
				}
			}
		);
		await queryInterface.addIndex("article_files", ["article_id"], {
			indexName: "article_id_index"
		});
		await queryInterface.addIndex("article_files", ["is_file_used"], {
			indexName: "is_file_used_index"
		});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable("article_files");
	}
};
