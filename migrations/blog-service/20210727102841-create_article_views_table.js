"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable("article_views", {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			article_id: {
				type: Sequelize.INTEGER,
				allowNull: false,
				references: {
					model: "articles",
					key: "id"
				}
			},
			view_id: {
				type: Sequelize.STRING,
				allowNull: false
			},
			createdAt: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			},
			updatedAt: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			}
		});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable("article_views");
	}
};
