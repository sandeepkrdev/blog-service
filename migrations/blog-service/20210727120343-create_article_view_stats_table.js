"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable(
			"article_view_stats",
			{
				id: {
					type: Sequelize.INTEGER,
					primaryKey: true,
					autoIncrement: true
				},
				article_id: {
					type: Sequelize.INTEGER,
					allowNull: false,
					references: {
						model: "articles",
						key: "id"
					}
				},
				view_id: {
					type: Sequelize.STRING,
					allowNull: false
				},
				count: {
					type: Sequelize.INTEGER,
					allowNull: false,
					defaultValue: 0
				},
				createdAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				},
				updatedAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				}
			},
			{
				uniqueKeys: {
					article_view_stat_unique: {
						fields: ["article_id", "view_id"]
					}
				}
			}
		);
		await queryInterface.addIndex("article_view_stats", ["article_id"], {
			indexName: "article_id_index"
		});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable("article_view_stats");
	}
};
