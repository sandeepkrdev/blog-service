"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable("article_comments", {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			article_id: {
				type: Sequelize.INTEGER,
				allowNull: false,
				references: {
					model: "articles",
					key: "id"
				}
			},
			text: {
				type: Sequelize.TEXT("medium"),
				allowNull: false
			},
			parent_id: {
				type: Sequelize.INTEGER,
				allowNull: false,
				defaultValue: 0
			},
			user_id: {
				type: Sequelize.INTEGER,
				allowNull: false
			},
			createdAt: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			},
			updatedAt: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			}
		});
		await queryInterface.addIndex("article_comments", ["article_id"], {
			indexName: "article_id_index"
		});
		await queryInterface.addIndex("article_comments", ["parent_id"], {
			indexName: "parent_id_index"
		});
		await queryInterface.addIndex("article_comments", ["user_id"], {
			indexName: "user_id_index"
		});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable("article_comments");
	}
};
