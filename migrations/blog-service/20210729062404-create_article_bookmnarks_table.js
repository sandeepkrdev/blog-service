"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable(
			"article_bookmarks",
			{
				id: {
					type: Sequelize.INTEGER,
					primaryKey: true,
					autoIncrement: true
				},
				article_id: {
					type: Sequelize.INTEGER,
					allowNull: false,
					references: {
						model: "articles",
						key: "id"
					}
				},
				user_id: {
					type: Sequelize.INTEGER,
					allowNull: false
				},
				createdAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				},
				updatedAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				}
			},
			{
				uniqueKeys: {
					article_bookmark_unique: {
						fields: ["article_id", "user_id"]
					}
				}
			}
		);
		await queryInterface.addIndex("article_bookmarks", ["article_id"], {
			indexName: "article_id_index"
		});
		await queryInterface.addIndex("article_bookmarks", ["user_id"], {
			indexName: "user_id_index"
		});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable("article_bookmarks");
	}
};
