"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable(
			"article_reactions",
			{
				id: {
					type: Sequelize.INTEGER,
					primaryKey: true,
					autoIncrement: true
				},
				article_id: {
					type: Sequelize.INTEGER,
					allowNull: false,
					references: {
						model: "articles",
						key: "id"
					}
				},
				master_reaction_id: {
					type: Sequelize.INTEGER,
					allowNull: false,
					references: {
						model: "master_reactions",
						key: "id"
					}
				},
				user_id: {
					type: Sequelize.INTEGER,
					allowNull: false
				},
				createdAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				},
				updatedAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				}
			},
			{
				uniqueKeys: {
					article_reaction_unique: {
						fields: ["article_id", "master_reaction_id", "user_id"]
					}
				}
			}
		);
		await queryInterface.addIndex("article_reactions", ["article_id"], {
			indexName: "article_id_index"
		});
		await queryInterface.addIndex(
			"article_reactions",
			["master_reaction_id"],
			{
				indexName: "master_reaction_id_index"
			}
		);
		await queryInterface.addIndex("article_reactions", ["user_id"], {
			indexName: "user_id_index"
		});
	},

	down: async (queryInterface, Sequelize) => {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('users');
		 */
		await queryInterface.dropTable("article_reactions");
	}
};
