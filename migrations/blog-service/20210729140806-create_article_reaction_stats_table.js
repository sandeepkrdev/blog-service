"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		/**
		 * Add altering commands here.
		 *
		 * Example:
		 * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
		 */
		await queryInterface.createTable("article_reaction_stats", {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			article_id: {
				type: Sequelize.INTEGER,
				allowNull: false,
				references: {
					model: "articles",
					key: "id"
				}
			},
			master_reaction_id: {
				type: Sequelize.INTEGER,
				allowNull: false,
				references: {
					model: "master_reactions",
					key: "id"
				}
			},
			count: {
				type: Sequelize.INTEGER,
				allowNull: false,
				defaultValue: 0
			},
			createdAt: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			},
			updatedAt: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			}
		});
		await queryInterface.addIndex(
			"article_reaction_stats",
			["article_id"],
			{
				indexName: "article_id_index"
			}
		);
		await queryInterface.addIndex(
			"article_reaction_stats",
			["master_reaction_id"],
			{
				indexName: "master_reaction_id_index"
			}
		);
	},

	down: async (queryInterface, Sequelize) => {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('users');
		 */
		await queryInterface.dropTable("article_reaction_stats");
	}
};
