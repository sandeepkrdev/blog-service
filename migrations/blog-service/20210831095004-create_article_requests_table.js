"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable(
			"article_requests",
			{
				id: {
					type: Sequelize.INTEGER,
					primaryKey: true,
					autoIncrement: true
				},
				article_id: {
					type: Sequelize.INTEGER,
					allowNull: false,
					references: {
						model: "articles",
						key: "id"
					}
				},
				status: {
					type: Sequelize.STRING,
					allowNull: false,
					defaultValue: "pending"
				},
				approved_by: {
					type: Sequelize.INTEGER,
					defaultValue: 0
				},
				approved_on: {
					type: Sequelize.DATE,
					allowNull: true
				},
				comments: {
					type: Sequelize.TEXT,
					allowNull: true
				},
				share_comments: {
					type: Sequelize.BOOLEAN,
					defaultValue: 0
				},
				createdAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				},
				updatedAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				}
			}
		);
		await queryInterface.addIndex("article_requests", ["article_id"], {
			indexName: "article_id_index"
		});
		await queryInterface.addIndex("article_requests", ["status"], {
			indexName: "status_index"
		});
		await queryInterface.addIndex("article_requests", ["approved_by"], {
			indexName: "approved_by_index"
		});
	},

	down: async (queryInterface, Sequelize) => {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('users');
		 */
		await queryInterface.dropTable("article_requests");
	}
};
