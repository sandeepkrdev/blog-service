"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.createTable("failed_jobs", {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			queue: {
				type: Sequelize.STRING,
				allowNull: false
			},
			payload: {
				type: Sequelize.TEXT,
				allowNull: false
			},
			exception: {
				type: Sequelize.TEXT,
				allowNull: true
			},
			failed_at: {
				type: Sequelize.DATE,
				allowNull: false,
			},
			createdAt: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			},
			updatedAt: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			}
		});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable("failed_jobs");
	}
};
