"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.changeColumn("failed_jobs", "payload", {
			type: Sequelize.TEXT("long"),
			allowNull: false
		});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.changeColumn("failed_jobs", "payload", {
			type: Sequelize.TEXT,
			allowNull: false
		});
	}
};
