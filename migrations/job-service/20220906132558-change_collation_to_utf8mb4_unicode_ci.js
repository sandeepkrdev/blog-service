"use strict";

const failed_jobs_table = "failed_jobs";
const failed_jobs_columns = ["queue", "exception"];

module.exports = {
	up: async (queryInterface, Sequelize) => {
		for (let i = 0; i < failed_jobs_columns.length; i++) {
			if (failed_jobs_columns[i] == "exception") {
				const alterQuery =
					"ALTER TABLE " +
					failed_jobs_table +
					" MODIFY " +
					failed_jobs_columns[i] +
					" TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";
				await queryInterface.sequelize.query(alterQuery);
			} else {
				const alterQuery =
					"ALTER TABLE " +
					failed_jobs_table +
					" MODIFY " +
					failed_jobs_columns[i] +
					" VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;";
				await queryInterface.sequelize.query(alterQuery);
			}
		}
	},

	down: async (queryInterface, Sequelize) => {}
};
