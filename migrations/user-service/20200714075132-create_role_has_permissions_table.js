"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		/**
		 * Add altering commands here.
		 *
		 * Example:
		 * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
		 */
		await queryInterface.createTable(
			"role_has_permissions",
			{
				id: {
					type: Sequelize.INTEGER,
					primaryKey: true,
					autoIncrement: true
				},
				role_id: {
					type: Sequelize.INTEGER,
					allowNull: false
				},
				permission_id: {
					type: Sequelize.INTEGER,
					allowNull: false
				},
				createdAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				},
				updatedAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				}
			},
			{
				uniqueKeys: {
					role_permission_unique: {
						fields: ["role_id", "permission_id"]
					}
				}
			}
		);
		await queryInterface.addIndex("role_has_permissions", ["role_id"], {
			indexName: "role_id_index"
		});
		await queryInterface.addIndex(
			"role_has_permissions",
			["permission_id"],
			{
				indexName: "permission_id_index"
			}
		);
	},

	down: async (queryInterface, Sequelize) => {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('users');
		 */
		await queryInterface.dropTable("role_has_permissions");
	}
};
