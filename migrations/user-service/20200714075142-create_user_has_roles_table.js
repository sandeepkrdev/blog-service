"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		/**
		 * Add altering commands here.
		 *
		 * Example:
		 * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
		 */
		await queryInterface.createTable(
			"user_has_roles",
			{
				id: {
					type: Sequelize.INTEGER,
					primaryKey: true,
					autoIncrement: true
				},
				user_id: {
					type: Sequelize.INTEGER,
					allowNull: false
				},
				role_id: {
					type: Sequelize.INTEGER,
					allowNull: false
				},
				createdAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				},
				updatedAt: {
					type: Sequelize.DATE,
					defaultValue: Sequelize.NOW
				}
			},
			{
				uniqueKeys: {
					user_role_unique: {
						fields: ["user_id", "role_id"]
					}
				}
			}
		);

		await queryInterface.addIndex("user_has_roles", ["user_id"], {
			indexName: "user_id_index"
		});
		await queryInterface.addIndex("user_has_roles", ["role_id"], {
			indexName: "role_id_index"
		});
	},

	down: async (queryInterface, Sequelize) => {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('users');
		 */
		await queryInterface.dropTable("user_has_roles");
	}
};
