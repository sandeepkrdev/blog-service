"use strict";
const { DB_KEYS, ENUM, MODELS } = require(process.cwd() + "/constants");

module.exports = {
	up: async (queryInterface, Sequelize) => {
		/*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
		await queryInterface.createTable(MODELS.AUTH, {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			user_id: {
				type: Sequelize.INTEGER,
				allowNull: false
			},
			ref_type: {
				type: Sequelize.ENUM,
				allowNull: false,
				values: [ENUM.AUTH.REF_TYPE.AUTH_EMAIL],
				comment: "Flag which says the auth type. It can be auth_email."
			},
			ref_id: {
				type: Sequelize.INTEGER,
				allowNull: false,
				comment:
					"Based on ref_type, it is the PK of the auth_email table."
			},
			createdAt: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			},
			updatedAt: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			}
		});

		await queryInterface.addIndex(MODELS.AUTH, [DB_KEYS.COMMON.USER_ID], {
			indexName: "user_id_index"
		});
	},

	down: async (queryInterface, Sequelize) => {
		/*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
		await queryInterface.dropTable(MODELS.AUTH);
	}
};
