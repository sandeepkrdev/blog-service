"use strict";

const MODEL_NAME = "permissions";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.bulkInsert(MODEL_NAME, [
			{
				name: "roles-create",
				createdAt: new Date(),
				updatedAt: new Date()
			},
			{
				name: "roles-update",
				createdAt: new Date(),
				updatedAt: new Date()
			},
			{
				name: "roles-delete",
				createdAt: new Date(),
				updatedAt: new Date()
			},
			{
				name: "user-role-create",
				createdAt: new Date(),
				updatedAt: new Date()
			},
			{
				name: "user-role-delete",
				createdAt: new Date(),
				updatedAt: new Date()
			},
			{
				name: "role-permission-create",
				createdAt: new Date(),
				updatedAt: new Date()
			},
			{
				name: "role-permission-delete",
				createdAt: new Date(),
				updatedAt: new Date()
			}
		]);
	},
	down: async (queryInterface, Sequelize) => {
		await queryInterface.bulkDelete(
			MODEL_NAME,
			{
				name: {
					[Sequelize.Op.in]: [
						"roles-create",
						"roles-update",
						"roles-delete",
						"user-role-create",
						"user-role-delete",
						"role-permission-create",
						"role-permission-delete"
					]
				}
			},
			{}
		);
	}
};
