"use strict";

const MODEL_NAME = "permissions";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		await queryInterface.bulkInsert(MODEL_NAME, [
			{
				name: "user-read-list",
				createdAt: new Date(),
				updatedAt: new Date()
			},
			{
				name: "role-read-list",
				createdAt: new Date(),
				updatedAt: new Date()
			},
			{
				name: "permission-read-list",
				createdAt: new Date(),
				updatedAt: new Date()
			}
		]);
	},
	down: async (queryInterface, Sequelize) => {
		await queryInterface.bulkDelete(
			MODEL_NAME,
			{
				name: {
					[Sequelize.Op.in]: [
						"user-read-list",
						"role-read-list",
						"permission-read-list"
					]
				}
			},
			{}
		);
	}
};
