"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		/**
		 * Add altering commands here.
		 *
		 * Example:
		 * await queryInterface.createTable('sessions', { id: Sequelize.INTEGER });
		 */
		await queryInterface.createTable("sessions", {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			user_id: {
				type: Sequelize.INTEGER,
				allowNull: false
			},
			session_id: {
				type: Sequelize.STRING,
				allowNull: false
			},
			createdAt: {
				type: Sequelize.DATE,
				defaultValue: Sequelize.NOW
			},
			expiresAt: {
				type: Sequelize.DATE
			}
		});

		await queryInterface.addIndex("sessions", ["user_id"], {
			indexName: "user_id_index"
		});
		await queryInterface.addIndex("sessions", ["session_id"], {
			name: "session_id_unique",
			type: "unique"
		});
	},

	down: async (queryInterface, Sequelize) => {
		/**
		 * Add reverting commands here.
		 *
		 * Example:
		 * await queryInterface.dropTable('sessions');
		 */
		await queryInterface.dropTable("sessions");
	}
};
