const routeOptions = require(process.cwd() + "/utils/routeOptions");
const schemaDirectives = require(process.cwd() + "/graphql/schemaDirectives");

module.exports = {
	// Global GraphQL typeDefs			// Global GraphQL typeDefs
	typeDefs: `
		directive @can(permission: String!) on FIELD_DEFINITION
		scalar Date
		enum OrderByEnum {
			ASC
			DESC
		}
		type Response {
			message: String
			status: Boolean
		}
		type KeyValue {
			key: String
			value: String
		}
    `,
	// Global resolvers
	resolvers: {
		//
	},

	schemaDirectives,

	// API Gateway route options
	routeOptions: {
		path: "/graphql",
		cors: true,
		mappingPolicy: "restrict",
		authorization: true,
		logging: true,
		bodyParsers: {
			json: {
				strict: false,
				limit: "1MB"
			},
			urlencoded: {
				extended: true,
				limit: "1MB"
			}
		},
		...routeOptions
	},
	createAction: true,

	// https://www.apollographql.com/docs/apollo-server/v2/api/apollo-server.html
	serverOptions: {
		playground: false,
		tracing: false,
		engine: {
			apiKey: process.env.APOLLO_ENGINE_KEY
		}
	}
};
