const _ = require("lodash");
const ApiGateway = require("moleculer-web");
const { UnAuthorizedError } = ApiGateway.Errors;
const { config } = require(process.cwd() + "/utils/env");
const authorizeByBearer = require(process.cwd() + "/services/gateway/api.service/api-methods/authorize");

module.exports = {
	methods: {
		async checkIsAuthenticated(ctx) {
			if (ctx.meta && ctx.meta.is_internal) return true;
			if (!_.isEmpty(ctx.meta) && !ctx.meta.user) throw new UnAuthorizedError();
		},
		async checkHasUserId(ctx) {
			if (
				(ctx.params && ctx.params.is_login_not_required && ctx.params.is_login_not_required == "1") ||
				(ctx.params.user_id) ||
				(ctx.meta && ctx.meta.user)
			) return;
			throw new UnAuthorizedError();
		},
		async checkIsRecordedAuthenticated(ctx) {
			if (ctx.meta.is_internal) {
				return;
			}
			if (ctx.meta.authorization || ctx.params.token) {
				if (
					ctx.meta.authorization === config("openings-service.recorder_webhook_token") ||
					ctx.params.token === config("openings-service.recorder_webhook_token")
				) {
					return;
				}
			}
			throw new UnAuthorizedError();
		},
		authorizeByBearer,
		async checkIsWebRecorderAuthenticated(ctx) {
			if (ctx.meta.is_internal) {
				return;
			}
			if (ctx.meta.authorization || ctx.params.token) {
				if (
					ctx.meta.authorization === config("webrecorder-service.app_key") ||
					ctx.params.token === config("webrecorder-service.app_key")
				) {
					return;
				}
			}
			throw new UnAuthorizedError();
		},
	}
};
