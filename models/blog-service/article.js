module.exports = (sequelize, DataTypes) => {
	const Article = sequelize.define(
		"article",
		{
			user_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			slug: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: true
			}
		},
		{
			indexes: [
				{
					unique: false,
					fields: ["user_id"]
				},
				{
					unique: true,
					fields: ["slug"]
				}
			]
		}
	);
	Article.associate = function (models) {
		Article.hasOne(models.article_draft, {
			foreignKey: "article_id",
			as: "draft",
			onDelete: "cascade",
			hooks: true
		});
		Article.hasOne(models.article_published, {
			foreignKey: "article_id",
			as: "published",
			onDelete: "cascade",
			hooks: true
		});
		Article.hasMany(models.article_history, {
			foreignKey: "article_id",
			as: "histories",
			onDelete: "cascade",
			hooks: true
		});
		Article.hasMany(models.article_file, {
			foreignKey: "article_id",
			as: "files",
			onDelete: "cascade",
			hooks: true
		});
		Article.hasMany(models.article_view, {
			foreignKey: "article_id",
			as: "views",
			onDelete: "cascade",
			hooks: true
		});
		Article.hasMany(models.article_view_stat, {
			foreignKey: "article_id",
			as: "viewStats",
			onDelete: "cascade",
			hooks: true
		});
		Article.hasMany(models.article_comment, {
			foreignKey: "article_id",
			as: "comments",
			onDelete: "cascade",
			hooks: true
		});
		Article.hasMany(models.article_reaction, {
			foreignKey: "article_id",
			as: "reactions",
			onDelete: "cascade",
			hooks: true
		});
		Article.hasMany(models.article_reaction_stat, {
			foreignKey: "article_id",
			as: "reactionStats",
			onDelete: "cascade",
			hooks: true
		});
		Article.hasMany(models.article_bookmark, {
			foreignKey: "article_id",
			as: "bookmarks",
			onDelete: "cascade",
			hooks: true
		});
	};
	return Article;
};
