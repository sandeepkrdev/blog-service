module.exports = (sequelize, DataTypes) => {
	const ArticleBookmark = sequelize.define(
		"article_bookmark",
		{
			article_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			user_id: {
				type: DataTypes.STRING,
				allowNull: false
			}
		},
		{
			indexes: [
				{
					unique: false,
					fields: ["article_id"]
				},
				{
					unique: false,
					fields: ["user_id"]
				},
				{
					unique: true,
					fields: ["article_id", "user_id"]
				}
			]
		}
	);
	ArticleBookmark.associate = function (models) {
		ArticleBookmark.belongsTo(models.article, {
			foreignKey: "article_id",
			as: "article"
		});
	};
	return ArticleBookmark;
};
