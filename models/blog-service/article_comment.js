module.exports = (sequelize, DataTypes) => {
	const ArticleComment = sequelize.define(
		"article_comment",
		{
			article_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			text: {
				type: DataTypes.TEXT("medium"),
				allowNull: false
			},
			parent_id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				defaultValue: 0
			},
			user_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			}
		},
		{
			indexes: [
				{
					unique: false,
					fields: ["article_id"]
				},
				{
					unique: false,
					fields: ["parent_id"]
				},
				{
					unique: false,
					fields: ["user_id"]
				}
			]
		}
	);
	ArticleComment.associate = function (models) {
		ArticleComment.belongsTo(models.article, {
			foreignKey: "article_id",
			as: "article"
		});
	};
	return ArticleComment;
};
