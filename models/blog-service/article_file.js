module.exports = (sequelize, DataTypes) => {
	const ArticleFile = sequelize.define(
		"article_file",
		{
			article_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			file_url: {
				type: DataTypes.STRING,
				allowNull: false
			},
			is_file_used: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
				defaultValue: 1
			}
		},
		{
			indexes: [
				{
					unique: false,
					fields: ["article_id"]
				},
				{
					unique: false,
					fields: ["is_file_used"]
				},
				{
					unique: true,
					fields: ["article_id", "file_url"]
				}
			]
		}
	);
	ArticleFile.associate = function (models) {
		ArticleFile.belongsTo(models.article, {
			foreignKey: "article_id",
			as: "article"
		});
	};
	return ArticleFile;
};
