module.exports = (sequelize, DataTypes) => {
	const ArticleHistory = sequelize.define(
		"article_history",
		{
			article_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			title: {
				type: DataTypes.STRING,
				allowNull: false
			},
			subtitles: {
				type: DataTypes.STRING,
				allowNull: true
			},
			cover_image_url: {
				type: DataTypes.STRING,
				allowNull: false
			},
			cover_image_position: {
				type: DataTypes.STRING,
				allowNull: false
			},
			blog_content: {
				type: DataTypes.TEXT("long"),
				allowNull: true
			},
			seo_og_image_url: {
				type: DataTypes.STRING,
				allowNull: true
			},
			seo_title: {
				type: DataTypes.STRING,
				allowNull: true
			},
			seo_description: {
				type: DataTypes.STRING,
				allowNull: true
			},
			original_url: {
				type: DataTypes.STRING,
				allowNull: true
			},
			publishing_date: {
				type: DataTypes.DATE,
				allowNull: true
			},
			allow_comments: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
				defaultValue: 1
			}
		},
		{ tableName: "articles_histories" },
		{
			indexes: [
				{
					unique: false,
					fields: ["article_id"]
				}
			]
		}
	);
	ArticleHistory.associate = function (models) {
		ArticleHistory.belongsTo(models.article, {
			foreignKey: "article_id",
			as: "article"
		});
	};
	return ArticleHistory;
};
