module.exports = (sequelize, DataTypes) => {
	const ArticlePublished = sequelize.define(
		"article_published",
		{
			article_id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				unique: true
			},
			title: {
				type: DataTypes.STRING,
				allowNull: false
			},
			subtitles: {
				type: DataTypes.STRING,
				allowNull: true
			},
			cover_image_url: {
				type: DataTypes.STRING,
				allowNull: false
			},
			cover_image_position: {
				type: DataTypes.STRING,
				allowNull: false
			},
			blog_content: {
				type: DataTypes.TEXT("long"),
				allowNull: true
			},
			seo_og_image_url: {
				type: DataTypes.STRING,
				allowNull: true
			},
			seo_title: {
				type: DataTypes.STRING,
				allowNull: true
			},
			seo_description: {
				type: DataTypes.STRING,
				allowNull: true
			},
			original_url: {
				type: DataTypes.STRING,
				allowNull: true
			},
			publishing_date: {
				type: DataTypes.DATE,
				allowNull: true
			},
			allow_comments: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
				defaultValue: 1
			}
		},
		{ tableName: "articles_published" }
	);
	ArticlePublished.associate = function (models) {
		ArticlePublished.belongsTo(models.article, {
			foreignKey: "article_id",
			as: "article"
		});
	};
	return ArticlePublished;
};
