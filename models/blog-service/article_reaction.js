module.exports = (sequelize, DataTypes) => {
	const ArticleReaction = sequelize.define(
		"article_reaction",
		{
			article_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			master_reaction_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			user_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			}
		},
		{
			indexes: [
				{
					unique: false,
					fields: ["article_id"]
				},
				{
					unique: false,
					fields: ["master_reaction_id"]
				},
				{
					unique: false,
					fields: ["user_id"]
				},
				{
					unique: true,
					fields: ["article_id", "master_reaction_id", "user_id"]
				}
			]
		}
	);
	ArticleReaction.associate = function (models) {
		ArticleReaction.belongsTo(models.article, {
			foreignKey: "article_id",
			as: "article"
		});
		ArticleReaction.belongsTo(models.master_reaction, {
			foreignKey: "master_reaction_id",
			as: "masterReaction"
		});
	};
	return ArticleReaction;
};
