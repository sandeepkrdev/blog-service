"use strict";
module.exports = (sequelize, DataTypes) => {
	const ArticleReactionStat = sequelize.define(
		"article_reaction_stat",
		{
			article_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			master_reaction_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			count: {
				type: DataTypes.INTEGER,
				allowNull: false,
				defaultValue: 0
			}
		},
		{
			indexes: [
				{
					unique: false,
					fields: ["article_id"]
				},
				{
					unique: false,
					fields: ["master_reaction_id"]
				}
			]
		}
	);
	ArticleReactionStat.associate = function (models) {
		ArticleReactionStat.belongsTo(models.article, {
			foreignKey: "article_id",
			as: "article"
		});
		ArticleReactionStat.belongsTo(models.master_reaction, {
			foreignKey: "master_reaction_id",
			as: "master_reaction"
		});
	};
	return ArticleReactionStat;
};
