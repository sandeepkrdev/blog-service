module.exports = (sequelize, DataTypes) => {
	const ArticleRequest = sequelize.define(
		"article_request",
		{
			article_id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				unique: true
			},
			status: {
				type: DataTypes.STRING,
				allowNull: false,
				defaultValue: "pending"
			},
			approved_by: {
				type: DataTypes.INTEGER,
				defaultValue: 0
			},
			approved_on: {
				type: DataTypes.DATE,
				allowNull: true
			},
			comments: {
				type: DataTypes.TEXT,
				allowNull: true
			},
			share_comments: {
				type: DataTypes.BOOLEAN,
				defaultValue: 0
			},
		},
		{ tableName: "article_requests" }
	);
	ArticleRequest.associate = function (models) {
		ArticleRequest.belongsTo(models.article, {
			foreignKey: "article_id",
			as: "article"
		});
	};
	return ArticleRequest;
};
