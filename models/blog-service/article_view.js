module.exports = (sequelize, DataTypes) => {
	const ArticleView = sequelize.define(
		"article_view",
		{
			article_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			view_id: {
				type: DataTypes.STRING,
				allowNull: false
			}
		},
		{
			indexes: [
				{
					unique: false,
					fields: ["article_id"]
				}
			]
		}
	);
	ArticleView.associate = function (models) {
		ArticleView.belongsTo(models.article, {
			foreignKey: "article_id",
			as: "article"
		});
	};
	return ArticleView;
};
