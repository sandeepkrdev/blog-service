module.exports = (sequelize, DataTypes) => {
	const ArticleViewStat = sequelize.define(
		"article_view_stat",
		{
			article_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			view_id: {
				type: DataTypes.STRING,
				allowNull: false
			},
			count: {
				type: DataTypes.INTEGER,
				allowNull: false,
				defaultValue: 0
			}
		},
		{
			indexes: [
				{
					unique: false,
					fields: ["article_id"]
				},
				{
					unique: true,
					fields: ["article_id", "view_id"]
				}
			]
		}
	);
	ArticleViewStat.associate = function (models) {
		ArticleViewStat.belongsTo(models.article, {
			foreignKey: "article_id",
			as: "article"
		});
	};
	return ArticleViewStat;
};
