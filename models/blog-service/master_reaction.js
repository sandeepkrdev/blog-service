"use strict";
module.exports = (sequelize, DataTypes) => {
	const MasterReaction = sequelize.define(
		"master_reaction",
		{
			file_url: {
				type: DataTypes.STRING,
				allowNull: false
			},
			is_primary: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
				defaultValue: 0
			}
		},
		{
			indexes: [
				{
					unique: false,
					fields: ["is_primary"]
				}
			]
		}
	);
	MasterReaction.associate = function (models) {
		MasterReaction.hasMany(models.article_reaction, {
			foreignKey: "master_reaction_id",
			as: "article_reactions",
			onDelete: "cascade",
			hooks: true
		});
		MasterReaction.hasMany(models.article_reaction_stat, {
			foreignKey: "master_reaction_id",
			as: "article_reaction_stats",
			onDelete: "cascade",
			hooks: true
		});
	};
	return MasterReaction;
};
