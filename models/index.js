"use strict";

const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const basename = path.basename(__filename);
const config = require(process.cwd() + "/config/config.json");
const env = require(process.cwd() + "/utils/env");

const db = { sequelize: {} };

const databases = Object.keys(config);

for (let i = 0; i < databases.length; ++i) {
	let database = databases[i];
	let dbPath = config[database];
	let sequelize = new Sequelize(
		dbPath.database,
		dbPath.username,
		dbPath.password,
		{
			...dbPath,
			pool: { max: 5, idle: 60000, acquire: 60000 },
			logging: env.config("app.sequelize.logging"),
			logQueryParameters: true
		}
	);
	db.sequelize[database] = sequelize;
	if (fs.existsSync(__dirname + "/" + database)) {
		fs.readdirSync(__dirname + "/" + database)
			.filter(
				file =>
					file.indexOf(".") !== 0 &&
				file !== basename &&
				file.slice(-3) === ".js"
			)
			.forEach(file => {
				const model = require(path.join(__dirname + "/" + database, file))(
					sequelize,
					Sequelize.DataTypes
				);
				db[model.name] = model;
			});
	}
}

Object.keys(db).forEach(modelName => {
	if (db[modelName].associate) {
		db[modelName].associate(db);
	}
});

module.exports = db;
