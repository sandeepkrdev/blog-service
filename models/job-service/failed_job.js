module.exports = (sequelize, DataTypes) => {
	const FailedJob = sequelize.define(
		"failed_job",
		{
			queue: {
				type: DataTypes.STRING,
				allowNull: false
			},
			payload: {
				type: DataTypes.TEXT("long"),
				allowNull: false
			},
			exception: {
				type: DataTypes.TEXT,
				allowNull: false
			},
			failed_at: {
				type: DataTypes.DATE,
				allowNull: false
			},
		},
	);
	return FailedJob;
};
