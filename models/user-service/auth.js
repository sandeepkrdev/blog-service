"use strict";

const { MODELS, ENUM, DB_KEYS } = require(process.cwd() + "/constants");

module.exports = (sequelize, DataTypes) => {
	const Auth = sequelize.define(
		MODELS.AUTH,
		{
			[DB_KEYS.COMMON.USER_ID]: {
				type: DataTypes.INTEGER,
				allowNull: false,
				unique: true
			},
			[DB_KEYS.COMMON.REF_TYPE]: {
				type: DataTypes.ENUM,
				allowNull: false,
				values: [
					ENUM.AUTH.REF_TYPE.AUTH_EMAIL,
				],
				comment:
					"Flag which says the auth type. It can be auth_email, auth_phone or otp."
			},
			[DB_KEYS.COMMON.REF_ID]: {
				type: DataTypes.INTEGER,
				allowNull: false,
				comment:
					"Based on ref_type, it is the PK of the auth_email, auth_phone or otp table."
			}
		},
		{
			freezeTableName: true,
			indexes: [
				{
					unique: true,
					fields: [DB_KEYS.COMMON.REF_TYPE]
				},
				{
					unique: true,
					fields: [DB_KEYS.COMMON.REF_ID]
				}
			]
		}
	);
	Auth.associate = function (models) {
		//Polymorphic Association Start//
		Auth.hasMany(models[MODELS.AUTH_EMAIL], {
			foreignKey: DB_KEYS.COMMON.ID,
			sourceKey: DB_KEYS.COMMON.REF_ID,
			constraints: false,
			as: "authEmails",
			scopes: {
				[DB_KEYS.COMMON.REF_TYPE]: ENUM.AUTH.REF_TYPE.AUTH_EMAIL
			}
		});

		Auth.belongsTo(models.user, {
			foreignKey: "user_id",
			as: "user"
		});
	};
	return Auth;
};
