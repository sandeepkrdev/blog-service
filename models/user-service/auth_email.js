"use strict";

const { MODELS, DB_KEYS } = require(process.cwd() + "/constants");

module.exports = (sequelize, DataTypes) => {
	const AuthEmail = sequelize.define(
		MODELS.AUTH_EMAIL,
		{
			[DB_KEYS.AUTH.EMAIL]: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: true,
				validate: {
					isEmail: true
				}
			},
			[DB_KEYS.AUTH.PASSWORD]: {
				type: DataTypes.STRING,
				allowNull: true
			},
		},
		{
			freezeTableName: true,
			[DB_KEYS.COMMON.UPDATED_AT]: false,
			indexes: [
				{
					unique: true,
					fields: [DB_KEYS.AUTH.EMAIL]
				}
			]
		}
	);

	AuthEmail.associate = function (models) {
		AuthEmail.hasOne(models.auth, {
			foreignKey: "ref_id",
			as: "auth"
		});
	};

	return AuthEmail;
};
