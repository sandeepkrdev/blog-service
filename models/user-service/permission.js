"use strict";
module.exports = (sequelize, DataTypes) => {
	const Permission = sequelize.define(
		"permission",
		{
			name: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: true
			}
		},
		{}
	);
	Permission.associate = function (models) {
		Permission.hasMany(models.role_has_permission, {
			foreignKey: "permission_id",
			as: "rolePermissions",
			onDelete: "cascade",
			hooks: true
		});
	};
	return Permission;
};
