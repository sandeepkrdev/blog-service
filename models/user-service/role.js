"use strict";
module.exports = (sequelize, DataTypes) => {
	const Role = sequelize.define(
		"role",
		{
			name: {
				type: DataTypes.STRING,
				allowNull: false,
				unique: true
			}
		},
		{}
	);
	Role.associate = function (models) {
		Role.hasMany(models.role_has_permission, {
			foreignKey: "role_id",
			as: "rolePermissions",
			onDelete: "cascade",
			hooks: true
		});
		Role.hasMany(models.user_has_role, {
			foreignKey: "role_id",
			as: "userRoles",
			onDelete: "cascade",
			hooks: true
		});
	};
	return Role;
};
