"use strict";
module.exports = (sequelize, DataTypes) => {
	const RoleHasPermission = sequelize.define(
		"role_has_permission",
		{
			role_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			permission_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			}
		},
		{}
	);
	RoleHasPermission.associate = function (models) {
		RoleHasPermission.belongsTo(models.permission, {
			foreignKey: "permission_id",
			as: "permission",
			hooks: true
		});
		RoleHasPermission.belongsTo(models.role, {
			foreignKey: "role_id",
			as: "role",
			hooks: true
		});
	};
	return RoleHasPermission;
};
