"use strict";
module.exports = (sequelize, DataTypes) => {
	const Session = sequelize.define(
		"session",
		{
			user_id: {
				type: DataTypes.INTEGER,
				allowNull: false
			},
			session_id: {
				type: DataTypes.STRING,
				unique: true
			},
			createdAt: {
				type: DataTypes.DATE
			},
			expiresAt: DataTypes.DATE
		},
		{
			updatedAt: false,
			indexes: [
				{
					unique: false,
					fields: ["user_id"]
				}
			]
		}
	);
	Session.associate = function (models) {
		// associations can be defined here
	};
	return Session;
};
