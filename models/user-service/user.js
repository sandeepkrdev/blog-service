"use strict";
module.exports = (sequelize, DataTypes) => {
	const User = sequelize.define(
		"user",
		{
			name: {
				type: DataTypes.STRING,
				allowNull: false
			},
			email: {
				type: DataTypes.STRING,
				allowNull: true
			},
		},
		{
			indexes: [
				{
					unique: false,
					fields: ["email"]
				},
			]
		},
	);
	User.associate = function (models) {
		User.hasMany(models.user_has_role, {
			foreignKey: "user_id",
			as: "userRoles",
			onDelete: "cascade",
			hooks: true
		});
		User.hasMany(models.auth, {
			foreignKey: "user_id",
			as: "userAuths",
			onDelete: "cascade",
			hooks: true
		});
	};
	return User;
};
