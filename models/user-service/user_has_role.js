"use strict";
module.exports = (sequelize, DataTypes) => {
	const UserHasRole = sequelize.define(
		"user_has_role",
		{
			user_id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				unique: true
			},
			role_id: {
				type: DataTypes.INTEGER,
				allowNull: false,
				unique: true
			}
		},
		{}
	);
	UserHasRole.associate = function (models) {
		UserHasRole.belongsTo(models.role, {
			foreignKey: "role_id",
			as: "role",
			hooks: true
		});
		UserHasRole.belongsTo(models.user, {
			foreignKey: "user_id",
			as: "user",
			hooks: true
		});
	};
	return UserHasRole;
};
