(async () => {
	const {
		exec
	} = require("child_process");

	await new Promise((resolve, reject) => {
		const migrate = exec(
			"./node_modules/sequelize-cli/lib/sequelize --options-path ./sequelize/.sequelize-job-service --env job-service db:migrate", {
				env: process.env
			},
			err => (err ? reject(err) : resolve())
		);

		// Forward stdout+stderr to this process
		migrate.stdout.pipe(process.stdout);
		migrate.stderr.pipe(process.stderr);
	});
})();
