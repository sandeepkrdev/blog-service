"use strict";

const Permission = require(process.cwd() + "/models").permission;

const permissions = [
	"roles-create",
	"roles-update",
	"roles-delete",
	"user-role-create",
	"user-role-delete",
	"role-permission-create",
	"role-permission-delete",
	"user-read-list",
	"role-read-list",
	"permission-read-list",
	"manage-users",
	"can-publish-articles",
	"manage-reactions",
];

module.exports = {
	up: async (queryInterface, Sequelize) => {
		/*
		Add altering commands here.
		Return a promise to correctly handle asynchronicity.

		Example:
		return queryInterface.bulkInsert('People', [{
			name: 'John Doe',
			isBetaMember: false
		}], {});
		*/
		await Promise.all(
			permissions.map(permission => {
				console.log("permission - ", permission);
				return Permission.upsert({
					name: permission
				});
			}),
		);
	},

	down: async (queryInterface, Sequelize) => {
		/*
		Add reverting commands here.
		Return a promise to correctly handle asynchronicity.

		Example:
		return queryInterface.bulkDelete('People', null, {});
		*/

		await Permission.destroy({
			where: {},
			truncate: true
		});
	}
};
