const { MoleculerClientError } = require("moleculer").Errors;

const canAccessArticle = {
	params: {
		article_id: {
			type: "number",
			optional: true
		}
	},
	async handler(ctx) {
		try {
			if (ctx.meta && ctx.meta.user) {
				const article = await ctx.call("blogs.resolveArticle", {
					id: ctx.params.article_id
				});
				if (!article || ctx.meta.user.id != article.user_id) {
					throw new MoleculerClientError(
						"Unauthorized Access!",
						403,
						"user_id"
					);
				}
			}
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = canAccessArticle;
