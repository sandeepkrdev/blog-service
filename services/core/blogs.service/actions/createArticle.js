const Article = require(process.cwd() + "/models").article;
const { MoleculerClientError } = require("moleculer").Errors;

async function checkSlugExists(ctx) {
	const slug = await ctx.call("blogs.resolveArticle", {
		slug: ctx.params.input.slug
	});
	if (slug) {
		throw new MoleculerClientError("Slug already exists", 400, "slug");
	}
}

const createArticle = {
	graphql: {
		mutation: "createArticle(input: ArticleInput!): Article!"
	},
	async handler(ctx) {
		try {
			await checkSlugExists(ctx);
			return await Article.create({
				user_id: ctx.meta.user.id,
				slug: ctx.params.input.slug
			});
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = createArticle;
