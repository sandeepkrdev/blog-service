const ArticleBookmark = require(process.cwd() + "/models").article_bookmark;
const { MoleculerClientError } = require("moleculer").Errors;

async function getArticleAndValidate(ctx) {
	const article = await ctx.call("blogs.resolveArticle", {
		id: ctx.params.input.article_id
	});
	if (!article) {
		throw new MoleculerClientError("Article not found", 400, "article_id");
	}
	return article;
}

async function getUserAndValidate(ctx) {
	const user = await ctx.call("users.resolveUser", {
		id: ctx.params.input.user_id
	});
	if (!user) {
		throw new MoleculerClientError("User not found", 400, "user_id");
	}
}

const createArticleBookmark = {
	graphql: {
		mutation:
			"createArticleBookmark(input: ArticleBookmarkInput!): ArticleBookmark!"
	},
	async handler(ctx) {
		const article = await getArticleAndValidate(ctx);
		if (ctx.params.input.user_id) await getUserAndValidate(ctx);
		const input = {
			article_id: article.id,
			user_id: ctx.params.input.user_id
				? ctx.params.input.user_id
				: ctx.meta.user.id
		};
		let articleBookArticleBookmark = await ArticleBookmark.findOne({
			where: input
		});
		if (!articleBookArticleBookmark) {
			articleBookArticleBookmark = await ArticleBookmark.create(input);
		} else {
			await articleBookArticleBookmark.update(ctx.params.input);
		}
		return articleBookArticleBookmark;
	}
};

module.exports = createArticleBookmark;
