const ArticleComment = require(process.cwd() + "/models").article_comment;
const { MoleculerClientError } = require("moleculer").Errors;

async function getArticleAndValidate(ctx) {
	const article = await ctx.call("blogs.resolveArticle", {
		id: ctx.params.input.article_id
	});
	if (!article) {
		throw new MoleculerClientError("Article not found", 400, "article_id");
	}
}

async function getParentAndValidate(ctx) {
	const parent = await ctx.call("blogs.resolveArticleComment", {
		id: ctx.params.input.parent_id
	});
	if (!parent) {
		throw new MoleculerClientError("Parent not found", 400, "parent_id");
	}
}

const createArticleComment = {
	graphql: {
		mutation:
			"createArticleComment(input: ArticleCommentInput!): ArticleComment!"
	},
	async handler(ctx) {
		await getArticleAndValidate(ctx);
		if (ctx.params.input.parent_id) await getParentAndValidate(ctx);
		return await ArticleComment.create({
			article_id: ctx.params.input.article_id,
			text: ctx.params.input.text,
			parent_id: ctx.params.input.parent_id
				? ctx.params.input.parent_id
				: 0,
			user_id: ctx.meta.user.id
		});
	}
};

module.exports = createArticleComment;
