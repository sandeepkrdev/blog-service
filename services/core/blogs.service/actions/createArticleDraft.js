const ArticleDraft = require(process.cwd() + "/models").article_draft;
const { MoleculerClientError } = require("moleculer").Errors;

async function getArticleAndValidate(ctx) {
	const article = await ctx.call("blogs.resolveArticle", {
		id: ctx.params.input.article_id
	});
	if (!article) {
		throw new MoleculerClientError("Article not found", 400, "article_id");
	}
	return article;
}

async function checkArticleAlreadyExistsInArticleDraft(ctx, article_id) {
	const articleDraft = await ctx.call("blogs.resolveArticleDraft", {
		article_id: article_id
	});
	if (articleDraft) {
		throw new MoleculerClientError(
			"Draft of this article already exists",
			400,
			"article_id"
		);
	}
}

const createArticleDraft = {
	graphql: {
		mutation:
			"createArticleDraft(input: ArticleStatusInput!): ArticleDraft!"
	},
	async handler(ctx) {
		try {
			const article = await getArticleAndValidate(ctx);
			await checkArticleAlreadyExistsInArticleDraft(ctx, article.id);
			const articleDraft = await ArticleDraft.create(ctx.params.input);
			return articleDraft;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = createArticleDraft;
