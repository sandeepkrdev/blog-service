const ArticleHistory = require(process.cwd() + "/models").article_history;
const { MoleculerClientError } = require("moleculer").Errors;

async function getArticleAndValidate(ctx) {
	const article = await ctx.call("blogs.resolveArticle", {
		id: ctx.params.input.article_id
	});
	if (!article) {
		throw new MoleculerClientError("Article not found", 400, "article_id");
	}
	return article;
}

const createArticleHistory = {
	params: {
		input: { type: "object" }
	},
	async handler(ctx) {
		try {
			await getArticleAndValidate(ctx);
			const articleHistory = await ArticleHistory.create(
				ctx.params.input
			);
			return articleHistory;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = createArticleHistory;
