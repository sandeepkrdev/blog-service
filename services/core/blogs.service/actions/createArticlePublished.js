const moment = require("moment");
const ArticlePublished = require(process.cwd() + "/models").article_published;
const { brokerBroadCast } = require(process.cwd() + "/utils/helpers");
const { MoleculerClientError } = require("moleculer").Errors;

async function getArticleAndValidate(ctx) {
	const article = await ctx.call("blogs.resolveArticle", {
		id: ctx.params.input.article_id
	});
	if (!article) {
		throw new MoleculerClientError("Article not found", 400, "article_id");
	}
	return article;
}

async function checkArticleAlreadyExistsInArticlePublished(ctx, article_id) {
	const articlePublished = await ctx.call("blogs.resolveArticlePublished", {
		article_id: article_id
	});
	if (articlePublished) {
		throw new MoleculerClientError(
			"This article has already been published",
			400,
			"article_id"
		);
	}
}

const createArticlePublished = {
	params: {
		input: { type: "object" }
	},
	async handler(ctx) {
		try {
			const article = await getArticleAndValidate(ctx);
			await checkArticleAlreadyExistsInArticlePublished(ctx, article.id);
			const articlePublished = await ArticlePublished.create({
				...ctx.params.input,
				publishing_date: moment()
			});
			brokerBroadCast(ctx, "blogs", "article.published.created", {
				id: article.id,
				user_id: ctx.meta && ctx.meta.user ? ctx.meta.user.id : 0
			});
			return articlePublished;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = createArticlePublished;
