const ArticleReaction = require(process.cwd() + "/models").article_reaction;
const { MoleculerClientError } = require("moleculer").Errors;
const { brokerBroadCast } = require(process.cwd() + "/utils/helpers");

const validateArticle = async ctx => {
	const article = await ctx.call("blogs.resolveArticle", {
		id: ctx.params.input.article_id
	});
	if (!article) {
		throw new MoleculerClientError(
			"Article does not exist",
			400,
			"article_id"
		);
	}
};

const validateMasterReaction = async ctx => {
	const masterReaction = await ctx.call("blogs.resolveMasterReaction", {
		id: ctx.params.input.master_reaction_id
	});
	if (!masterReaction) {
		throw new MoleculerClientError(
			"Master Reaction does not exist",
			400,
			"master_reaction_id"
		);
	}
};

async function existingArticleReaction(ctx) {
	return await ArticleReaction.findOne({
		where: {
			article_id: ctx.params.input.article_id,
			master_reaction_id: ctx.params.input.master_reaction_id,
			user_id: ctx.meta.user.id
		}
	});
}

const createArticleReaction = {
	graphql: {
		mutation:
			"createArticleReaction(input: ArticleReactionInput!): ArticleReaction!"
	},
	async handler(ctx) {
		try {
			await validateArticle(ctx);
			await validateMasterReaction(ctx);
			let articleReaction = await existingArticleReaction(ctx);
			if (!articleReaction) {
				articleReaction = await ArticleReaction.create({
					article_id: ctx.params.input.article_id,
					master_reaction_id: ctx.params.input.master_reaction_id,
					user_id: ctx.meta.user.id
				});
				brokerBroadCast(ctx, "blogs", "article.reaction.added", {
					article_id: ctx.params.input.article_id,
					master_reaction_id: ctx.params.input.master_reaction_id
				});
			}
			return articleReaction;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = createArticleReaction;
