const { MoleculerClientError } = require("moleculer").Errors;
const { brokerBroadCast } = require(process.cwd() + "/utils/helpers");
const ArticleRequest = require(process.cwd() + "/models").article_request;

async function validateUser(ctx, articleId) {
	await ctx.call("blogs.canAccessArticle", {
		article_id: articleId
	});
}

async function getArticleAndValidate(ctx) {
	const article = await ctx.call("blogs.resolveArticle", {
		id: ctx.params.input.article_id
	});
	if (!article) {
		throw new MoleculerClientError("Article not found", 400, "article_id");
	}
	return article;
}

async function checkArticleRequestPendingAlreadyExists(ctx, article_id) {
	const articleRequest = await ctx.call("blogs.resolveArticleRequest", {
		article_id: article_id,
		status: "pending"
	});
	if (articleRequest) {
		throw new MoleculerClientError(
			"Our editorial team is reviewing this article.",
			400,
			"article_id"
		);
	}
}

const createArticleRequest = {
	graphql: {
		mutation:
			"createArticleRequest(input: ArticleRequestInput!): ArticleRequest!"
	},
	async handler(ctx) {
		try {
			await validateUser(ctx, ctx.params.input.article_id);
			const article = await getArticleAndValidate(ctx);
			await checkArticleRequestPendingAlreadyExists(ctx, article.id);
			const articleRequest = await ArticleRequest.create(ctx.params.input);
			brokerBroadCast(ctx, "blogs", "article.request.created", {
				id: article.id,
				article_request_id: articleRequest.id,
				user_id: ctx.meta && ctx.meta.user ? ctx.meta.user.id : 0
			});
			return articleRequest;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = createArticleRequest;
