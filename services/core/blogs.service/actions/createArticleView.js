const ArticleView = require(process.cwd() + "/models").article_view;
const { brokerBroadCast } = require(process.cwd() + "/utils/helpers");

async function getArticle(ctx) {
	return await ctx.call("blogs.resolveArticle", {
		id: ctx.params.input.article_id
	});
}

const createArticleView = {
	graphql: {
		mutation: "createArticleView(input: ArticleViewInput!): ArticleView!"
	},
	async handler(ctx) {
		try {
			const article = await getArticle(ctx);
			if (article) {
				const articleView = await ArticleView.create(ctx.params.input);
				brokerBroadCast(ctx, "blogs", "article.view.created", ctx.params.input);
				return articleView;
			}
		} catch(e)  {
			//
		}
	}
};

module.exports = createArticleView;
