const MasterReaction = require(process.cwd() + "/models").master_reaction;

async function validateUser(ctx) {
	await ctx.call("users.hasPermission", {
		permissionName: "manage-reactions"
	});
}

const createMasterReaction = {
	graphql: {
		mutation:
			"createMasterReaction(input: MasterReactionInput!): MasterReaction!"
	},
	async handler(ctx) {
		await validateUser(ctx);
		try {
			return await MasterReaction.create(ctx.params.input);
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = createMasterReaction;
