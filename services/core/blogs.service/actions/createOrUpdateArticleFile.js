const ArticleFile = require(process.cwd() + "/models").article_file;
const { MoleculerClientError } = require("moleculer").Errors;

async function validateUser(ctx, articleId) {
	await ctx.call("blogs.canAccessArticle", {
		article_id: articleId
	});
}

async function getArticleAndValidate(ctx) {
	const article = await ctx.call("blogs.resolveArticle", {
		id: ctx.params.input.article_id
	});
	if (!article) {
		throw new MoleculerClientError("Article not found", 400, "article_id");
	}
	return article;
}

const createOrUpdateArticleFile = {
	graphql: {
		mutation:
			"createOrUpdateArticleFile(input: ArticleFileInput!): ArticleFile!"
	},
	async handler(ctx) {
		try {
			await validateUser(ctx, ctx.params.input.article_id);
			const article = await getArticleAndValidate(ctx);
			const input = {
				article_id: article.id,
				file_url: ctx.params.input.file_url
			};
			let articleFile = await ArticleFile.findOne({
				where: input
			});
			if (!articleFile) {
				articleFile = await ArticleFile.create(ctx.params.input);
			} else {
				await articleFile.update(ctx.params.input);
			}
			return articleFile;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = createOrUpdateArticleFile;
