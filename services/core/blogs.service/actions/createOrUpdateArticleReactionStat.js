const ArticleReactionStat = require(process.cwd() + "/models").article_reaction_stat;

const createOrUpdateArticleReactionStat = {
	params: {
		article_id: { type: "number" },
		master_reaction_id: { type: "number" },
		removed: { type: "boolean", optional: true },
	},
	async handler(ctx) {
		try {
			const articleReactionStat = await ctx.call(
				"blogs.resolveArticleReactionStat",
				{
					article_id: ctx.params.article_id,
					master_reaction_id: ctx.params.master_reaction_id
				}
			);
			if (!ctx.params.removed) {
				if (articleReactionStat) {
					await articleReactionStat.update({
						count: articleReactionStat.count + 1
					});
				} else {
					await ArticleReactionStat.create({
						article_id: ctx.params.article_id,
						master_reaction_id: ctx.params.master_reaction_id,
						count: 1
					});
				}
			} else {
				// removed
				if (articleReactionStat && articleReactionStat.count > 0) {
					await articleReactionStat.update({
						count: articleReactionStat.count - 1
					});
				}
			}
		} catch (e) {
			//
		}
	}
};

module.exports = createOrUpdateArticleReactionStat;
