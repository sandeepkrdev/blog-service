const ArticleViewStat = require(process.cwd() + "/models").article_view_stat;

const createOrUpdateArticleViewStat = {
	params: {
		article_id: { type: "number" },
		view_id: { type: "string" }
	},
	async handler(ctx) {
		try {
			const articleViewStat = await ctx.call("blogs.resolveArticleViewStat", {
				article_id: ctx.params.article_id,
				view_id: ctx.params.view_id
			});
			if (articleViewStat) {
				await articleViewStat.update({
					count: articleViewStat.count + 1
				});
			} else {
				await ArticleViewStat.create({
					article_id: ctx.params.article_id,
					view_id: ctx.params.view_id,
					count: 1
				});
			}
		} catch (e) {
			//
		}
	}
};

module.exports = createOrUpdateArticleViewStat;
