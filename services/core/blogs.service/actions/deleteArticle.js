async function getArticle(ctx) {
	return await ctx.call("blogs.resolveArticle", {
		id: ctx.params.id
	});
}

async function getArticlePublished(ctx, article) {
	return await ctx.call("blogs.resolveArticlePublished", {
		article_id: article.id
	});
}

async function getArticleHistory(ctx, article) {
	return await ctx.call("blogs.resolveArticleHistory", {
		article_id: article.id
	});
}

const deleteArticle = {
	params: {
		id: { type: "number" }
	},
	async handler(ctx) {
		try {
			const article = await getArticle(ctx);
			if (article) {
				const published = await getArticlePublished(ctx, article);
				if (!published) {
					const histories = await getArticleHistory(ctx, article);
					if (!histories || !histories.length) {
						await article.destroy();
					}
				}
			}
			return { message: "Article deleted successfully!" };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = deleteArticle;
