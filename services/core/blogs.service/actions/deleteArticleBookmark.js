async function getArticleBookmark(ctx) {
	return await ctx.call("blogs.resolveArticleBookmark", {
		id: ctx.params.id
	});
}

const deleteArticleBookmark = {
	graphql: {
		mutation: "deleteArticleBookmark(id: Int!): Response!"
	},
	async handler(ctx) {
		try {
			const articleBookmark = await getArticleBookmark(ctx);
			if (articleBookmark) {
				await articleBookmark.destroy();
			}
			return { message: "Article Bookmark deleted successfully!" };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = deleteArticleBookmark;
