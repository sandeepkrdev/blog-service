const { MoleculerClientError } = require("moleculer").Errors;

async function getArticleCommentAndValidate(ctx) {
	const articleComment = await ctx.call("blogs.resolveArticleComment", {
		id: ctx.params.id
	});
	if (!articleComment) {
		throw new MoleculerClientError("Article Comment not found", 400, "id");
	}
	return articleComment;
}

async function canAccessArticleComment(ctx, user_id) {
	if (user_id != ctx.meta.user.id) {
		throw new MoleculerClientError(
			"Cannot update someone else's comment",
			400,
			"id"
		);
	}
}

const deleteArticleComment = {
	graphql: {
		mutation: "deleteArticleComment(id: Int!): Response!"
	},
	async handler(ctx) {
		try {
			let articleComment = await getArticleCommentAndValidate(ctx);
			await canAccessArticleComment(ctx, articleComment.user_id);
			await articleComment.destroy();
			return { message: "Article Comment deleted successfully!" };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = deleteArticleComment;
