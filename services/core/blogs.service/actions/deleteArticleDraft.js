const { MoleculerClientError } = require("moleculer").Errors;
const { brokerBroadCast } = require(process.cwd() + "/utils/helpers");

async function validateUser(ctx, articleId) {
	await ctx.call("blogs.canAccessArticle", {
		article_id: articleId
	});
}

async function getArticleDraftAndValidate(ctx) {
	const articleDraft = await ctx.call("blogs.resolveArticleDraft", {
		id: ctx.params.id
	});
	if (!articleDraft) {
		throw new MoleculerClientError("Article Draft not found", 400, "id");
	}
	return articleDraft;
}

const deleteArticleDraft = {
	graphql: {
		mutation: "deleteArticleDraft(id: Int!): Response!"
	},
	async handler(ctx) {
		try {
			const articleDraft = await getArticleDraftAndValidate(ctx);
			await validateUser(ctx, articleDraft.article_id);
			brokerBroadCast(ctx, "blogs", "article.draft.deleted", {
				id: articleDraft.article_id,
				user_id: ctx.meta && ctx.meta.user ? ctx.meta.user.id : 0
			});
			await articleDraft.destroy();
			return { message: "Article Draft deleted successfully!" };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = deleteArticleDraft;
