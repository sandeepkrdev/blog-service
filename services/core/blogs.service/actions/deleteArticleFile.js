const { MoleculerClientError } = require("moleculer").Errors;

async function validateUser(ctx, articleId) {
	await ctx.call("blogs.canAccessArticle", {
		article_id: articleId
	});
}

async function getArticleFileAndValidate(ctx) {
	const articleFile = await ctx.call("blogs.resolveArticleFile", {
		id: ctx.params.id
	});
	if (!articleFile) {
		throw new MoleculerClientError("Article File not found", 400, "id");
	}
	return articleFile;
}

const deleteArticleFile = {
	graphql: {
		mutation: "deleteArticleFile(id: Int!): Response!"
	},
	async handler(ctx) {
		try {
			const articleFile = await getArticleFileAndValidate(ctx);
			await validateUser(ctx, articleFile.article_id);
			await articleFile.destroy();
			return { message: "Article File deleted successfully!" };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = deleteArticleFile;
