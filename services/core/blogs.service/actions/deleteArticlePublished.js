const { MoleculerClientError } = require("moleculer").Errors;

async function validateUser(ctx, articleId) {
	await ctx.call("blogs.canAccessArticle", {
		article_id: articleId
	});
}

async function getArticlePublishedAndValidate(ctx) {
	const articlePublished = await ctx.call("blogs.resolveArticlePublished", {
		id: ctx.params.id
	});
	if (!articlePublished) {
		throw new MoleculerClientError(
			"Article Published not found",
			400,
			"id"
		);
	}
	return articlePublished;
}

const deleteArticlePublished = {
	params: {
		id: { type: "number" }
	},
	async handler(ctx) {
		try {
			const articlePublished = await getArticlePublishedAndValidate(ctx);
			await validateUser(ctx, articlePublished.article_id);
			await articlePublished.destroy();
			return { message: "Article Published deleted successfully!" };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = deleteArticlePublished;
