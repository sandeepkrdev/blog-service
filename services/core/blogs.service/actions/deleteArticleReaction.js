const ArticleReaction = require(process.cwd() + "/models").article_reaction;
const { brokerBroadCast } = require(process.cwd() + "/utils/helpers");

const getAndValidateArticleReaction = async ctx => {
	return await ArticleReaction.findOne({
		where: {
			article_id: ctx.params.input.article_id,
			master_reaction_id: ctx.params.input.master_reaction_id,
			user_id: ctx.meta.user.id
		}
	});
};

const deleteArticleReaction = {
	graphql: {
		mutation:
			"deleteArticleReaction(input: ArticleReactionInput!): Response!"
	},
	async handler(ctx) {
		try {
			let articleReaction = await getAndValidateArticleReaction(ctx);
			if (articleReaction) {
				await articleReaction.destroy();
				brokerBroadCast(ctx, "blogs", "article.reaction.removed", {
					article_id: ctx.params.input.article_id,
					master_reaction_id: ctx.params.input.master_reaction_id
				});
			}
			return { message: "Article Reaction deleted successfully" };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = deleteArticleReaction;
