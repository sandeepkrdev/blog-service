const { MoleculerClientError } = require("moleculer").Errors;

async function validateUser(ctx, articleId) {
	try {
		await ctx.call("blogs.canAccessArticle", {
			article_id: articleId
		});
	} catch (e) {
		await ctx.call("users.hasPermission", {
			permissionName: "can-publish-articles"
		});
	}
}

async function getArticleRequestAndValidate(ctx) {
	const articleRequest = await ctx.call("blogs.resolveArticleRequest", {
		id: ctx.params.id
	});
	if (!articleRequest) {
		throw new MoleculerClientError("Article Request not found", 400, "id");
	}
	return articleRequest;
}

const deleteArticleRequest = {
	graphql: {
		mutation: "deleteArticleRequest(id: Int!): Response!"
	},
	async handler(ctx) {
		try {
			const articleRequest = await getArticleRequestAndValidate(ctx);
			await validateUser(ctx, articleRequest.article_id);
			await articleRequest.destroy();
			return { message: "Article Request deleted successfully!" };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = deleteArticleRequest;
