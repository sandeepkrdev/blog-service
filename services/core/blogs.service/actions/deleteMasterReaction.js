const { MoleculerClientError } = require("moleculer").Errors;

async function validateUser(ctx) {
	await ctx.call("users.hasPermission", {
		permissionName: "manage-reactions"
	});
}

const getAndValidateMasterReaction = async ctx => {
	const masterReaction = await ctx.call("blogs.resolveMasterReaction", {
		id: ctx.params.id
	});
	if (!masterReaction) {
		throw new MoleculerClientError("Master Reaction not found", 400, "id");
	}
	return masterReaction;
};

const deleteMasterReaction = {
	graphql: {
		mutation: "deleteMasterReaction(id: Int!): Response!"
	},
	async handler(ctx) {
		await validateUser(ctx);
		try {
			let masterReaction = await getAndValidateMasterReaction(ctx);
			await masterReaction.destroy();
			return { message: "Master Reaction deleted successfully!" };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = deleteMasterReaction;
