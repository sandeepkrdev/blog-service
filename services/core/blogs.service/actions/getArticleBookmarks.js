const { PER_PAGE, PAGE_NO } = require(process.cwd() + "/constants/misc");
const ArticleBookmark = require(process.cwd() + "/models").article_bookmark;

async function getPageNo(ctx) {
	return ctx.params.filters && ctx.params.filters.page_no
		? ctx.params.filters.page_no
		: PAGE_NO;
}

async function getPerPage(ctx) {
	return ctx.params.filters &&
		ctx.params.filters.per_page &&
		ctx.params.filters.per_page <= PER_PAGE
		? ctx.params.filters.per_page
		: PER_PAGE;
}

const getArticleBookmarks = {
	graphql: {
		query:
			"getArticleBookmarks(filters: ArticleBookmarkFilters): ArticleBookmarkDataOutput"
	},
	async handler(ctx) {
		const pageNo = await getPageNo(ctx);
		const perPage = await getPerPage(ctx);
		let whereObj = [],
			orderByObj = [];
		try {
			if (ctx.params.filters) {
				if (
					ctx.params.filters.id &&
					ctx.params.filters.id.length != 0
				) {
					whereObj.push({
						id: ctx.params.filters.id
					});
				}
				if (
					ctx.params.filters.article_id &&
					ctx.params.filters.article_id.length != 0
				) {
					whereObj.push({
						article_id: ctx.params.filters.article_id
					});
				}
				if (
					ctx.params.filters.user_id &&
					ctx.params.filters.user_id.length != 0
				) {
					whereObj.push({
						user_id: ctx.params.filters.user_id
					});
				}
				if (ctx.params.filters.orderBy) {
					if (ctx.params.filters.orderBy.id) {
						orderByObj.push(["id", ctx.params.filters.orderBy.id]);
					}
					if (ctx.params.filters.orderBy.createdAt) {
						orderByObj.push([
							"createdAt",
							ctx.params.filters.orderBy.createdAt
						]);
					}
				}
			}
			let count = await ArticleBookmark.count({
				where: whereObj,
				order: orderByObj
			});
			let articleBookmarks = await ArticleBookmark.findAll({
				where: whereObj,
				order: orderByObj,
				offset: (pageNo - 1) * perPage,
				limit: perPage
			});
			return {
				data: articleBookmarks,
				meta: {
					total: count,
					per_page: perPage,
					page_no: pageNo,
					next_data: count > perPage * pageNo
				}
			};
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = getArticleBookmarks;
