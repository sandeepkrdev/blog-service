const { PER_PAGE, PAGE_NO } = require(process.cwd() + "/constants/misc");
const ArticleFile = require(process.cwd() + "/models").article_file;

async function getPageNo(ctx) {
	return ctx.params.filters && ctx.params.filters.page_no
		? ctx.params.filters.page_no
		: PAGE_NO;
}

async function getPerPage(ctx) {
	return ctx.params.filters &&
		ctx.params.filters.per_page &&
		ctx.params.filters.per_page <= PER_PAGE
		? ctx.params.filters.per_page
		: PER_PAGE;
}

const getArticleFiles = {
	graphql: {
		query:
			"getArticleFiles(filters: ArticleFileFilters): ArticleFileDataOutput"
	},
	async handler(ctx) {
		const pageNo = await getPageNo(ctx);
		const perPage = await getPerPage(ctx);
		let whereObj = [];
		try {
			if (ctx.params.filters) {
				if (
					ctx.params.filters.id &&
					ctx.params.filters.id.length != 0
				) {
					whereObj.push({
						id: ctx.params.filters.id
					});
				}
				if (
					ctx.params.filters.article_id &&
					ctx.params.filters.article_id.length != 0
				) {
					whereObj.push({
						article_id: ctx.params.filters.article_id
					});
				}
				if (ctx.params.filters.file_url) {
					whereObj.push({
						file_url: ctx.params.filters.file_url
					});
				}
				if (ctx.params.filters.is_file_used) {
					whereObj.push({
						is_file_used: ctx.params.filters.is_file_used
					});
				}
			}
			let count = await ArticleFile.count({
				where: whereObj
			});
			let articleFiles = await ArticleFile.findAll({
				where: whereObj,
				offset: (pageNo - 1) * perPage,
				limit: perPage
			});
			return {
				data: articleFiles,
				meta: {
					total: count,
					per_page: perPage,
					page_no: pageNo,
					next_data: count > perPage * pageNo
				}
			};
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = getArticleFiles;
