const { PER_PAGE, PAGE_NO } = require(process.cwd() + "/constants/misc");
const ArticleReactionStat = require(process.cwd() + "/models")
	.article_reaction_stat;

async function getPageNo(ctx) {
	return ctx.params.filters && ctx.params.filters.page_no
		? ctx.params.filters.page_no
		: PAGE_NO;
}

async function getPerPage(ctx) {
	return ctx.params.filters &&
		ctx.params.filters.per_page &&
		ctx.params.filters.per_page <= PER_PAGE
		? ctx.params.filters.per_page
		: PER_PAGE;
}

const getArticleReactionStats = {
	graphql: {
		query:
			"getArticleReactionStats(filters: ArticleReactionStatFilters): ArticleReactionStatDataOutput"
	},
	async handler(ctx) {
		const pageNo = await getPageNo(ctx);
		const perPage = await getPerPage(ctx);
		let whereObj = [];
		try {
			if (ctx.params.filters) {
				if (
					ctx.params.filters.id &&
					ctx.params.filters.id.length != 0
				) {
					whereObj.push({
						id: ctx.params.filters.id
					});
				}
				if (
					ctx.params.filters.article_id &&
					ctx.params.filters.article_id.length != 0
				) {
					whereObj.push({
						article_id: ctx.params.filters.article_id
					});
				}
				if (
					ctx.params.filters.master_reaction_id &&
					ctx.params.filters.master_reaction_id.length != 0
				) {
					whereObj.push({
						master_reaction_id:
							ctx.params.filters.master_reaction_id
					});
				}
			}
			let count = await ArticleReactionStat.count({
				where: whereObj
			});
			let articleReactionStats = await ArticleReactionStat.findAll({
				where: whereObj,
				offset: (pageNo - 1) * perPage,
				limit: perPage
			});
			return {
				data: articleReactionStats,
				meta: {
					total: count,
					per_page: perPage,
					page_no: pageNo,
					next_data: count > perPage * pageNo
				}
			};
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = getArticleReactionStats;
