const { PER_PAGE, PAGE_NO } = require(process.cwd() + "/constants/misc");
const ArticleReaction = require(process.cwd() + "/models").article_reaction;

async function getPageNo(ctx) {
	return ctx.params.filters && ctx.params.filters.page_no
		? ctx.params.filters.page_no
		: PAGE_NO;
}

async function getPerPage(ctx) {
	return ctx.params.filters &&
		ctx.params.filters.per_page &&
		ctx.params.filters.per_page <= PER_PAGE
		? ctx.params.filters.per_page
		: PER_PAGE;
}

const getArticleReactions = {
	graphql: {
		query:
			"getArticleReactions(filters: ArticleReactionFilters): ArticleReactionDataOutput!"
	},
	async handler(ctx) {
		try {
			const pageNo = await getPageNo(ctx);
			const perPage = await getPerPage(ctx);
			let whereObj = [];
			if (ctx.params.filters) {
				if (
					ctx.params.filters.id &&
					ctx.params.filters.id.length != 0
				) {
					whereObj.push({
						id: ctx.params.filters.id
					});
				}
				if (
					ctx.params.filters.article_id &&
					ctx.params.filters.article_id.length != 0
				) {
					whereObj.push({
						article_id: ctx.params.filters.article_id
					});
				}
				if (
					ctx.params.filters.master_reaction_id &&
					ctx.params.filters.master_reaction_id.length != 0
				) {
					whereObj.push({
						master_reaction_id:
							ctx.params.filters.master_reaction_id
					});
				}
				if (
					ctx.params.filters.user_id &&
					ctx.params.filters.user_id.length != 0
				) {
					whereObj.push({
						user_id: ctx.params.filters.user_id
					});
				}
			}
			let count = await ArticleReaction.count({
				where: whereObj
			});
			let articleReactions = await ArticleReaction.findAll({
				where: whereObj,
				offset: (pageNo - 1) * perPage,
				limit: perPage
			});
			return {
				data: articleReactions,
				meta: {
					total: count,
					per_page: perPage,
					page_no: pageNo,
					next_data: count > perPage * pageNo
				}
			};
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = getArticleReactions;
