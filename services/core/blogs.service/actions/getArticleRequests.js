const sequelize = require("sequelize");
const { PER_PAGE, PAGE_NO } = require(process.cwd() + "/constants/misc");
const Article = require(process.cwd() + "/models").article;
const ArticleRequest = require(process.cwd() + "/models").article_request;

async function getPageNo(ctx) {
	return ctx.params.filters && ctx.params.filters.page_no
		? ctx.params.filters.page_no
		: PAGE_NO;
}

async function getPerPage(ctx) {
	return ctx.params.filters &&
		ctx.params.filters.per_page &&
		ctx.params.filters.per_page <= PER_PAGE
		? ctx.params.filters.per_page
		: PER_PAGE;
}

const getArticleRequests = {
	graphql: {
		query: "getArticleRequests(filters: ArticleRequestFilters): ArticleRequestDataOutput!"
	},
	async handler(ctx) {
		const pageNo = await getPageNo(ctx);
		const perPage = await getPerPage(ctx);
		let whereObj = [],
			includeObj = [],
			orderByObj = [];
		try {
			if (ctx.params.filters) {
				if (ctx.params.filters.id) {
					whereObj.push({
						id: ctx.params.filters.id
					});
				}
				if (ctx.params.filters.article_id) {
					whereObj.push({
						article_id: ctx.params.filters.article_id
					});
				}
				if (ctx.params.filters.status) {
					whereObj.push({
						status: ctx.params.filters.status
					});
				}
				if (ctx.params.filters.approved_by) {
					whereObj.push({
						approved_by: ctx.params.filters.approved_by
					});
				}
				if (ctx.params.filters.user_id) {
					includeObj.push({
						model: Article,
						as: "article",
						where: {
							user_id: ctx.params.filters.user_id
						}
					});
				}
				if (ctx.params.filters.search) {
					includeObj.push({
						model: Article,
						as: "article",
						where: {
							slug: {
								[sequelize.Op.like]:
									"%" + ctx.params.filters.search + "%"
							}
						}
					});
				}
			}
			if (ctx.params.filters.orderBy) {
				if (ctx.params.filters.orderBy.createdAt) {
					orderByObj.push([
						"createdAt",
						ctx.params.filters.orderBy.createdAt
					]);
				}
			}

			let count = await ArticleRequest.count({
				where: whereObj,
				include: includeObj,
				order: orderByObj
			});
			let articleRequests = await ArticleRequest.findAll({
				where: whereObj,
				include: includeObj,
				order: orderByObj,
				offset: (pageNo - 1) * perPage,
				limit: perPage
			});

			return {
				data: articleRequests,
				meta: {
					total: count,
					per_page: perPage,
					page_no: pageNo,
					next_data: count > perPage * pageNo
				}
			};
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = getArticleRequests;
