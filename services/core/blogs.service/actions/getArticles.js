const sequelize = require("sequelize");
const Article = require(process.cwd() + "/models").article;
const { PER_PAGE, PAGE_NO } = require(process.cwd() + "/constants/misc");

async function getPageNo(ctx) {
	return ctx.params.filters && ctx.params.filters.page_no
		? ctx.params.filters.page_no
		: PAGE_NO;
}

async function getPerPage(ctx) {
	return ctx.params.filters &&
		ctx.params.filters.per_page &&
		ctx.params.filters.per_page <= PER_PAGE
		? ctx.params.filters.per_page
		: PER_PAGE;
}

const getArticles = {
	graphql: {
		query: "getArticles(filters: ArticleFilters): ArticleDataOutput!"
	},
	async handler(ctx) {
		const pageNo = await getPageNo(ctx);
		const perPage = await getPerPage(ctx);
		let whereObj = [];
		try {
			if (ctx.params.filters) {
				if (ctx.params.filters.id) {
					whereObj.push({
						id: ctx.params.filters.id
					});
				}
				if (ctx.params.filters.user_id) {
					whereObj.push({
						user_id: ctx.params.filters.user_id
					});
				}
				if (ctx.params.filters.company_id) {
					whereObj.push({
						company_id: ctx.params.filters.company_id
					});
				}
				if (ctx.params.filters.slug) {
					whereObj.push({
						slug: ctx.params.filters.slug
					});
				}
				if (ctx.params.filters.search) {
					whereObj.push({
						slug: {
							[sequelize.Op.like]:
								"%" + ctx.params.filters.search + "%"
						}
					});
				}
			}

			let count = await Article.count({
				where: whereObj
			});
			let articles = await Article.findAll({
				where: whereObj,
				offset: (pageNo - 1) * perPage,
				limit: perPage
			});

			return {
				data: articles,
				meta: {
					total: count,
					per_page: perPage,
					page_no: pageNo,
					next_data: count > perPage * pageNo
				}
			};
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = getArticles;
