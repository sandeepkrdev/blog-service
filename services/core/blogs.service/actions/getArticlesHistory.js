const sequelize = require("sequelize");
const Article = require(process.cwd() + "/models").article;
const ArticleHistory = require(process.cwd() + "/models").article_history;
const { PER_PAGE, PAGE_NO } = require(process.cwd() + "/constants/misc");

async function getPageNo(ctx) {
	return ctx.params.filters && ctx.params.filters.page_no
		? ctx.params.filters.page_no
		: PAGE_NO;
}

async function getPerPage(ctx) {
	return ctx.params.filters &&
		ctx.params.filters.per_page &&
		ctx.params.filters.per_page <= PER_PAGE
		? ctx.params.filters.per_page
		: PER_PAGE;
}

const getArticlesHistory = {
	graphql: {
		query:
			"getArticlesHistory(filters: ArticleStatusFilters): ArticleHistoryDataOutput!"
	},
	async handler(ctx) {
		const pageNo = await getPageNo(ctx);
		const perPage = await getPerPage(ctx);
		let whereObj = [],
			includeObj = [];
		try {
			if (ctx.params.filters) {
				if (
					ctx.params.filters.id &&
					ctx.params.filters.id.length != 0
				) {
					whereObj.push({
						id: ctx.params.filters.id
					});
				}
				if (
					ctx.params.filters.article_id &&
					ctx.params.filters.article_id.length != 0
				) {
					whereObj.push({
						article_id: ctx.params.filters.article_id
					});
				}
				if (
					ctx.params.filters.user_id &&
					ctx.params.filters.user_id.length != 0
				) {
					includeObj.push({
						model: Article,
						as: "article",
						where: {
							user_id: ctx.params.filters.user_id
						}
					});
				}
				if (
					ctx.params.filters.cover_image_position &&
					ctx.params.filters.cover_image_position.length != 0
				) {
					whereObj.push({
						cover_image_position:
							ctx.params.filters.cover_image_position
					});
				}
				if (ctx.params.filters.publishing_date) {
					whereObj.push({
						publishing_date: ctx.params.filters.publishing_date
					});
				}
				if (ctx.params.filters.allow_comments) {
					whereObj.push({
						allow_comments: ctx.params.filters.allow_comments
					});
				}
				if (ctx.params.filters.search) {
					whereObj.push({
						title: {
							[sequelize.Op.like]:
								"%" + ctx.params.filters.search + "%"
						}
					});
				}
			}
			let count = await ArticleHistory.count({
				where: whereObj,
				include: includeObj
			});
			let articleHistories = await ArticleHistory.findAll({
				where: whereObj,
				include: includeObj,
				offset: (pageNo - 1) * perPage,
				limit: perPage
			});

			return {
				data: articleHistories,
				meta: {
					total: count,
					per_page: perPage,
					page_no: pageNo,
					next_data: count > perPage * pageNo
				}
			};
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = getArticlesHistory;
