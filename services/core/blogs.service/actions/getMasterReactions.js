const MasterReaction = require(process.cwd() + "/models").master_reaction;
const { PER_PAGE, PAGE_NO } = require(process.cwd() + "/constants/misc");

async function getPageNo(ctx) {
	return ctx.params.filters && ctx.params.filters.page_no
		? ctx.params.filters.page_no
		: PAGE_NO;
}

async function getPerPage(ctx) {
	return ctx.params.filters &&
		ctx.params.filters.per_page &&
		ctx.params.filters.per_page <= PER_PAGE
		? ctx.params.filters.per_page
		: PER_PAGE;
}

const getMasterReactions = {
	graphql: {
		query:
			"getMasterReactions(filters: MasterReactionFilters): MasterReactionOutput"
	},
	async handler(ctx) {
		try {
			const pageNo = await getPageNo(ctx);
			const perPage = await getPerPage(ctx);
			let whereObj = [];
			if (ctx.params.filters) {
				if (ctx.params.filters.id) {
					whereObj.push({
						id: ctx.params.filters.id
					});
				}
				if (
					ctx.params.filters.is_primary ||
					ctx.params.filters.is_primary === false
				) {
					whereObj.push({
						is_primary: ctx.params.filters.is_primary
					});
				}
			}
			let count = await MasterReaction.count({
				where: whereObj
			});
			let masterReactions = await MasterReaction.findAll({
				where: whereObj,
				offset: (pageNo - 1) * perPage,
				limit: perPage
			});
			return {
				data: masterReactions,
				meta: {
					total: count,
					per_page: perPage,
					page_no: pageNo,
					next_data: count > perPage * pageNo
				}
			};
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = getMasterReactions;
