async function getArticleDraft(ctx) {
	return await ctx.call("blogs.resolveArticleDraft", {
		id: ctx.params.article_draft_id
	});
}

const publishArticle = {
	params: {
		article_draft_id: { type: "number"},
	},
	async handler(ctx) {
		try {
			const articleDraft = await getArticleDraft(ctx);
			if (articleDraft) {
				this.waitForServices(["jobs"]).then(async () => {
					ctx.call("jobs.push", {
						name: "article.draft.action.publish",
						payload: {
							article_draft_id: articleDraft.id
						}
					});
				});
			}
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = publishArticle;
