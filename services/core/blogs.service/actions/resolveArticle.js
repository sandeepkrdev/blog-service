const Article = require(process.cwd() + "/models").article;

const resolveArticle = {
	params: {
		id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		slug: { type: "string", optional: true },
		user_id: {
			type: "number",
			optional: true
		}
	},
	async handler(ctx) {
		let whereObj = [];
		if (ctx.params.id) {
			if (!Array.isArray(ctx.params.id)) {
				return await Article.findByPk(ctx.params.id);
			}
			whereObj.push({
				id: ctx.params.id
			});
		}
		if (ctx.params.slug) {
			return await Article.findOne({
				where: {
					slug: ctx.params.slug
				}
			});
		}
		if (ctx.params.user_id) {
			whereObj.push({
				user_id: ctx.params.user_id
			});
		}
		return await Article.findAll({
			where: whereObj
		});
	}
};

module.exports = resolveArticle;
