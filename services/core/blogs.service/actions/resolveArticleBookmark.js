const ArticleBookmark = require(process.cwd() + "/models").article_bookmark;

const resolveArticleBookmark = {
	params: {
		id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		article_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		user_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		]
	},
	async handler(ctx) {
		let whereObj = [];
		if (ctx.params.id) {
			if (!Array.isArray(ctx.params.id)) {
				return await ArticleBookmark.findByPk(ctx.params.id);
			}
			whereObj.push({
				id: ctx.params.id
			});
		}
		if (ctx.params.article_id) {
			whereObj.push({
				article_id: ctx.params.article_id
			});
		}
		if (ctx.params.user_id) {
			whereObj.push({
				user_id: ctx.params.user_id
			});
		}
		return await ArticleBookmark.findAll({
			where: whereObj
		});
	}
};

module.exports = resolveArticleBookmark;
