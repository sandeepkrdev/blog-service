const ArticleComment = require(process.cwd() + "/models").article_comment;

const resolveArticleComment = {
	params: {
		id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		article_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		parent_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		user_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		]
	},
	async handler(ctx) {
		let whereObj = [];
		if (ctx.params.id) {
			if (!Array.isArray(ctx.params.id)) {
				return await ArticleComment.findByPk(ctx.params.id);
			}
			whereObj.push({
				id: ctx.params.id
			});
		}
		if (ctx.params.article_id && ctx.params.article_id.length != 0) {
			whereObj.push({
				article_id: ctx.params.article_id
			});
		}
		if (ctx.params.parent_id && ctx.params.parent_id.length != 0) {
			whereObj.push({
				parent_id: ctx.params.parent_id
			});
		}
		if (ctx.params.user_id && ctx.params.user_id.length != 0) {
			whereObj.push({
				user_id: ctx.params.user_id
			});
		}
		return await ArticleComment.findAll({
			where: whereObj
		});
	}
};

module.exports = resolveArticleComment;
