const ArticlePublished = require(process.cwd() + "/models").article_published;

const resolveArticlePublished = {
	params: {
		id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		article_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		]
	},
	async handler(ctx) {
		let whereObj = [];
		if (ctx.params.id) {
			if (!Array.isArray(ctx.params.id)) {
				return await ArticlePublished.findByPk(ctx.params.id);
			}
			whereObj.push({
				id: ctx.params.id
			});
		}
		if (ctx.params.article_id) {
			if (!Array.isArray(ctx.params.article_id)) {
				return await ArticlePublished.findOne({
					where: {
						article_id: ctx.params.article_id
					}
				});
			}
			whereObj.push({
				article_id: ctx.params.article_id
			});
		}
		return await ArticlePublished.findAll({
			where: whereObj
		});
	}
};

module.exports = resolveArticlePublished;
