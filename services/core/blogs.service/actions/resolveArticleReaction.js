const ArticleReaction = require(process.cwd() + "/models").article_reaction;

const resolveArticleReaction = {
	params: {
		id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		article_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		master_reaction_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		user_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		]
	},
	async handler(ctx) {
		let whereObj = [];
		if (ctx.params.id) {
			if (!Array.isArray(ctx.params.id)) {
				return await ArticleReaction.findByPk(ctx.params.id);
			}
			whereObj.push({
				id: ctx.params.id
			});
		}
		if (ctx.params.article_id) {
			whereObj.push({
				article_id: ctx.params.article_id
			});
		}
		if (ctx.params.master_reaction_id) {
			whereObj.push({
				master_reaction_id: ctx.params.master_reaction_id
			});
		}
		if (ctx.params.user_id) {
			whereObj.push({
				user_id: ctx.params.user_id
			});
		}
		if (
			Array.isArray(ctx.params.id) ||
			Array.isArray(ctx.params.article_id) ||
			Array.isArray(ctx.params.master_reaction_id) ||
			Array.isArray(ctx.params.user_id) ||
			!ctx.params.master_reaction_id ||
			!ctx.params.user_id
		) {
			return await ArticleReaction.findAll({
				where: whereObj
			});
		}
		return await ArticleReaction.findOne({
			where: whereObj
		});
	}
};

module.exports = resolveArticleReaction;
