const ArticleRequest = require(process.cwd() + "/models").article_request;

const resolveArticleRequest = {
	params: {
		id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		article_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		status: {
			type: "string",
			optional: true
		},
		return_array: {
			type: "boolean",
			optional: true
		},
	},
	async handler(ctx) {
		let whereObj = [];
		if (ctx.params.id) {
			if (!Array.isArray(ctx.params.id)) {
				return await ArticleRequest.findByPk(ctx.params.id);
			}
			whereObj.push({
				id: ctx.params.id
			});
		}
		if (ctx.params.article_id) {
			whereObj.push({
				article_id: ctx.params.article_id
			});
		}
		if (ctx.params.status) {
			whereObj.push({
				status: ctx.params.status
			});
		}
		if (ctx.params.return_array) {
			return await ArticleRequest.findAll({
				where: whereObj
			});
		}
		return await ArticleRequest.findOne({
			where: whereObj
		});
	}
};

module.exports = resolveArticleRequest;
