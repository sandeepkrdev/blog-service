const ArticleViewStat = require(process.cwd() + "/models").article_view_stat;

const resolveArticleViewStat = {
	params: {
		id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		article_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		view_id: { type: "string", optional: true }
	},
	async handler(ctx) {
		let whereObj = [];
		if (ctx.params.id) {
			if (!Array.isArray(ctx.params.id)) {
				return await ArticleViewStat.findByPk(ctx.params.id);
			}
			whereObj.push({
				id: ctx.params.id
			});
		}
		if (ctx.params.article_id) {
			whereObj.push({
				article_id: ctx.params.article_id
			});
		}
		if (ctx.params.view_id) {
			whereObj.push({
				view_id: ctx.params.view_id
			});
		}
		if (
			Array.isArray(ctx.params.id) ||
			Array.isArray(ctx.params.article_id) ||
			!ctx.params.view_id
		) {
			return await ArticleViewStat.findAll({
				where: whereObj
			});
		}
		return await ArticleViewStat.findOne({
			where: whereObj
		});
	}
};

module.exports = resolveArticleViewStat;
