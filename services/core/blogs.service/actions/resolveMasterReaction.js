const MasterReaction = require(process.cwd() + "/models").master_reaction;

const resolveMasterReaction = {
	params: {
		id: { type: "number", optional: true }
	},
	async handler(ctx) {
		let whereObj = [];
		if (ctx.params.id) {
			whereObj.push({
				id: ctx.params.id
			});
		}
		return await MasterReaction.findOne({
			where: whereObj
		});
	}
};

module.exports = resolveMasterReaction;
