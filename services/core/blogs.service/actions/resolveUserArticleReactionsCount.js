const Article = require(process.cwd() + "/models").article;
const ArticleReaction = require(process.cwd() + "/models").article_reaction;
const ArticlePublished = require(process.cwd() + "/models").article_published;

const resolveUserArticleReactionsCount = {
	params: {
		user_id: {
			type: "number"
		}
	},
	async handler(ctx) {
		return await ArticleReaction.count({
			include: {
				as: "article",
				model: Article,
				where: {
					user_id: ctx.params.user_id
				},
				include: {
					required: true,
					as: "published",
					model: ArticlePublished,
				}
			}
		});
	}
};

module.exports = resolveUserArticleReactionsCount;
