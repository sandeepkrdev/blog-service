const { MoleculerClientError } = require("moleculer").Errors;

async function validateUser(ctx, articleId) {
	try {
		await ctx.call("blogs.canAccessArticle", {
			article_id: articleId
		});
	} catch (e) {
		await ctx.call("users.hasPermission", {
			permissionName: "can-publish-articles"
		});
	}
}

async function getArticlePublishAndValidate(ctx) {
	const articlePublished = await ctx.call("blogs.resolveArticlePublished", {
		id: ctx.params.article_published_id
	});
	if (!articlePublished) {
		throw new MoleculerClientError(
			"Article Published not found",
			400,
			"article_published_id"
		);
	}
	return articlePublished;
}

const unPublishArticle = {
	graphql: {
		mutation: "unPublishArticle(article_published_id: Int!): Response!"
	},
	async handler(ctx) {
		try {
			const articlePublished = await getArticlePublishAndValidate(ctx);
			await validateUser(ctx, articlePublished.article_id);
			this.waitForServices(["jobs"]).then(async () => {
				ctx.call("jobs.push", {
					name: "article.published.previous.delete",
					payload: {
						article_published_id: articlePublished.id,
						publish: false,
						user_id: ctx.meta && ctx.meta.user ? ctx.meta.user.id : 0
					}
				});
			});
			return { message: "Article has been unpublished" };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = unPublishArticle;
