const { MoleculerClientError } = require("moleculer").Errors;

async function validateUser(ctx, articleId) {
	await ctx.call("blogs.canAccessArticle", {
		article_id: articleId
	});
}

async function getArticleAndValidate(ctx) {
	const article = await ctx.call("blogs.resolveArticle", {
		id: ctx.params.id
	});
	if (!article) {
		throw new MoleculerClientError("Article not found", 400, "id");
	}
	return article;
}

async function checkSlugExists(ctx) {
	const slug = await ctx.call("blogs.resolveArticle", {
		slug: ctx.params.input.slug
	});
	if (slug && slug.id !== ctx.params.id) {
		throw new MoleculerClientError("Slug already exists", 400, "slug");
	}
	return !slug ? true : slug.id !== ctx.params.id;
}

const updateArticle = {
	graphql: {
		mutation: "updateArticle(id:Int!, input: ArticleInput!): Article!"
	},
	async handler(ctx) {
		try {
			await validateUser(ctx, ctx.params.id);
			let article = await getArticleAndValidate(ctx);
			if (ctx.params.input.slug.length < 1) {
				throw new MoleculerClientError(
					"Slug cannot be empty",
					400,
					"slug"
				);
			}
			await checkSlugExists(ctx);
			await article.update(ctx.params.input);
			return article;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = updateArticle;
