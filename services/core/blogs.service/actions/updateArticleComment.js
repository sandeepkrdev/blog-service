const { MoleculerClientError } = require("moleculer").Errors;

async function getArticleCommentAndValidate(ctx) {
	const articleComment = await ctx.call("blogs.resolveArticleComment", {
		id: ctx.params.id
	});
	if (!articleComment) {
		throw new MoleculerClientError("Article Comment not found", 400, "id");
	}
	return articleComment;
}

async function canAccessArticleComment(ctx, user_id) {
	if (user_id != ctx.meta.user.id) {
		throw new MoleculerClientError(
			"Cannot update someone else's comment",
			400,
			"id"
		);
	}
}

async function getArticleAndValidate(ctx) {
	const article = await ctx.call("blogs.resolveArticle", {
		id: ctx.params.input.article_id
	});
	if (!article) {
		throw new MoleculerClientError("Article not found", 400, "article_id");
	}
}

async function getParentAndValidate(ctx) {
	const parent = await ctx.call("blogs.resolveArticleComment", {
		id: ctx.params.input.parent_id
	});
	if (!parent) {
		throw new MoleculerClientError("Parent not found", 400, "parent_id");
	}
}

const updateArticleComment = {
	graphql: {
		mutation:
			"updateArticleComment(id:Int!, input: ArticleCommentInput!): ArticleComment!"
	},
	async handler(ctx) {
		try {
			let articleComment = await getArticleCommentAndValidate(ctx);
			await canAccessArticleComment(ctx, articleComment.user_id);
			await getArticleAndValidate(ctx);
			if (ctx.params.input.parent_id) await getParentAndValidate(ctx);
			return articleComment.update(ctx.params.input);
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = updateArticleComment;
