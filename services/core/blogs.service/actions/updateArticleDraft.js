const { MoleculerClientError } = require("moleculer").Errors;

async function validateUser(ctx, articleId) {
	await ctx.call("blogs.canAccessArticle", {
		article_id: articleId
	});
}

async function getArticleDraftAndValidate(ctx) {
	const articleDraft = await ctx.call("blogs.resolveArticleDraft", {
		id: ctx.params.id
	});
	if (!articleDraft) {
		throw new MoleculerClientError("Article Draft not found", 400, "id");
	}
	return articleDraft;
}

async function checkArticleAlreadyExistsInArticleDraft(ctx, article_id, id) {
	const articleDraft = await ctx.call("blogs.resolveArticleDraft", {
		article_id: article_id
	});
	if (articleDraft && articleDraft.id != id) {
		throw new MoleculerClientError(
			"Draft of this article already exists",
			400,
			"article_id"
		);
	}
}

const updateArticleDraft = {
	graphql: {
		mutation:
			"updateArticleDraft(id:Int!, input: ArticleStatusInput!): ArticleDraft!"
	},
	async handler(ctx) {
		try {
			const articleDraft = await getArticleDraftAndValidate(ctx);
			await validateUser(ctx, articleDraft.article_id);
			await checkArticleAlreadyExistsInArticleDraft(
				ctx,
				articleDraft.article_id,
				articleDraft.id
			);
			await articleDraft.update(ctx.params.input);
			return articleDraft;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = updateArticleDraft;
