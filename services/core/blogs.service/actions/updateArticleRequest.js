const moment = require("moment");
const { MoleculerClientError } = require("moleculer").Errors;
const { brokerBroadCast } = require(process.cwd() + "/utils/helpers");

async function validateUser(ctx) {
	await ctx.call("users.hasPermission", {
		permissionName: "can-publish-articles"
	});
}

async function getArticleRequestAndValidate(ctx) {
	const articleRequest = await ctx.call("blogs.resolveArticleRequest", {
		id: ctx.params.id
	});
	if (!articleRequest) {
		throw new MoleculerClientError("Article Request not found", 400, "id");
	}
	return articleRequest;
}

async function validateArticleRequestStatus(ctx, articleRequest) {
	if (articleRequest.status != "pending") {
		throw new MoleculerClientError(
			`Article is already ${articleRequest.status}, cannot be ${ctx.params.input.status}`,
			400,
			"status"
		);
	}
}

const updateArticleRequest = {
	graphql: {
		mutation:
			"updateArticleRequest(id:Int!, input: ArticleRequestUpdateInput!): ArticleRequest!"
	},
	async handler(ctx) {
		await validateUser(ctx);
		try {
			let isApproved = false, isRejected = false;
			const articleRequest = await getArticleRequestAndValidate(ctx);
			await validateArticleRequestStatus(ctx, articleRequest);
			if (ctx.params.input.status == "approved") {
				isApproved = true;
			}
			if (ctx.params.input.status == "rejected") {
				isRejected = true;
			}
			// db update
			const input = ctx.params.input;
			input.approved_by = ctx.meta.user.id;
			input.approved_on = moment();
			await articleRequest.update(input);

			if (isApproved) {
				brokerBroadCast(ctx, "blogs", "article.request.approved", {
					id: articleRequest.article_id,
					article_request_id: articleRequest.id,
					user_id: ctx.meta && ctx.meta.user ? ctx.meta.user.id : 0
				});
			}
			if (isRejected) {
				brokerBroadCast(ctx, "blogs", "article.request.rejected", {
					id: articleRequest.article_id,
					article_request_id: articleRequest.id,
					user_id: ctx.meta && ctx.meta.user ? ctx.meta.user.id : 0
				});
			}
			return articleRequest;
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = updateArticleRequest;
