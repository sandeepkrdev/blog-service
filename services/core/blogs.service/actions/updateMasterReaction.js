const { MoleculerClientError } = require("moleculer").Errors;

async function validateUser(ctx) {
	await ctx.call("users.hasPermission", {
		permissionName: "manage-reactions"
	});
}

const getAndValidateMasterReaction = async ctx => {
	const masterReaction = await ctx.call("blogs.resolveMasterReaction", {
		id: ctx.params.id
	});
	if (!masterReaction) {
		throw new MoleculerClientError("Master Reaction not found", 400, "id");
	}
	return masterReaction;
};

const updateMasterReaction = {
	graphql: {
		mutation:
			"updateMasterReaction(id: Int!, input: MasterReactionUpdateInput!): MasterReaction!"
	},
	async handler(ctx) {
		await validateUser(ctx);
		try {
			let masterReaction = await getAndValidateMasterReaction(ctx);
			return await masterReaction.update(ctx.params.input);
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = updateMasterReaction;
