module.exports = {
	graphql: {
		type: `
            input ArticleInput {
				slug: String
            }
            type Article {
                id: Int!
				user: User!
				slug: String!
				draft: ArticleDraft
				published: ArticlePublished
				histories: [ArticleHistory]
				files: [ArticleFile]
				views: [ArticleView]
				viewStats: [ArticleViewStat]
				comments: [ArticleComment]
				reactions: [ArticleReaction]
				reactionStats: [ArticleReactionStat]
				bookmarks: [ArticleBookmark]
				pendingArticleRequest: ArticleRequest
				articleRequests: [ArticleRequest]
            }
			type ArticleDraft {
				id: Int!
				article: Article
				title: String!
				subtitles: String
				cover_image_url: String!
				cover_image_position: CoverImagePositionEnum!
				blog_content: String
				seo_og_image_url: String
				seo_title: String
				seo_description: String
				original_url: String
				publishing_date: Date
				allow_comments: Boolean!
				updatedAt: Date
			}
			type ArticlePublished {
				id: Int!
				article: Article
				title: String!
				subtitles: String
				cover_image_url: String!
				cover_image_position: CoverImagePositionEnum!
				blog_content: String
				seo_og_image_url: String
				seo_title: String
				seo_description: String
				original_url: String
				publishing_date: Date
				allow_comments: Boolean!
			}
			type ArticleHistory {
				id: Int!
				article: Article
				title: String!
				subtitles: String
				cover_image_url: String!
				cover_image_position: CoverImagePositionEnum!
				blog_content: String
				seo_og_image_url: String
				seo_title: String
				seo_description: String
				original_url: String
				publishing_date: Date
				allow_comments: Boolean!
			}
            type ArticleDataOutput {
				data: [Article!]
				meta: ResponseMetaOutput
			}
            input ArticleFilters {
				id: [Int]
				search: String
                user_id: [Int]
                slug: String
				per_page: Int
				page_no: Int
			}
			input ArticleStatusInput {
				article_id: Int!
				title: String!
				subtitles: String
				cover_image_url: String
				cover_image_position: CoverImagePositionEnum
				blog_content: String
				seo_og_image_url: String
				seo_title: String
				seo_description: String
				original_url: String
				publishing_date: Date
				allow_comments: Boolean
			}
			enum CoverImagePositionEnum{
				top
				bottom
				background
			}
            type ArticleDraftDataOutput {
				data: [ArticleDraft!]
				meta: ResponseMetaOutput
			}
            input ArticleStatusFilters {
				id: [Int]
				search: String
                article_id: [Int]
                user_id: [Int]
                cover_image_position: [CoverImagePositionEnum]
                publishing_date: Date
                allow_comments: Boolean
				per_page: Int
				page_no: Int
				orderBy: ArticleStatusOrderByInput
			}
			input ArticleStatusOrderByInput {
				id: OrderByEnum
				publishing_date: OrderByEnum
				updatedAt: OrderByEnum
            }
            type ArticlePublishedDataOutput {
				data: [ArticlePublished!]
				meta: ResponseMetaOutput
			}
            type ArticleHistoryDataOutput {
				data: [ArticleHistory!]
				meta: ResponseMetaOutput
			}
			input ArticleFileInput {
				article_id: Int!
				file_url: String!
				is_file_used: Boolean
			}
			type ArticleFile {
				id: Int!
				article: Article!
				file_url: String!
				is_file_used: Boolean
			}
			input ArticleFileFilters {
				id: [Int]
				article_id: [Int]
				file_url: String
				is_file_used: Boolean
				per_page: Int
				page_no: Int
			}
            type ArticleFileDataOutput {
				data: [ArticleFile!]
				meta: ResponseMetaOutput
            }
			input ArticleViewInput {
				article_id: Int!
				view_id: String!
			}
			type ArticleView {
				id: Int!
				article: Article!
				view_id: String!
			}
			input ArticleViewFilters {
				id: [Int]
				article_id: [Int]
				search: String
				per_page: Int
				page_no: Int
			}
            type ArticleViewDataOutput {
				data: [ArticleView!]
				meta: ResponseMetaOutput
            }
			type ArticleViewStat {
				id: Int!
				article: Article!
				view_id: String!
				count: Int
			}
			input ArticleViewStatFilters {
				id: [Int]
				article_id: [Int]
				search: String
				per_page: Int
				page_no: Int
			}
            type ArticleViewStatDataOutput {
				data: [ArticleViewStat!]
				meta: ResponseMetaOutput
            }
            input ArticleCommentInput {
				article_id: Int!
				text: String!
				parent_id: Int
            }
			type ArticleComment {
				id: Int
				article: Article!
				text: String!
				parent_id: Int
				user: User
				createdAt: Date
			}
			input ArticleCommentFilters {
				id: [Int]
				article_id: [Int]
				parent_id: [Int]
				user_id: [Int]
				search: String
				per_page: Int
				page_no: Int
				orderBy: ArticleCommentOrderByInput
			}
			input ArticleCommentOrderByInput {
				id: OrderByEnum
				createdAt: OrderByEnum
            }
            type ArticleCommentDataOutput {
				data: [ArticleComment!]
				meta: ResponseMetaOutput
            }
			type MasterReaction {
				id: Int!
				file_url: String!
				is_primary: Boolean!
			}
			input MasterReactionInput {
				file_url: String!
				is_primary: Boolean
			}
			input MasterReactionUpdateInput {
				is_primary: Boolean
			}
			input MasterReactionFilters {
				id: [Int]
				search: String
				is_primary: Boolean
				per_page: Int
				page_no: Int
			}
			type MasterReactionOutput {
				data: [MasterReaction]
				meta: ResponseMetaOutput
			}
			input ArticleReactionInput {
				article_id: Int!
				master_reaction_id: Int!
			}
			type ArticleReaction {
				id: Int!
				article: Article
				masterReaction: MasterReaction
				user: User
			}
			input ArticleReactionFilters {
				id: [Int]
				article_id: [Int]
				master_reaction_id: [Int]
				user_id: [Int]
				per_page: Int
				page_no: Int
			}
			type ArticleReactionDataOutput {
				data: [ArticleReaction]
				meta: ResponseMetaOutput
			}
			type ArticleReactionStat {
				id: Int!
				article: Article
				masterReaction: MasterReaction
				count: Int
			}
			input ArticleReactionStatFilters {
				id: [Int]
				article_id: [Int]
				master_reaction_id: [Int]
				per_page: Int
				page_no: Int
			}
            type ArticleReactionStatDataOutput {
				data: [ArticleReactionStat!]
				meta: ResponseMetaOutput
            }
            input ArticleBookmarkInput {
				article_id: Int!
				user_id: Int
            }
			type ArticleBookmark {
				id: Int
				article: Article!
				user: User
				createdAt: Date
			}
			input ArticleBookmarkFilters {
				id: [Int]
				article_id: [Int]
				user_id: [Int]
				per_page: Int
				page_no: Int
				orderBy: ArticleBookmarkOrderByInput
			}
			input ArticleBookmarkOrderByInput {
				id: OrderByEnum
				createdAt: OrderByEnum
            }
            type ArticleBookmarkDataOutput {
				data: [ArticleBookmark!]
				meta: ResponseMetaOutput
            }
			input ArticleRequestInput {
				article_id: Int!
			}
			input ArticleRequestUpdateInput {
				status: ArticleRequestStatusEnum!
				comments: String
				share_comments: Boolean
			}
            enum ArticleRequestStatusEnum {
                pending
                approved
                rejected
			}
			type ArticleRequest {
				id: Int!
				article: Article!
				approvedBy: User
				approved_on: Date
				status: ArticleRequestStatusEnum
				comments: String
				share_comments: Boolean
				createdAt: Date!
			}
			input ArticleRequestFilters {
				id: [Int]
				article_id: [Int]
				status: [ArticleRequestStatusEnum]
				approved_by: [Int]
				user_id: [Int]
				search: String
				per_page: Int
				page_no: Int
				orderBy: ArticleRequestOrderByInput
			}
			input ArticleRequestOrderByInput {
				id: OrderByEnum
				createdAt: OrderByEnum
            }
            type ArticleRequestDataOutput {
				data: [ArticleRequest!]
				meta: ResponseMetaOutput
            }
        `,
		resolvers: {
			Article: {
				user: {
					action: "users.resolveUser",
					rootParams: {
						user_id: "id"
					}
				},
				draft: {
					action: "blogs.resolveArticleDraft",
					rootParams: {
						id: "article_id"
					}
				},
				published: {
					action: "blogs.resolveArticlePublished",
					rootParams: {
						id: "article_id"
					}
				},
				histories: {
					action: "blogs.resolveArticleHistory",
					rootParams: {
						id: "article_id"
					}
				},
				files: {
					action: "blogs.resolveArticleFile",
					rootParams: {
						id: "article_id"
					}
				},
				views: {
					action: "blogs.resolveArticleView",
					rootParams: {
						id: "article_id"
					}
				},
				viewStats: {
					action: "blogs.resolveArticleViewStat",
					rootParams: {
						id: "article_id"
					}
				},
				comments: {
					action: "blogs.resolveArticleComment",
					rootParams: {
						id: "article_id"
					}
				},
				reactions: {
					action: "blogs.resolveArticleReaction",
					rootParams: {
						id: "article_id"
					}
				},
				reactionStats: {
					action: "blogs.resolveArticleReactionStat",
					rootParams: {
						id: "article_id"
					}
				},
				bookmarks: {
					action: "blogs.resolveArticleBookmark",
					rootParams: {
						id: "article_id"
					}
				},
				pendingArticleRequest: {
					action: "blogs.resolveArticleRequest",
					rootParams: {
						id: "article_id"
					},
					params: {
						status: "pending",
						return_array: false
					}
				},
				articleRequests: {
					action: "blogs.resolveArticleRequest",
					rootParams: {
						id: "article_id"
					},
					params: {
						return_array: true
					}
				}
			},
			ArticleDraft: {
				article: {
					action: "blogs.resolveArticle",
					rootParams: {
						article_id: "id"
					}
				}
			},
			ArticlePublished: {
				article: {
					action: "blogs.resolveArticle",
					rootParams: {
						article_id: "id"
					}
				}
			},
			ArticleHistory: {
				article: {
					action: "blogs.resolveArticle",
					rootParams: {
						article_id: "id"
					}
				}
			},
			ArticleFile: {
				article: {
					action: "blogs.resolveArticle",
					rootParams: {
						article_id: "id"
					}
				}
			},
			ArticleView: {
				article: {
					action: "blogs.resolveArticle",
					rootParams: {
						article_id: "id"
					}
				}
			},
			ArticleViewStat: {
				article: {
					action: "blogs.resolveArticle",
					rootParams: {
						article_id: "id"
					}
				}
			},
			ArticleComment: {
				article: {
					action: "blogs.resolveArticle",
					rootParams: {
						article_id: "id"
					}
				},
				user: {
					action: "users.resolveUser",
					rootParams: {
						user_id: "id"
					}
				}
			},
			ArticleReaction: {
				article: {
					action: "blogs.resolveArticle",
					rootParams: {
						article_id: "id"
					}
				},
				masterReaction: {
					action: "blogs.resolveMasterReaction",
					rootParams: {
						master_reaction_id: "id"
					}
				},
				user: {
					action: "users.resolveUser",
					rootParams: {
						user_id: "id"
					}
				}
			},
			ArticleReactionStat: {
				article: {
					action: "blogs.resolveArticle",
					rootParams: {
						article_id: "id"
					}
				},
				masterReaction: {
					action: "blogs.resolveMasterReaction",
					rootParams: {
						master_reaction_id: "id"
					}
				}
			},
			ArticleBookmark: {
				article: {
					action: "blogs.resolveArticle",
					rootParams: {
						article_id: "id"
					}
				},
				user: {
					action: "users.resolveUser",
					rootParams: {
						user_id: "id"
					}
				}
			},
			ArticleRequest: {
				article: {
					action: "blogs.resolveArticle",
					rootParams: {
						article_id: "id"
					}
				},
				approvedBy: {
					action: "users.resolveUser",
					rootParams: {
						approved_by: "id"
					}
				}
			}
		}
	}
};
