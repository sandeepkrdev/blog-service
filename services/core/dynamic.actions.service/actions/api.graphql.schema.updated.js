const fs = require("fs");

module.exports = {
	async handler(ctx) {
		fs.writeFileSync(
			process.cwd() + "/graphql/schema.gql",
			ctx.params.schema,
			"utf8"
		);
	}
};
