async function deleteArticle(ctx) {
	await ctx.call("blogs.deleteArticle", {
		id: ctx.params.id
	});
}

module.exports = {
	params: {
		id: { type: "number" },
		user_id: { type: "number" }
	},
	async handler(ctx) {
		await deleteArticle(ctx);
	}
};
