const { MAIL } = require(process.cwd() + "/constants");
const { config } = require(process.cwd() + "/utils/env");
const { getCompiledTemplate } = require(process.cwd() + "/utils/mail-template");
const { FROM_EMAIL } = require(process.cwd() + "/constants/enum");

async function getArticle(ctx) {
	return await ctx.call("blogs.resolveArticle", {
		id: ctx.params.id
	});
}

async function getArticleDraft(ctx, article) {
	return await ctx.call("blogs.resolveArticleDraft", {
		article_id: article.id
	});
}

async function sendApprovalMail(ctx, article) {
	const articleDraft = await getArticleDraft(ctx, article);
	if (articleDraft) {
		const user = await ctx.call("users.resolveUser", {
			id: article.user_id
		});
		if (user) {
			const authEmail = await ctx.call("auth.resolveEmail", {
				user_id: user.id
			});
			if(authEmail) {
				const mailBody = await getCompiledTemplate(
					MAIL.TEMPLATE.ARTICLE_APPROVED,
					{
						salutation: "Hi " + user.name,
						title: MAIL.TITLE.ARTICLE_APPROVED(articleDraft.title),
						article_title: articleDraft.title,
						url: config("frontend-url.website.url") + config("frontend-url.website.blog") + "/" + article.slug,
					}
				);
				const mailer_payload = {
					subject: MAIL.SUBJECT.ARTICLE_APPROVED(articleDraft.title),
					to: authEmail.email,
					body: mailBody,
					type: FROM_EMAIL.NOREPLY,
					template: "ARTICLE_APPROVED"
				};
				ctx.call("mail.send", mailer_payload);
			}
		}
	}
}

module.exports = {
	params: {
		id: { type: "number" },
		user_id: { type: "number" }
	},
	async handler(ctx) {
		const article = await getArticle(ctx);
		if (article) {
			// await sendApprovalMail(ctx, article);
		}
	}
};
