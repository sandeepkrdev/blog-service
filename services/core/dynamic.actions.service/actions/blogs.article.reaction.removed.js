module.exports = {
	params: {
		article_id: { type: "number" },
		master_reaction_id: { type: "number" }
	},
	async handler(ctx) {
		await ctx.call("blogs.createOrUpdateArticleReactionStat", {
			article_id: ctx.params.article_id,
			master_reaction_id: ctx.params.master_reaction_id,
			removed: true
		});
	}
};
