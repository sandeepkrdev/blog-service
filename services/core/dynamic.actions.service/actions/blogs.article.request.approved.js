async function getArticleRequest(ctx) {
	return await ctx.call("blogs.resolveArticleRequest", {
		id: ctx.params.article_request_id
	});
}

async function getArticle(ctx, articleRequest) {
	return await ctx.call("blogs.resolveArticle", {
		id: articleRequest.article_id
	});
}

async function getArticleDraft(ctx, article) {
	return await ctx.call("blogs.resolveArticleDraft", {
		article_id: article.id
	});
}

async function publishArticle(ctx) {
	const articleRequest = await getArticleRequest(ctx);
	if (articleRequest) {
		const article = await getArticle(ctx, articleRequest);
		if (article) {
			const articleDraft = await getArticleDraft(ctx, article);
			if (articleDraft) {
				await ctx.call("blogs.publishArticle", {
					article_draft_id: articleDraft.id
				});
			}
		}
	}
}

module.exports = {
	params: {
		id: { type: "number" },
		user_id: { type: "number" }
	},
	async handler(ctx) {
		await publishArticle(ctx);
	}
};
