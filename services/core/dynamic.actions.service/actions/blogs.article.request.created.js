async function pushToSendNotification(ctx, $this) {
	$this.waitForServices(["jobs"]).then(async () => {
		ctx.call("jobs.push", {
			name: "article.request.send.notification",
			payload: ctx.params
		});
	});
}

module.exports = {
	params: {
		id: { type: "number" },
		user_id: { type: "number" }
	},
	async handler(ctx) {
		await pushToSendNotification(ctx, this);
	}
};
