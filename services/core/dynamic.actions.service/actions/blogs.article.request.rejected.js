const { MAIL } = require(process.cwd() + "/constants");
const { getCompiledTemplate } = require(process.cwd() + "/utils/mail-template");
const { FROM_EMAIL } = require(process.cwd() + "/constants/enum");

async function getArticleRequest(ctx) {
	return await ctx.call("blogs.resolveArticleRequest", {
		id: ctx.params.article_request_id
	});
}

async function getArticle(ctx, articleRequest) {
	return await ctx.call("blogs.resolveArticle", {
		id: articleRequest.article_id
	});
}

async function getArticleDraft(ctx, article) {
	return await ctx.call("blogs.resolveArticleDraft", {
		article_id: article.id
	});
}

async function sendRejectionMail(ctx, articleRequest, article, articleDraft) {
	const user = await ctx.call("users.resolveUser", {
		id: article.user_id
	});
	if (user) {
		const authEmail = await ctx.call("auth.resolveEmail", {
			user_id: user.id
		});
		if(authEmail) {
			const mailBody = await getCompiledTemplate(
				MAIL.TEMPLATE.ARTICLE_REJECTED,
				{
					salutation: "Hi " + user.name,
					title: MAIL.TITLE.ARTICLE_REJECTED(articleDraft.title),
					article_title: articleDraft.title,
					comments: articleRequest.share_comments && articleRequest.comments ? articleRequest.comments : null
				}
			);
			const mailer_payload = {
				subject: MAIL.SUBJECT.ARTICLE_REJECTED(articleDraft.title),
				to: authEmail.email,
				body: mailBody,
				type: FROM_EMAIL.NOREPLY,
				template: "ARTICLE_REJECTED"
			};
			ctx.call("mail.send", mailer_payload);
		}
	}
}

module.exports = {
	params: {
		id: { type: "number" },
		user_id: { type: "number" }
	},
	async handler(ctx) {
		const articleRequest = await getArticleRequest(ctx);
		if (articleRequest) {
			const article = await getArticle(ctx, articleRequest);
			if (article) {
				const articleDraft = await getArticleDraft(ctx, article);
				if (articleDraft) {
					// await sendRejectionMail(ctx, articleRequest, article, articleDraft);
				}
			}
		}
	}
};
