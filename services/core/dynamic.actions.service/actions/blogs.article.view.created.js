module.exports = {
	params: {
		article_id: { type: "number" },
		view_id: { type: "string" }
	},
	async handler(ctx) {
		await ctx.call("blogs.createOrUpdateArticleViewStat", {
			article_id: ctx.params.article_id,
			view_id: ctx.params.view_id,
		});
	}
};
