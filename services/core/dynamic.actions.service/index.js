"use strict";

const { loadFiles } = require(process.cwd() + "/utils/env");
const methods = require("./methods");
const settings = require("./settings");

module.exports = {
	// Define service name
	name: "dynamic-actions",
	settings: settings,
	actions: loadFiles(__dirname + "/actions"),
	methods: methods,
	events: {
		// Subscribe to every events
		// Legacy event handler signature with context
		"**"(payload, sender, event, ctx) {
			let actionName = event.replace("-", ".");
			switch (event) {
				case "graphql.schema.updated":
					/*
					this.waitForServices(["jobs"]).then(async () => {
						ctx.call("jobs.queuesPush", {
							name: "dynamic.actions",
							payload: {
								event: "api.graphql.schema.updated",
								eventPayload: payload
							}
						});
					});
					*/
					break;
				case "$services.changed":
					break;
				case "$broker.started":
					break;
				default:
					if (this.actions[actionName]) {
						this.waitForServices(["jobs"]).then(async () => {
							ctx.call("jobs.queuesPush", {
								name: "dynamic.actions",
								payload: {
									event: event,
									eventPayload: payload
								}
							});
						});
					}
					break;
			}
		}
	}
};
