const apiMethods = require(process.cwd() + "/services/gateway/api.service/methods");

module.exports = {
	...apiMethods,
};
