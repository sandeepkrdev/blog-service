const moment = require("moment");
const FailedJob = require(process.cwd() + "/models").failed_job;

const createFailedJob = {
	params: {
		queue: { type: "string" },
		payload: { type: "string" },
		exception: { type: "string"},
	},
	async handler(ctx) {
		try {
			return await FailedJob.create({
				queue: ctx.params.queue,
				payload: ctx.params.payload,
				exception: ctx.params.exception,
				failed_at: moment()
			});
		} catch (e) {
			//
		}
	}
};

module.exports = createFailedJob;
