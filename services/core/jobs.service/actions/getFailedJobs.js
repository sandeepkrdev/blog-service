const sequelize = require("sequelize");
const FailedJob = require(process.cwd() + "/models").failed_job;
const { PER_PAGE, PAGE_NO } = require(process.cwd() + "/constants/misc");

const getFailedJobs = {
	graphql: {
		query: "getFailedJobs(filters: FailedJobFilters): FailedJobDataOutput"
	},
	async handler(ctx) {
		await ctx.call("users.hasPermission", {
			permissionName: "manage-failed-jobs"
		});
		const pageNo =
			ctx.params.filters && ctx.params.filters.page_no
				? ctx.params.filters.page_no
				: PAGE_NO;
		const perPage =
			ctx.params.filters &&
			ctx.params.filters.per_page &&
			ctx.params.filters.per_page <= PER_PAGE
				? ctx.params.filters.per_page
				: PER_PAGE;
		let whereObj = [],
			orderByObj = [];
		if (ctx.params.filters) {
			if (ctx.params.filters.search) {
				whereObj.push({
					[sequelize.Op.or]: [
						{
							queue: {
								[sequelize.Op.like]:
									"%" + ctx.params.filters.search + "%"
							}
						},
						{
							payload: {
								[sequelize.Op.like]:
									"%" + ctx.params.filters.search + "%"
							}
						},
						{
							exception: {
								[sequelize.Op.like]:
									"%" + ctx.params.filters.search + "%"
							}
						}
					]
				});
			}
			if (ctx.params.filters.orderBy) {
				if (ctx.params.filters.orderBy.createdAt) {
					orderByObj.push([
						"createdAt",
						ctx.params.filters.orderBy.createdAt
					]);
				}
				if (ctx.params.filters.orderBy.updatedAt) {
					orderByObj.push([
						"updatedAt",
						ctx.params.filters.orderBy.updatedAt
					]);
				}
				if (ctx.params.filters.orderBy.failed_at) {
					orderByObj.push([
						"failed_at",
						ctx.params.filters.orderBy.failed_at
					]);
				}
			}
		}
		let count = await FailedJob.count({
			where: whereObj,
			order: orderByObj
		});
		let delayedJobs;
		delayedJobs = await FailedJob.findAll({
			where: whereObj,
			order: orderByObj,
			offset: (pageNo - 1) * perPage,
			limit: perPage
		});

		return {
			data: delayedJobs,
			meta: {
				total: count,
				per_page: perPage,
				page_no: pageNo,
				next_data: count > perPage * pageNo
			}
		};
	}
};

module.exports = getFailedJobs;
