const push = {
	params: {
		name: { type: "string" },
		payload: { type: "object" },
		delayUntil: { type: "number", optional: true},
	},
	async handler(ctx) {
		try {
			if (ctx.params.delayUntil) {
				this.waitForServices(["jobs"]).then(async () => {
					ctx.call("jobs.queuesPush", {
						name: "queue.to.delayed.jobs",
						payload: ctx.params
					});
				});
				return true;
			}
			this.waitForServices(["jobs"]).then(async () => {
				ctx.call("jobs.start", {
					queue: ctx.params.name,
					payload: ctx.params.payload,
				});
			});
			return true;
		} catch (e) {
			//
		}
	}
};
    

module.exports = push;
