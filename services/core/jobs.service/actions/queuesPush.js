const queuesPush = {
	params: {
		name: { type: "string" },
		payload: { type: "object" }
	},
	async handler(ctx) {
		const job = this.createJob(ctx.params.name, ctx.params.payload);
		job.on("progress", progress => {
			this.logger.info(`Job #${job.id} progress is ${progress}%`);
		});
		job.on("succeeded", async res => {
			this.logger.info(`Job #${job.id} completed!. Result:`, res);
			job.remove();
		});
		job.on("failed", async err => {
			job.remove();
			// move this job to failed events
		});
		job.save();
	}
};

module.exports = queuesPush;
