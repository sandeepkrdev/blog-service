const { MoleculerClientError } = require("moleculer").Errors;
const FailedJob = require(process.cwd() + "/models").failed_job;

const requeueFailedJob = {
	graphql: {
		mutation: "requeueFailedJob(id: Int!): Response!"
	},
	async handler(ctx) {
		await ctx.call("users.hasPermission", {
			permissionName: "manage-delayed-jobs"
		});
		await ctx.call("users.hasPermission", {
			permissionName: "manage-failed-jobs"
		});

		try {
			const failedJob = await FailedJob.findByPk(ctx.params.id);
			if (!failedJob) {
				throw new MoleculerClientError("Failed job not found", 400, "id");
			}
			await ctx.call("jobs.push", {
				name: failedJob.queue,
				payload: JSON.parse(failedJob.payload)
			});
			await failedJob.destroy();
			return {
				message: "Job pushed to queue"
			};
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = requeueFailedJob;
