const { map } = require("lodash");
const sequelize = require("sequelize");
const DelayedJob = require(process.cwd() + "/models").delayed_job;
const { PER_PAGE } = require(process.cwd() + "/constants/misc");

const resolveJob = {
	params: {
		id: { type: "number", optional: true },
		available_at: { type: "string", optional: true },
		payload: { type: "object", optional: true }
	},
	async handler(ctx) {
		let whereObj = [];
		if (ctx.params.id) {
			return await DelayedJob.findByPk(ctx.params.id);
		}
		if (ctx.params.available_at) {
			const delayedJobs = await DelayedJob.findAll({
				where: {
					available_at: {
						[sequelize.Op.lte]: ctx.params.available_at
					}
				},
				raw: true,
				limit: PER_PAGE
			});
			return map(delayedJobs, "id");
		}
		if (ctx.params.payload) {
			whereObj.push({
				payload: JSON.stringify(ctx.params.payload)
			});
		}
		return await DelayedJob.findOne({
			where: whereObj
		});
	}
};

module.exports = resolveJob;
