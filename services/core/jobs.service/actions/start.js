const { isEmpty } = require("lodash");

const start = {
	params: {
		queue: { type: "string" },
		payload: { type: "object" },
	},
	async handler(ctx) {
		try {
			const queue = ctx.params.queue;
			const payload = ctx.params.payload;
			if (queue && !isEmpty(payload)) {
				const job = this.createJob(queue, payload);
				job.on("progress", progress => {
					this.logger.info(`Job #${job.id} progress is ${progress}%`);
				});
				job.on("succeeded", async (res) => {
					this.logger.info(`Job #${job.id} completed!. Result:`, res);
					job.remove();
				});
				job.on("failed", async (err) => {
					job.remove();
					this.waitForServices(["jobs"]).then(async () => {
						ctx.call("jobs.queuesPush", {
							name: "queue.to.failed.jobs",
							payload: {
								queue: queue,
								payload: payload,
								exception: err && err.message ? err.message : ""
							}
						});
					});
				});
				job.save();
			}
		} catch (e) {
			//
		}
	}
};

module.exports = start;
