module.exports = {
	before: {
		getFailedJobs: ["checkIsAuthenticated"],
		requeueFailedJob: ["checkIsAuthenticated"],
	}
};
