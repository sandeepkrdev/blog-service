const {config, loadFiles} = require(process.cwd() + "/utils/env");
const settings = require("./settings");
const hooks = require("./hooks");
const QueueService = require("moleculer-bee-queue");
const AuthMixin = require(process.cwd() + "/mixins/auth.mixin");

module.exports = {
	// Define service name
	name: "jobs",
	mixins: [
		QueueService({
			redis: {
				port: config("redis.port"),
				host: config("redis.host"),
				password: config("redis.password")
			}
		}),
		AuthMixin
	],
	settings: settings,
	hooks: hooks,
	actions: loadFiles(__dirname + "/actions"),
	queues: loadFiles(__dirname + "/queues"),
};
