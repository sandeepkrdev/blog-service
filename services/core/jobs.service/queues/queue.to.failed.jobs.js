const FailedJob = require(process.cwd() + "/models").failed_job;

const queueToFailedJobs = async function (job) {
	if (!job.data.queue) {
		throw new Error("queue is missing");
	}
	if (!job.data.payload) {
		throw new Error("payload is missing");
	}
	await FailedJob.create({
		queue: job.data.queue,
		payload: JSON.stringify(job.data.payload),
		exception: job.data.exception || ""
	});
};

module.exports = queueToFailedJobs;