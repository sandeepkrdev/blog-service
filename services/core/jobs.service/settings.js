module.exports = {
	graphql: {
		type: `
			type FailedJob {
				id: Int!
                queue: String!
                payload: String!
                exception: String!
                failed_at: Date!
            }
            input FailedJobFilters {
				search: String
				per_page: Int
				page_no: Int
				orderBy: FailedJobOrderByInput
            }
            type FailedJobDataOutput {
				data: [FailedJob!]
				meta: ResponseMetaOutput
			}
			input FailedJobOrderByInput {
				createdAt: OrderByEnum
				updatedAt: OrderByEnum
				failed_at: OrderByEnum
            }
        `
	}
};
