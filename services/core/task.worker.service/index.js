"use strict";

const {config, loadFiles} = require(process.cwd() + "/utils/env");
const methods = require("./methods");
const QueueService = require("moleculer-bee-queue");

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

module.exports = {
	// Define service name
	name: "task-worker",
	mixins: [
		QueueService({
			redis: {
				port: config("redis.port"),
				host: config("redis.host"),
				password: config("redis.password")
			}
		})
	],
	settings: {},
	queues: loadFiles(__dirname + "/queues"),
	methods: methods
};
