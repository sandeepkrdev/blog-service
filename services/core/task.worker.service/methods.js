const _ = require("lodash");
const apiMethods = require(process.cwd() +
	"/services/gateway/api.service/methods");

module.exports = {
	...apiMethods,
	async getRandom(collection, size) {
		const shuffled = _.shuffle(collection);
		return shuffled.slice(0, size);
	},
	async base64_encode(file) {
		return new Buffer.alloc(file.length, file).toString("base64");
	},
	getObjectKey(xlData, keyName) {
		return Object.keys(xlData).find(
			key => key.toLowerCase() === keyName.toLowerCase()
		);
	},
	async articleRequestMessage(userName, articleTitle, url) {
		return {
			text: "*" + userName + "* has send a publish request.",
			attachments: [
				{
					"fallback": "Action Required.",
					"color": "#2eb886",
					"title": articleTitle,
					"title_link": url,
					"actions": [
						{
							"type": "button",
							"text": "View Request",
							"url": url,
						}
					],
				}
			]
		};
	},
	cleanString(input) {
		let output = "";
		for (let i=0; i<input.length; i++) {
			if (input.charCodeAt(i) <= 127) {
				output += input.charAt(i);
			}
		}
		return output;
	},
};
