async function getArticleDraftAndValidate($this, job) {
	const articleDraft = await $this.broker.call("blogs.resolveArticleDraft", {
		id: parseInt(job.data.article_draft_id)
	});
	if (!articleDraft) {
		throw new Error("Article Draft not found");
	}
	return articleDraft;
}

async function getArticlePublishedAndValidate($this, article_id) {
	const articlePublished = await $this.broker.call(
		"blogs.resolveArticlePublished",
		{
			article_id: parseInt(article_id)
		}
	);
	return articlePublished;
}

const publishArticle = async function (job) {
	const articleDraft = await getArticleDraftAndValidate(this, job);
	const articlePublished = await getArticlePublishedAndValidate(
		this,
		articleDraft.article_id
	);
	const draft = JSON.parse(JSON.stringify(articleDraft));
	let flag = true;

	delete draft.id;
	delete draft.createdAt;
	delete draft.updatedAt;

	if (articlePublished) {
		await this.broker.call("jobs.push", {
			name: "article.published.previous.delete",
			payload: {
				article_published_id: articlePublished.id,
				publish: true
			}
		});
		flag = false;
	}

	//create new published
	if (flag)
		await this.broker.call("blogs.createArticlePublished", {
			input: draft
		});
};

module.exports = publishArticle;
