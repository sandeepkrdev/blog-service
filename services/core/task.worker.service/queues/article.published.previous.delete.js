async function getArticlePublishedAndValidate($this, job) {
	const articlePublished = await $this.broker.call(
		"blogs.resolveArticlePublished",
		{
			id: parseInt(job.data.article_published_id)
		}
	);
	if (!articlePublished) {
		throw new Error("Article Published not found");
	}
	return articlePublished;
}

async function getArticleDraftAndValidate($this, articleId) {
	const articleDraft = await $this.broker.call("blogs.resolveArticleDraft", {
		article_id: parseInt(articleId)
	});
	if (!articleDraft) {
		throw new Error("Article Draft not found");
	}
	return articleDraft;
}

const deletePreviousPublishedArticle = async function (job) {
	const articlePublished = await getArticlePublishedAndValidate(this, job);
	if (articlePublished) {
		const published = JSON.parse(JSON.stringify(articlePublished));
		delete published.id;
		delete published.createdAt;
		delete published.updatedAt;

		const articleDraft = await getArticleDraftAndValidate(
			this,
			articlePublished.article_id
		);
		if (articleDraft) {
			//create article history for previous published
			const articleHistory = await this.broker.call(
				"blogs.createArticleHistory",
				{
					input: published
				}
			);
			if (articleHistory) {
				//destroy previous article published
				const deleted = await this.broker.call(
					"blogs.deleteArticlePublished",
					{
						id: parseInt(job.data.article_published_id)
					}
				);
				if (deleted && deleted.message && job.data.publish) {
					const draft = JSON.parse(JSON.stringify(articleDraft));

					delete draft.id;
					delete draft.createdAt;
					delete draft.updatedAt;
					//create new article published
					await this.broker.call("blogs.createArticlePublished", {
						input: draft
					});
				}
			}
		}
	}
};

module.exports = deletePreviousPublishedArticle;
