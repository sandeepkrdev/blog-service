const { config } = require(process.cwd() + "/utils/env");
const { BLOG_MESSAGE_CHANNEL_ID } = require(process.cwd() + "/constants/setting");

async function getSetting($this) {
	return await $this.broker.call("settings.resolveSetting", {
		key: BLOG_MESSAGE_CHANNEL_ID
	});
}

function getUrl(articleRequest) {
	return `${config("frontend-url.admin.url")}${config("frontend-url.admin.article-requests")}/${articleRequest.id}`;
}

async function getSlackFields($this, articleRequest, articleDraft, user) {
	let channelId, userName, articleTitle, url;
	const setting = await getSetting($this);
	if (setting && setting.value) {
		channelId = setting.value;
		userName = user.name || "Author";
		articleTitle = articleDraft.title || "Article";
		url = getUrl(articleRequest);
	}
	return [channelId, userName, articleTitle, url];
}

async function validateFields(job) {
	if (!job.data.article_request_id) {
		throw new Error("article_request_id is missing");
	}
}

async function getArticleRequestAndValidate($this, job) {
	const articleRequest = await $this.broker.call("blogs.resolveArticleRequest", {
		id: job.data.article_request_id
	});
	if (!articleRequest) {
		throw new Error("Article request not found");
	}
	return articleRequest;
}

async function getArticleAndValidate($this, articleRequest) {
	const article = await $this.broker.call("blogs.resolveArticle", {
		id: articleRequest.article_id
	});
	if (!article) {
		throw new Error("Article not found");
	}
	return article;
}

async function getArticleDraft($this, article) {
	const articleDraft = await $this.broker.call("blogs.resolveArticleDraft", {
		article_id: article.id
	});
	if (!articleDraft) {
		throw new Error("Article draft not found");
	}
	return articleDraft;
}

async function getUserAndValidate($this, article) {
	const user = await $this.broker.call("users.resolveUser", {
		id: article.user_id
	});
	if (!user) {
		throw new Error("User not found");
	}
	return user;
}

async function sendSlackMessage($this, articleRequest, articleDraft, user) {
	const [channelId, userName, articleTitle, url] = await getSlackFields($this, articleRequest, articleDraft, user);
	if (channelId) {
		const msg = await $this.broker.call("notifications.slack", {
			channel_id: channelId,
			message: await $this.articleRequestMessage(userName, articleTitle, url)
		});
		if (msg.ok) {
			// Notification sent
		}
	}
}

const articleRequestSendNotification = async function (job) {
	try {
		await validateFields(job);
		const articleRequest = await getArticleRequestAndValidate(this, job);
		const article = await getArticleAndValidate(this, articleRequest);
		const articleDraft = await getArticleDraft(this, article);
		const user = await getUserAndValidate(this, article);
		await sendSlackMessage(this, articleRequest, articleDraft, user);
	} catch (e) {
		//
	}
};

module.exports = articleRequestSendNotification;
