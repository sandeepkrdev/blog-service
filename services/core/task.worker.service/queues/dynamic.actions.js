const dynamicActions = async function (job) {
	let event = job.data.event;
	let actionName = event.replace("-", ".");
	await this.broker.call(`dynamic-actions.${actionName}`, {
		...job.data.eventPayload
	});
};

module.exports = dynamicActions;
