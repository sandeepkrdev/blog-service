const postmark = require("postmark");
const { config } = require(process.cwd() + "/utils/env");

const mailPostmarkSend = async function (job) {
	try {
		job.reportProgress(10);
		const client = new postmark.Client(config("postmark-service.api_token"));

		if (!job.data.from) {
			job.data.from = config("postmark-service.from");
		}

		// setup email data with unicode symbols
		let mailOptions = {
			From: job.data.from, // sender address
			To: job.data.to, // list of receivers
			Subject: job.data.subject, // Subject line
		};

		if (job.data.plain_text) {
			mailOptions.TextBody = job.data.body; // html body
		}
		if (job.data.raw_html) {
			mailOptions.HtmlBody = job.data.body; // html body
		}
		if (job.data.is_broadcast) {
			mailOptions.MessageStream = config("postmark-service.broadcast_stream_id"); // broadcast type
		}

		// send mail with defined transport object
		const info = await client.sendEmail(mailOptions);

		this.broker.call("jobs.push", {
			name: "mail.postmark.sent",
			payload: {
				...job.data,
				postmark_message_id: info.MessageID
			}
		});

		return this.Promise.resolve({
			done: true,
			id: job.data.id,
			worker: process.pid,
			postmark_message_id: info.MessageID
		});
	} catch (e) {
		// console.log(e);
	}
};

module.exports = mailPostmarkSend;
