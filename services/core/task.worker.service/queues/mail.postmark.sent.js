const { brokerBroadCast } = require(process.cwd() + "/utils/helpers");

async function validateFields(job) {
	if (!job.data.postmark_message_id) {
		throw new Error("postmark_message_id is missing");
	}
}

const mailPostmarkSent = async function (job) {
	await validateFields(job);
	if (job.data.campaign_newsletter_subscriber_id) {
		brokerBroadCast(this, "newsletters", "message.sent", {
			...job.data,
			sender_type: "postmark",
			sender_type_message_id: job.data.postmark_message_id,
		});
	}
};

module.exports = mailPostmarkSent;
