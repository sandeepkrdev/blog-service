const moment = require("moment");
const nodemailer = require("nodemailer");
const {config} = require(process.cwd() + "/utils/env");
const { FROM_EMAIL } = require(process.cwd() + "/constants/enum");
const { MAIL_LAST_VIEWED_IN_DAYS } = require(process.cwd() + "/constants/setting");

async function getSetting($this) {
	const setting = await $this.broker.call("settings.resolveSetting", {
		key: MAIL_LAST_VIEWED_IN_DAYS
	});
	return setting ? (!isNaN(parseInt(setting.value)) ? parseInt(setting.value) : 15) : 15;
}

async function resolveCriticalEmail($this, job) {
	return await $this.broker.call("mail.resolveCriticalEmail", {
		name: job.data.template
	});
}

async function resolveEmailActivity($this, job) {
	return await $this.broker.call("mail.resolveEmailActivity", {
		email: job.data.to
	});
}

async function checkIsEmailInactive($this, job) {
	let flag = true, update = false;

	const criticalEmail = await resolveCriticalEmail($this, job);
	const emailActivity = await resolveEmailActivity($this, job);
	const MAIL_LAST_VIEWED = await getSetting($this);
	
	if (emailActivity && emailActivity.is_bounced && emailActivity.is_email_inactive) {
		flag = false;
	} else {
		if (!criticalEmail) {
			if (emailActivity) {
				if (emailActivity.is_email_inactive) {
					flag = false;
				} else {
					let lastViewedAt = emailActivity.last_viewed_at;
					let lastSentAt = moment(emailActivity.last_sent_at || emailActivity.first_sent_at || emailActivity.createdAt);
					const currentDate = moment();
					
					if (lastViewedAt) {
						lastViewedAt = moment(lastViewedAt);
						if (
							currentDate.diff(lastViewedAt, "days") >= MAIL_LAST_VIEWED && currentDate.diff(lastSentAt, "days") < MAIL_LAST_VIEWED
						) {
							flag = false;
							update = true;
						}
					} else {
						if (currentDate.diff(lastSentAt, "days") >= MAIL_LAST_VIEWED) {
							flag = false;
							update = true;
						}
					}
				}
			}
		}
	}
	
	if (flag) {
		try {
			await $this.broker.call("mail.createOrUpdateEmailActivity", {
				email: job.data.to,
				update_last_sent: true
			});
		} catch (e) {
			//
		}
	} else {
		
		try {
			await $this.broker.call("mail.createEmailDropLog", {
				email: job.data.to,
				template: job.data.template,
				tried_at: moment().toDate()
			});
		} catch (e) {
			//
		}

		if (update) {
			try {
				await $this.broker.call("mail.createOrUpdateEmailActivity", {
					email: job.data.to,
					is_email_inactive: true,
					update_last_sent: true,
					inactive_reason: `NotViewedSinceLast${MAIL_LAST_VIEWED}Days`
				});
			} catch (e) {
				//
			}
		}
	}

	return !flag;
}

const mailSend = function (job) {
	job.reportProgress(10);

	let transporter = nodemailer.createTransport({
		host: config("mail-service.host"),
		port: config("mail-service.port"),
		auth: {
			user: config("mail-service.user"), // generated ethereal user
			pass: config("mail-service.password") // generated ethereal password
		},
		tls: {
			rejectUnauthorized: false
		}
	});

	if (!job.data.type) {
		job.data.type = FROM_EMAIL.NOREPLY;
	}

	const from = config(`mail-service.from.${job.data.type}`);
	if (Array.isArray(job.data.to)) {
		job.data.to = job.data.to[0];
	}

	// setup email data with unicode symbols
	return new Promise((resolve, reject) => {
		const isEmailInactive = checkIsEmailInactive(this, job);

		isEmailInactive
			.then(value => {
				if (value) {
					return resolve({
						done: true
					});
				} else {
				// setup email data with unicode symbols
					let mailOptions = {
						from: from, // sender address
						to: job.data.to, // list of receivers
						subject: job.data.subject, // Subject line
						html: job.data.body // html body
					};

					const $this = this;
					// send mail with defined transport object
					transporter.sendMail(mailOptions, (error, info) => {
						if (error) {
							return console.log(error);
						}
						$this.broker.call("jobs.push", {
							name: "mail.sent",
							payload: {
								from_email: from,
								to_email: job.data.to,
								subject: job.data.subject,
								postmark_message_id: info.messageId
							}
						});
						console.log("Message sent: %s", info.messageId);
						console.log(
							"Preview URL: %s",
							nodemailer.getTestMessageUrl(info)
						);
					});

					return resolve({
						done: true,
						id: job.data.id,
						worker: process.pid
					});
				}
			})
			.catch(e => {
				return resolve({
					done: true
				});
			});
	});
};

module.exports = mailSend;
