async function validateFields(job) {
	if (!job.data.from_email) {
		throw new Error("from_email is missing");
	}
	if (!job.data.to_email) {
		throw new Error("to_email is missing");
	}
	if (!job.data.postmark_message_id) {
		throw new Error("postmark_message_id is missing");
	}
}

async function createOrUpdateEmailHistory($this, job) {
	await $this.broker.call("mail.createOrUpdateEmailHistory", {
		from_email: job.data.from_email,
		to_email: job.data.to_email,
		message_id: job.data.postmark_message_id,
		subject: job.data.subject,
		status: "Queued",
	});
}

const mailPostmarkSent = async function (job) {
	try {
		await validateFields(job);
		await createOrUpdateEmailHistory(this, job);
	} catch (e) {
		//
	}
};

module.exports = mailPostmarkSent;
