const moment = require("moment");
const { brokerBroadCast } = require(process.cwd() + "/utils/helpers");

async function ifOpenUpdateLastViewedAt($this, job) {
	try {
		let recordTypeOpen = false;
		if (job.data && job.data.RecordType) {
			if (job.data.RecordType === "Open") {
				await $this.broker.call("mail.createOrUpdateEmailActivity", {
					last_viewed_at: moment(),
					email: job.data.Recipient,
					is_email_inactive: false,
					is_bounced: false,
					inactive_reason: null,
					bounce_reason: null,
				});
				recordTypeOpen = true;
			}
		}
		return recordTypeOpen;
	} catch (e) {
		//
	}
}

async function markInActive($this, job) {
	try {
		let recordTypeBounce = false;
		if (job.data && job.data.RecordType) {
			if (job.data.RecordType === "Bounce" || job.data.RecordType === "SpamComplaint") {
				await $this.broker.call("mail.createOrUpdateEmailActivity", {
					email: job.data.Email,
					is_email_inactive: job.data && job.data.Type === "SoftBounce" ? false : true,
					is_bounced: true,
					inactive_reason: job.data.RecordType,
					bounce_reason: job.data && job.data.Type ? job.data.Type : job.data.RecordType,
					last_viewed_at: null
				});
				recordTypeBounce = true;
			}
		}
		return recordTypeBounce;
	} catch (e) {
		//
	}
}

async function updateEmailHistory($this, job) {
	try {
		return await $this.broker.call("mail.createOrUpdateEmailHistory", {
			message_id: job.data.MessageID,
			status: job.data.Type || job.data.RecordType,
		});
	} catch (e) {
		//
	}
}

async function getUser($this, emailHistory) {
	return await $this.broker.call("users.resolveUser", {
		email: emailHistory.to_email
	});
}

async function deactivateUser($this, job, emailHistory) {
	try {

		if (job.data && job.data.RecordType && job.data.Type) {
			if (job.data.RecordType === "Bounce" && job.data.Type !== "SoftBounce") {
				const user = await getUser($this, emailHistory);
				if (user) {
					brokerBroadCast($this, "newsletters", "subscriber.email_invalid", {
						email: emailHistory.to_email,
						user_id: user.id
					});
				}
			}
		}
	} catch (e) {
		//
	}
}

const savePostmarkEmailLog = async function (job) {
	this.waitForServices(["mail"]).then(async () => {
		const createEmailLog = await this.broker.call("mail.createEmailLog", {
			payload: job.data
		});
		if (!createEmailLog) {
			throw new Error("Something went wrong");
		}

		if (job.data.Recipient || job.data.Email) {
			await this.broker.call("mail.createOrUpdateEmailActivity", {
				email: job.data.Recipient || job.data.Email
			});
		}
		await markInActive(this, job);
		await ifOpenUpdateLastViewedAt(this, job);

		const emailHistory = await updateEmailHistory(this, job);
		if (emailHistory) {
			await deactivateUser(this, job, emailHistory);
		}
	});
};

module.exports = savePostmarkEmailLog;
