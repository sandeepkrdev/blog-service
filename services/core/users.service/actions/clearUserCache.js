const clearUserCache = {
	graphql: {
		mutation: "clearUserCache: Response!"
	},
	async handler() {
		try {
			this.broker.cacher.clean("users.**");
			return {
				message: "User cache has been cleared."
			};
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = clearUserCache;
