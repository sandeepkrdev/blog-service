const Role = require(process.cwd() + "/models").role;
const { MoleculerClientError } = require("moleculer").Errors;

const createRole = {
	graphql: {
		mutation: "createRole(name: String!): Role!"
	},
	async handler(ctx) {
		try {
			await ctx.call("users.hasPermission", {
				permissionName: "roles-create",
				user_id: 3
			});
			const role = await ctx.call("users.resolveRole", {
				name: ctx.params.name
			});
			if (role) {
				throw new MoleculerClientError("Role with this name already exists", 400, "name");
			}
			return await Role.create({
				name: ctx.params.name
			});
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = createRole;
