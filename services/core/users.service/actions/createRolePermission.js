const RoleHasPermission = require(process.cwd() + "/models")
	.role_has_permission;

const createRolePermission = {
	graphql: {
		mutation:
			"createRolePermission(role_id: Int!, permission_id: [Int]!): [RolePermission!]"
	},
	async handler(ctx) {
		await ctx.call("users.hasPermission", {
			permissionName: "role-permission-create"
		});
		let rolePermissions = [];
		const role = await ctx.call("users.resolveRole", {
			id: ctx.params.role_id
		});
		if (role) {
			for (let i = 0; i < ctx.params.permission_id.length; i++) {
				const permission = await ctx.call("users.resolvePermission", {
					id: ctx.params.permission_id[i]
				});
				if (permission) {
					let rolePermission = await RoleHasPermission.findOne({
						where: {
							role_id: ctx.params.role_id,
							permission_id: ctx.params.permission_id[i]
						}
					});
					if (!rolePermission) {
						rolePermission = await RoleHasPermission.create({
							role_id: ctx.params.role_id,
							permission_id: ctx.params.permission_id[i]
						});
					}
					rolePermissions.push(rolePermission);
				}
			}
		}
		await ctx.call("users.clearUserCache");
		return rolePermissions;
	}
};

module.exports = createRolePermission;
