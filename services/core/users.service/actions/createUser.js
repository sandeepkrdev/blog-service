const User = require(process.cwd() + "/models").user;
const { MoleculerClientError } = require("moleculer").Errors;

const createUser = {
	graphql: {
		mutation: "createUser(input: UserInput) : User"
	},
	async handler(ctx) {
		const entity = ctx.params;
		const user = await User.create({
			name: entity.name,
			email: entity.email
		});
		if (!user) {
			throw new MoleculerClientError("Something went wrong", 400, "name");
		}
		return user;
	}
};

module.exports = createUser;
