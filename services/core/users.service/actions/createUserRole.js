const UserHasRole = require(process.cwd() + "/models").user_has_role;
const { brokerBroadCast } = require(process.cwd() + "/utils/helpers");

async function fireEvent(ctx, userRoles) {
	userRoles.map(userRole => {
		brokerBroadCast(ctx, "users", "role.changed", {
			id: userRole.user_id
		});
	});
}

const createUserRole = {
	graphql: {
		mutation:
			"createUserRole(user_id: [Int]!, role_id: [Int]!): [UserRole!]"
	},
	async handler(ctx) {
		await ctx.call("users.hasPermission", {
			permissionName: "user-role-create"
		});
		let userRoles = [];
		for (let i = 0; i < ctx.params.user_id.length; i++) {
			const user = await ctx.call("users.resolveUser", {
				id: ctx.params.user_id[i]
			});
			if (user) {
				for (let j = 0; j < ctx.params.role_id.length; j++) {
					const role = await ctx.call("users.resolveRole", {
						id: ctx.params.role_id[j]
					});
					if (role) {
						let userRole = await UserHasRole.findOne({
							where: {
								user_id: ctx.params.user_id[i],
								role_id: ctx.params.role_id[j]
							}
						});
						if (!userRole) {
							userRole = await UserHasRole.create({
								user_id: ctx.params.user_id[i],
								role_id: ctx.params.role_id[j]
							});
						}
						userRoles.push(userRole);
					}
				}
			}
		}
		fireEvent(ctx, userRoles);
		return userRoles;
	}
};

module.exports = createUserRole;
