const Role = require(process.cwd() + "/models").role;
const { MoleculerClientError } = require("moleculer").Errors;

const deleteRole = {
	graphql: {
		mutation: "deleteRole(id: Int!): Response"
	},
	async handler(ctx) {
		try {
			await ctx.call("users.hasPermission", {
				permissionName: "roles-delete"
			});
			const role = await Role.findByPk(ctx.params.id);
			if (!role) {
				throw new MoleculerClientError("Role not found!", 400, "id");
			}
			await ctx.call("users.clearUserCache");
			await role.destroy();
			return { message: "Role deleted successfully!" };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = deleteRole;
