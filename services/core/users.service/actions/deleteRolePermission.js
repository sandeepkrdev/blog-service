const RoleHasPermission = require(process.cwd() + "/models")
	.role_has_permission;
const { MoleculerClientError } = require("moleculer").Errors;

const deleteRolePermission = {
	graphql: {
		mutation:
			"deleteRolePermission(role_id: Int!, permission_id: Int!): Response!"
	},
	async handler(ctx) {
		try {
			await ctx.call("users.hasPermission", {
				permissionName: "role-permission-delete"
			});
			const rolePermission = await RoleHasPermission.destroy({
				where: {
					role_id: ctx.params.role_id,
					permission_id: ctx.params.permission_id
				}
			});
			if (!rolePermission) {
				throw new MoleculerClientError(
					"Role Permission not found!",
					400,
					"role_id"
				);
			}
			await ctx.call("users.clearUserCache");
			return { message: "Role Permission deleted successfully!" };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = deleteRolePermission;
