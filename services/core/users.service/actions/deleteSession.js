const Session = require(process.cwd() + "/models").session;

const deleteSession = {
	params: {
		user_id: {
			type: "number"
		}
	},
	async handler(ctx) {
		try {
			const session = await Session.findOne({
				where: {
					user_id: ctx.params.user_id
				}
			});
			if (session) {
				await session.destroy();
			}
			return {
				message: true
			};
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = deleteSession;
