const { MoleculerClientError } = require("moleculer").Errors;
const UserHasRole = require(process.cwd() + "/models").user_has_role;
const { brokerBroadCast } = require(process.cwd() + "/utils/helpers");

async function fireEvent(ctx, user_id) {
	brokerBroadCast(ctx, "users", "role.changed", {
		id: user_id
	});
}

const deleteUserRole = {
	graphql: {
		mutation: "deleteUserRole(user_id: Int!, role_id: Int!): Response!"
	},
	async handler(ctx) {
		try {
			await ctx.call("users.hasPermission", {
				permissionName: "user-role-delete"
			});
			const userRole = await UserHasRole.destroy({
				where: {
					user_id: ctx.params.user_id,
					role_id: ctx.params.role_id
				}
			});
			if (!userRole) {
				throw new MoleculerClientError(
					"User Role not found!",
					400,
					"user_id"
				);
			}
			fireEvent(ctx, ctx.params.user_id);
			return { message: "User Role deleted successfully!" };
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = deleteUserRole;
