const sequelize = require("sequelize");
const Permission = require(process.cwd() + "/models").permission;
const { PER_PAGE, PAGE_NO } = require(process.cwd() + "/constants/misc");

const getPermission = {
	graphql: {
		query:
			"getPermissions(filters: PermissionFilters): PermissionDataOutput"
	},
	async handler(ctx) {
		const pageNo =
			ctx.params.filters && ctx.params.filters.page_no
				? ctx.params.filters.page_no
				: PAGE_NO;
		const perPage =
			ctx.params.filters &&
			ctx.params.filters.per_page &&
			ctx.params.filters.per_page <= PER_PAGE
				? ctx.params.filters.per_page
				: PER_PAGE;
		let whereObj = [];
		if (ctx.params.filters) {
			if (ctx.params.filters.id) {
				whereObj.push({
					id: ctx.params.filters.id
				});
			}
			if (ctx.params.filters.search) {
				whereObj.push({
					name: {
						[sequelize.Op.like]:
							"%" + ctx.params.filters.search + "%"
					}
				});
			}
		}
		let count = await Permission.count({
			where: whereObj
		});
		let permissions;
		try {
			permissions = await Permission.findAll({
				where: whereObj,
				offset: (pageNo - 1) * perPage,
				limit: perPage
			});
			return {
				data: permissions,
				meta: {
					total: count,
					per_page: perPage,
					page_no: pageNo,
					next_data: count > perPage * pageNo
				}
			};
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = getPermission;
