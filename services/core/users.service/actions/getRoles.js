const { map } = require("lodash");
const sequelize = require("sequelize");
const Role = require(process.cwd() + "/models").role;
const { PER_PAGE, PAGE_NO } = require(process.cwd() + "/constants/misc");
const { companyAuth, openingAuth } = require(process.cwd() + "/constants/auth");

function addCompanyRoles(search, whereObj) {
	if (search === companyAuth.rolePrefix) {
		whereObj.push({
			name: map(companyAuth.rolePermissions, "role")
		});
	}
}

function addOpeningRoles(search, whereObj) {
	if (search === openingAuth.rolePrefix) {
		whereObj.push({
			name: map(openingAuth.rolePermissions, "role")
		});
	}
}

const getRole = {
	graphql: {
		query: "getRoles(filters: RoleFilters): RoleDataOutput"
	},
	async handler(ctx) {
		// await ctx.call("users.hasPermission", {
		// 	permissionName: "role-read-list"
		// });
		const pageNo =
			ctx.params.filters && ctx.params.filters.page_no
				? ctx.params.filters.page_no
				: PAGE_NO;
		const perPage =
			ctx.params.filters &&
			ctx.params.filters.per_page &&
			ctx.params.filters.per_page <= PER_PAGE
				? ctx.params.filters.per_page
				: PER_PAGE;
		let whereObj = [];
		if (ctx.params.filters) {
			if (ctx.params.filters.id) {
				whereObj.push({
					id: ctx.params.filters.id
				});
			}
			if (ctx.params.filters.search) {
				addCompanyRoles(ctx.params.filters.search, whereObj);
				addOpeningRoles(ctx.params.filters.search, whereObj);
				whereObj.push({
					name: {
						[sequelize.Op.like]:
							"%" + ctx.params.filters.search + "%"
					}
				});
			}
		}
		let count = await Role.count({
			where: whereObj
		});
		let roles;
		try {
			roles = await Role.findAll({
				where: whereObj,
				offset: (pageNo - 1) * perPage,
				limit: perPage
			});
			return {
				data: roles,
				meta: {
					total: count,
					per_page: perPage,
					page_no: pageNo,
					next_data: count > perPage * pageNo
				}
			};
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = getRole;
