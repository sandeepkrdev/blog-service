const sequelize = require("sequelize");
const User = require(process.cwd() + "/models").user;
const UserHasRole = require(process.cwd() + "/models").user_has_role;
const { PER_PAGE, PAGE_NO } = require(process.cwd() + "/constants/misc");

const getUsers = {
	graphql: {
		query: "getUsers(filters: UserFilters): UserDataOutput"
	},
	async handler(ctx) {
		const pageNo =
			ctx.params.filters && ctx.params.filters.page_no
				? ctx.params.filters.page_no
				: PAGE_NO;
		const perPage =
			ctx.params.filters &&
			ctx.params.filters.per_page &&
			ctx.params.filters.per_page <= PER_PAGE
				? ctx.params.filters.per_page
				: PER_PAGE;
		let whereObj = [],
			includeObj = [],
			orderByObj = [];
		if (ctx.params.filters) {
			if (ctx.params.filters.id) {
				whereObj.push({
					id: ctx.params.filters.id
				});
			}
			if (ctx.params.filters.is_active || ctx.params.filters.is_active === false) {
				whereObj.push({
					is_active: ctx.params.filters.is_active
				});
			}
			if (ctx.params.filters.is_archived || ctx.params.filters.is_archived === false) {
				whereObj.push({
					is_archived: ctx.params.filters.is_archived
				});
			}
			if (ctx.params.filters.is_whatsapp_allowed || ctx.params.filters.is_whatsapp_allowed === 0) {
				whereObj.push({
					is_whatsapp_allowed: ctx.params.filters.is_whatsapp_allowed
				});
			}
			if (ctx.params.filters.source) {
				whereObj.push({
					source: ctx.params.filters.source
				});
			}
			if (ctx.params.filters.referral_code) {
				whereObj.push({
					referral_code: ctx.params.filters.referral_code
				});
			}
			if (ctx.params.filters.is_referred) {
				whereObj.push({
					referral_code: {
						[sequelize.Op.ne]: null
					}
				});
			}
			if (ctx.params.filters.is_referred === false) {
				whereObj.push({
					referral_code: null
				});
			}
			if (ctx.params.filters.phone_verified_at_is_null) {
				whereObj.push({
					phone_verified_at: null
				});
			}
			if (ctx.params.filters.phone_verified_at_is_null === false) {
				whereObj.push({
					phone_verified_at: {
						[sequelize.Op.ne]: null
					}
				});
			}
			if (ctx.params.filters.search) {
				whereObj.push({
					[sequelize.Op.or]: [
						{
							email: {
								[sequelize.Op.like]:
									"%" + ctx.params.filters.search + "%"
							}
						},
						{
							phone: {
								[sequelize.Op.like]:
									"%" + ctx.params.filters.search + "%"
							}
						},
						{
							name: {
								[sequelize.Op.like]:
									"%" + ctx.params.filters.search + "%"
							}
						},
						{
							id: {
								[sequelize.Op.like]:
									"%" + ctx.params.filters.search + "%"
							}
						},
						{
							referral_code: {
								[sequelize.Op.like]:
									"%" + ctx.params.filters.search + "%"
							}
						},
						{
							source: {
								[sequelize.Op.like]:
									"%" + ctx.params.filters.search + "%"
							}
						}
					]
				});
			}
			if (ctx.params.filters.role_id_not_null) {
				includeObj.push({
					model: UserHasRole,
					as: "userRoles",
					where: {
						role_id: { [sequelize.Op.ne]: null }
					}
				});
			}
			if (ctx.params.filters.orderBy) {
				if (ctx.params.filters.orderBy.id) {
					orderByObj.push(["id", ctx.params.filters.orderBy.id]);
				}
				if (ctx.params.filters.orderBy.name) {
					orderByObj.push(["name", ctx.params.filters.orderBy.name]);
				}
				if (ctx.params.filters.orderBy.createdAt) {
					orderByObj.push([
						"createdAt",
						ctx.params.filters.orderBy.createdAt
					]);
				}
			}
		}
		let count = await User.count({
			where: whereObj,
			include: includeObj,
			order: orderByObj
		});
		let users;
		users = await User.findAll({
			where: whereObj,
			include: includeObj,
			order: orderByObj,
			offset: (pageNo - 1) * perPage,
			limit: perPage
		});
		return {
			data: users,
			meta: {
				total: count,
				per_page: perPage,
				page_no: pageNo,
				next_data: count > perPage * pageNo
			}
		};
	}
};

module.exports = getUsers;
