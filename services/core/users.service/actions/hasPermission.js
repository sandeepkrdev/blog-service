const { MoleculerClientError } = require("moleculer").Errors;

const hasPermission = {
	params: {
		permissionName: { type: "string" },
		user_id: { type: "number", optional: true }
	},
	async handler(ctx) {
		try {
			const userId = ctx.params.user_id
				? ctx.params.user_id
				: ctx.meta.user
					? ctx.meta.user.id
					: "";
			if (!userId) {
				throw new MoleculerClientError(
					"User not given to check permission",
					400,
					"user_id"
				);
			}

			const permissionNames = ctx.meta.session && ctx.meta.session.permissions ? ctx.meta.session.permissions : [];
			if (!permissionNames.includes(ctx.params.permissionName))
				throw new MoleculerClientError(
					"Unauthorized Access!",
					403,
					"user_id"
				);
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = hasPermission;
