const bcrypt = require("bcrypt");
const { ENUM } = require(process.cwd() + "/constants");
const { config } = require(process.cwd() + "/utils/env");
const { MoleculerClientError } = require("moleculer").Errors;

const login = {
	graphql: {
		mutation: "login(email: String!, password: String!): UserWithToken!"
	},
	async handler(ctx) {
		const entity = ctx.params;
		const authEmail = await ctx.call("users.resolveEmail", {
			email: entity.email
		});
		if (!authEmail) {
			throw new MoleculerClientError(
				`Couldn't find your ${config("app.name")} Account`,
				400,
				"email"
			);
		}

		if (!authEmail.password) {
			throw new MoleculerClientError(
				"Password is not set for this email, please generate new password.",
				400,
				"password"
			);
		}

		if (
			!bcrypt.compareSync(entity.password, authEmail.password)
		) {
			throw new MoleculerClientError(
				"Wrong password. Try again or click Forgot password to reset it.",
				400,
				"password"
			);
		}

		let _user;
		try {
			_user = await this.getUser(
				ctx,
				ENUM.AUTH.REF_TYPE.AUTH_EMAIL,
				authEmail.id
			);

			return await this.getUserWithToken(_user);
		} catch (error) {
			return Promise.reject(error);
		}
	}
};

module.exports = login;
