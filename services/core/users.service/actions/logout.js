const Session = require(process.cwd() + "/models").session;
const sessionRedis = require(process.cwd() + "/utils/sessionRedis");

const logout = {
	graphql: {
		mutation: "logout: Response!"
	},
	async handler(ctx) {
		const session = await Session.findOne({
			where: {
				user_id: ctx.meta.user.id
			}
		});
		if (session) {
			sessionRedis.remove(this.broker, session.session_id);
			await session.destroy();
		}
		// this.entityChanged("logout", {}, ctx);
		return { message: "Logout successful" };
	}
};

module.exports = logout;
