const me = {
	graphql: {
		query: "me: User"
	},
	async handler(ctx) {
		return ctx.meta.user;
	}
};

module.exports = me;
