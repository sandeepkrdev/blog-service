const bcrypt = require("bcrypt");
const { ENUM, MODELS } = require(process.cwd() + "/constants");
const Auth = require(process.cwd() + "/models")[MODELS.AUTH];
const AuthEmail = require(process.cwd() + "/models")[MODELS.AUTH_EMAIL];
const { MoleculerClientError } = require("moleculer").Errors;

const registerUser = {
	graphql: {
		mutation:
			"registerUser(input: RegisterUserInput!): UserWithToken!"
	},
	async handler(ctx) {
		const entity = ctx.params.input;
		let authEmail = await ctx.call("users.resolveEmail", {
			email: entity.email
		});
		if (authEmail) {
			throw new MoleculerClientError(
				"Email already exists!",
				400,
				"email"
			);
		}

		let user = await ctx.call("users.createUser", {
			name: entity.name,
			email: entity.email
		});
		if (!user) {
			throw new MoleculerClientError("Something went wrong", 400, "name");
		}

		let authEmailDoc = {};
		authEmailDoc.email = entity.email;
		authEmailDoc.password = bcrypt.hashSync(entity.password, 10);
		authEmail = await AuthEmail.create(authEmailDoc);
		if (!authEmail) {
			throw new MoleculerClientError(
				"Something went wrong",
				400,
				"email"
			);
		}

		let authDocEmail = {};
		authDocEmail.user_id = user.id;
		authDocEmail.ref_type = ENUM.AUTH.REF_TYPE.AUTH_EMAIL;
		authDocEmail.ref_id = authEmail.id;
		let emailAuth = await Auth.create(authDocEmail);
		if (!emailAuth) {
			throw new MoleculerClientError("Something went wrong", 400, "id");
		}

		const _user = await this.getUser(
			ctx,
			ENUM.AUTH.REF_TYPE.AUTH_EMAIL,
			authEmail.id
		);

		return await this.getUserWithToken(_user);
	}
};
module.exports = registerUser;
