const { MODELS } = require(process.cwd() + "/constants");
const Auth = require(process.cwd() + "/models")[MODELS.AUTH];
const AuthEmail = require(process.cwd() + "/models")[MODELS.AUTH_EMAIL];

const resolveEmail = {
	params: {
		id: { type: "number", optional: true },
		email: { type: "email", optional: true },
		user_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
	},
	async handler(ctx) {
		if (ctx.params.id) {
			return await AuthEmail.findByPk(ctx.params.id);
		}
		if (ctx.params.email) {
			const authEmail = await AuthEmail.findOne({
				where: {
					email: ctx.params.email
				}
			});
			return authEmail;
		}
		if (ctx.params.user_id) {
			if (Array.isArray(ctx.params.user_id)) {
				return await Auth.findAll({
					where: {
						user_id: ctx.params.user_id,
						ref_type: "auth_email"
					},
					include: {
						as: "authEmails",
						model: AuthEmail,
					}
				});
			} else {
				const auth = await Auth.findOne({
					where: {
						user_id: ctx.params.user_id,
						ref_type: "auth_email"
					}
				});
				if (auth) {
					return await AuthEmail.findOne({
						where: {
							id: auth.ref_id
						}
					});
				}
			}
		}
	}
};

module.exports = resolveEmail;
