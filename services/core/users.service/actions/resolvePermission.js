const Permission = require(process.cwd() + "/models").permission;
const RoleHasPermission = require(process.cwd() + "/models")
	.role_has_permission;

const resolvePermission = {
	params: {
		id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		role_id: {
			type: "number",
			optional: true
		}
	},
	async handler(ctx) {
		if (ctx.params.id) {
			if (!Array.isArray(ctx.params.id)) {
				return await Permission.findOne({
					where: {
						id: ctx.params.id
					}
				});
			}
			let permissions = await Permission.findAll({
				where: {
					id: ctx.params.id
				}
			});
			const permissionMap = {};
			permissions.forEach(permission => (permissionMap[permission.id] = permission));
			return ctx.params.id.map(id => permissionMap[id]);
		}
		if (ctx.params.role_id) {
			return await Permission.findAll({
				include: {
					model: RoleHasPermission,
					as: "rolePermissions",
					where: {
						role_id: ctx.params.role_id
					}
				}
			});
		}
	}
};

module.exports = resolvePermission;
