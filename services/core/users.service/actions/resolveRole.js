const Role = require(process.cwd() + "/models").role;
const UserHasRole = require(process.cwd() + "/models").user_has_role;

const resolveRole = {
	params: {
		id: [{
			type: "number",
			optional: true
		},{type: "array", items:"number", optional: true}],
		name: {
			type: "string",
			optional: true
		},
		user_id: {
			type: "number",
			optional: true
		}
	},
	async handler(ctx) {
		if (ctx.params.id) {
			if(Array.isArray(ctx.params.id)){
				let roles = await Role.findAll({
					where: {
						id: ctx.params.id
					}
				});
				const roleMap = {};
				roles.forEach(role => (roleMap[role.id] = role));
				return ctx.params.id.map(id => roleMap[id]);
			}
			return await Role.findOne({
				where: {
					id: ctx.params.id
				}
			});
		}
		if (ctx.params.name) {
			return await Role.findOne({
				where: {
					name: ctx.params.name
				}
			});
		}
		if (ctx.params.user_id) {
			return await Role.findAll({
				include: {
					model: UserHasRole,
					as: "userRoles",
					where: {
						user_id: ctx.params.user_id
					}
				}
			});
		}
	}
};

module.exports = resolveRole;
