const RoleHasPermission = require(process.cwd() + "/models")
	.role_has_permission;

const resolveRolePermission = {
	params: {
		role_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		]
	},
	async handler(ctx) {
		if (ctx.params.role_id) {
			let rolePermissions = await RoleHasPermission.findAll({
				where: {
					role_id: ctx.params.role_id
				}
			});
			if(Array.isArray(ctx.params.role_id)){
				const permissionMap = {};
				rolePermissions.map(el => {
					if (!permissionMap[el.role_id]) {
						permissionMap[el.role_id] = [];
					}
					permissionMap[el.role_id].push(el);
				});
				return ctx.params.role_id.map(el => {
					return permissionMap[el] || [];
				});
			}
			return rolePermissions;
		}
	}
};

module.exports = resolveRolePermission;
