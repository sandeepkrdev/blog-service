const Session = require(process.cwd() + "/models").session;

const resolveSession = {
	params: {
		id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
		user_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
	},
	async handler(ctx) {
		let whereObj = [];
		if (ctx.params.id) {
			whereObj.push({
				id: ctx.params.id
			});
		}
		if (ctx.params.user_id) {
			whereObj.push({
				user_id: ctx.params.user_id
			});
		}
		if (Array.isArray( ctx.params.id) || Array.isArray(ctx.params.user_id)) {
			return await Session.findAll({
				where: whereObj
			});
		}
		return await Session.findOne({
			where: whereObj
		});
	}
};

module.exports = resolveSession;
