const Session = require(process.cwd() + "/models").session;
// const { MoleculerClientError } = require("moleculer").Errors;

const resolveToken = {
	params: {
		token: "string"
	},
	async handler(ctx) {
		let entity = ctx.params;
		if (entity.token) {
			const session = await Session.findOne({
				where: {
					session_id: entity.token
				}
			});
			if (!session) {
				return; // throw new MoleculerClientError("Session does not exist!", 401);
			}
			if (new Date() > session.expiresAt) {
				return; // throw new MoleculerClientError("Session expired", 401);
			}
			const user = await ctx.call("users.resolveUser", {
				id: session.user_id
			});
			return user;
		}
	}
};

module.exports = resolveToken;
