const User = require(process.cwd() + "/models").user;

const resolveUser = {
	params: {
		id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
	},
	async handler(ctx) {
		if (ctx.params.id) {
			if (!Array.isArray(ctx.params.id)) {
				return await User.findByPk(ctx.params.id);
			}
			let users = await User.findAll({
				where: {
					id: ctx.params.id
				}
			});
			const userMap = {};
			users.forEach(user => (userMap[user.id] = user));
			return ctx.params.id.map(id => userMap[id]);
		}

		return null;
	}
};

module.exports = resolveUser;
