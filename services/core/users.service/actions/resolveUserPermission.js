const { map, filter } = require("lodash");
const UserHasRole = require(process.cwd() + "/models").user_has_role;
const RoleHasPermission = require(process.cwd() + "/models")
	.role_has_permission;
const Permission = require(process.cwd() + "/models").permission;
const Role = require(process.cwd() + "/models").role;
const User = require(process.cwd() + "/models").user;

const resolveUserPermission = {
	params: {
		user_id: {
			type: "number",
			optional: true
		}
	},
	cache: {
		key: "user_id"
	},
	async handler(ctx) {
		if (ctx.params.user_id) {
			return map(
				filter(await Permission.findAll({
					include: {
						model: RoleHasPermission,
						as: "rolePermissions",
						required: true,
						include: {
							model: Role,
							as: "role",
							required: true,
							include: {
								model: UserHasRole,
								as: "userRoles",
								required: true,
								include: {
									model: User,
									as: "user",
									required: true,
									where: {
										id: ctx.params.user_id
									}
								}
							}
						}
					}
				})
				), item => {
					return {
						id: item.id,
						name: item.name
					};
				});
		}
	}
};

module.exports = resolveUserPermission;
