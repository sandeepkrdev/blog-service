const UserHasRole = require(process.cwd() + "/models").user_has_role;

const resolveUserRole = {
	params: {
		user_id: [{
			type: "number",
			optional: true
		},{ type: "array", items: "number", optional: true }],
		role_id: [
			{ type: "number", optional: true },
			{ type: "array", items: "number", optional: true }
		],
	},
	async handler(ctx) {
		if (ctx.params.user_id) {
			let userHasRoles = await UserHasRole.findAll({
				where: {
					user_id: ctx.params.user_id
				}
			});
			if(Array.isArray(ctx.params.user_id)){
				const roleMap = {};
				userHasRoles.map(el => {
					if (!roleMap[el.user_id]) {
						roleMap[el.user_id] = [];
					}
					roleMap[el.user_id].push(el);
				});
				return ctx.params.user_id.map(el => {
					return roleMap[el] || [];
				});
			}
			return userHasRoles;
		}
		if (ctx.params.role_id) {
			return await UserHasRole.findAll({
				where: {
					role_id: ctx.params.role_id
				}
			});
		}
	}
};

module.exports = resolveUserRole;
