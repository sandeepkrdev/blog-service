const { MoleculerClientError } = require("moleculer").Errors;

const updateRole = {
	graphql: {
		mutation: "updateRole(id: Int!, name: String!): Role!"
	},
	async handler(ctx) {
		try {
			await ctx.call("users.hasPermission", {
				permissionName: "roles-update"
			});
			const role = await ctx.call("users.resolveRole", {
				id: ctx.params.id
			});
			if (!role) {
				throw new MoleculerClientError("Role not found", 400, "id");
			}
			const roleExists = await ctx.call("users.resolveRole", {
				name: ctx.params.name
			});
			if (roleExists && roleExists.id != role.id) {
				throw new MoleculerClientError("Role with this name already exists", 400, "name");
			}
			return await role.update({
				name: ctx.params.name
			});
		} catch (e) {
			return Promise.reject(e);
		}
	}
};

module.exports = updateRole;
