const bcrypt = require("bcrypt");
const { isEmpty } = require("lodash");
const { config } = require(process.cwd() + "/utils/env");
const { MoleculerClientError } = require("moleculer").Errors;

async function validatePassword(ctx, user) {
	if (!ctx.params.input.password) {
		throw new MoleculerClientError(
			"Password is required",
			400,
			"password"
		);
	}
	const authEmail = await ctx.call("auth.resolveEmail", {
		user_id: user.id
	});
	if (!authEmail) {
		throw new MoleculerClientError(
			`Couldn't find your ${config("app.name")} Account`,
			400,
			"password"
		);
	}

	if (!authEmail.password) {
		throw new MoleculerClientError(
			"Password is not set for this email, please generate new password.",
			400,
			"password"
		);
	}

	if (
		!bcrypt.compareSync(ctx.params.input.password, authEmail.password)
	) {
		throw new MoleculerClientError(
			"Wrong password. Try again or click Forgot password to reset it.",
			400,
			"password"
		);
	}
}

const updateUser = {
	graphql: {
		mutation: "updateUser(id:Int!, input: UserUpdateInput!) : User"
	},
	async handler(ctx) {
		const user = await ctx.call("users.resolveUser", {
			id: ctx.params.id
		});
		if (!user) {
			throw new MoleculerClientError("User not found!", 400, "id");
		}

		if (ctx.meta && ctx.meta.user) {
			try {
				await ctx.call("users.hasPermission", {
					permissionName: "manage-users"
				});
			} catch (e) {
				if (ctx.meta.user.id !== user.id) {
					throw new MoleculerClientError(
						"Unauthorized Access!",
						403,
						"user_id"
					);
				}
				await validatePassword(ctx, user);
			}
		}

		if (ctx.params.input.name && this.validateName(ctx.params.input.name)) {
			throw new MoleculerClientError(
				"Name cannot contain special characters",
				400,
				"name"
			);
		}
		if (!isEmpty(ctx.params.input)) {
			await user.update(ctx.params.input);
		}
		return user;
	}
};

module.exports = updateUser;
