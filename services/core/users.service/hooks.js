module.exports = {
	before: {
		logout: ["checkIsAuthenticated"],
		me: ["checkIsAuthenticated"],
		updateUser: ["checkIsAuthenticated"],
		getUsers: ["checkIsAuthenticated"],
		createRole: ["checkIsAuthenticated"],
		updateRole: ["checkIsAuthenticated"],
		deleteRole: ["checkIsAuthenticated"],
		getRoles: ["checkIsAuthenticated"],
		getPermissions: ["checkIsAuthenticated"],
		createRolePermission: ["checkIsAuthenticated"],
		deleteRolePermission: ["checkIsAuthenticated"],
		createUserRole: ["checkIsAuthenticated"],
		deleteUserRole: ["checkIsAuthenticated"],
	}
};
