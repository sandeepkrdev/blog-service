"use strict";

const hooks = require("./hooks");
const {loadFiles} = require(process.cwd() + "/utils/env");
const methods = require("./methods");
const settings = require("./settings");
const AuthMixin = require(process.cwd() + "/mixins/auth.mixin");

module.exports = {
	// Define service name
	name: "users",
	mixins: [AuthMixin],
	hooks: hooks,
	settings: settings,
	actions: loadFiles(__dirname + "/actions"),
	methods: methods,
};
