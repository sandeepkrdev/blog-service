const crypto = require("crypto");
const moment = require("moment");
const { MODELS } = require(process.cwd() + "/constants");
const Session = require(process.cwd() + "/models").session;
const User = require(process.cwd() + "/models").user;
const Auth = require(process.cwd() + "/models")[MODELS.AUTH];
const { MoleculerClientError } = require("moleculer").Errors;
const apiMethods = require(process.cwd() + "/services/gateway/api.service/methods");
const sessionRedis = require(process.cwd() + "/utils/sessionRedis");

module.exports = {
	...apiMethods,
	/**
	 * Generate a token from user entity
	 *
	 * @param {Object} user
	 */
	async generateToken(user) {
		const found = await Session.findOne({
			where: {
				user_id: user.id
			}
		});

		if (found) {
			if (new Date() < found.expiresAt) {
				return found;
			}
			await found.destroy();
		}

		return await Session.create({
			user_id: user.id,
			createdAt: new Date(),
			session_id: crypto.randomBytes(30).toString("hex"),
			expiresAt: moment().add(30, "day"),
		});
	},

	/**
	 * Transform returned user entity. Generate token if neccessary.
	 *
	 * @param {Object} user
	 */
	async getUserWithToken(user) {
		let session;
		if (user) {
			session = await this.generateToken(user);
		}

		sessionRedis.build(this.broker, user.id);

		return { user: user, session: session };
	},

	async getUser(ctx, ref_type, ref_id) {
		//Get user.
		const _user = await User.findOne({
			include: {
				as: "userAuths",
				model: Auth,
				where: {
					ref_type: ref_type,
					ref_id: ref_id
				}
			}
		});
		if (!_user) {
			throw new MoleculerClientError("Invalid user id", 401);
		}
		return _user;
	},

	generateOTP() {
		return Math.floor((1 + Math.random() * 9) * 100000).toString();
	},

	validateName(name) {
		return !/^[A-Za-z0-9 ]*$/i.test(name) ? true : false;
	},
};
