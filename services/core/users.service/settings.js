module.exports = {
	/** Public fields */
	fields: ["id", "name"],
	graphql: {
		type: `
            type User {
                id: Int!
                name: String!
				email: String
				createdAt: Date
				updatedAt: Date
				userRoles: [UserRole]
				permissions: [Permission]
            }
			type UserWithToken {
                user: User
                session: Session
			}
			type Session {
				session_id: String
				expiresAt: String
			}
			type Role {
                id: Int!
				name: String!
				rolePermissions: [RolePermission]
            }
            type Permission {
                id: Int!
				name: String!
			}
			type UserRole {
                id: Int!
				user: User
				role: Role
			}
			type RolePermission {
                id: Int!
				role: Role
				permission: Permission
			}
			type UserDataOutput {
				data: [User!]
				meta: ResponseMetaOutput
			}
			type RoleDataOutput {
				data: [Role!]
				meta: ResponseMetaOutput
			}
			type PermissionDataOutput {
				data: [Permission!]
				meta: ResponseMetaOutput
			}
			type ResponseMetaOutput {
				total: Int
				per_page: Int
				page_no: Int
				next_data: Boolean
			}
			input RegisterUserInput{
				email: String!
				password: String!
				name: String!
			}
			input UserInput {
				name: String!
				email: String!
			}
            input UserUpdateInput {
				name: String
			}
			input UserFilters {
				id: [Int]
				search: String
				role_id_not_null: Boolean
				per_page: Int
				page_no: Int
				orderBy: UserOrderByInput
			}
			input RoleFilters {
				search: String
				per_page: Int
				page_no: Int
			}
			input PermissionFilters {
				search: String
				per_page: Int
				page_no: Int
			}
			input UserOrderByInput {
				id: OrderByEnum
                name: OrderByEnum
				createdAt: OrderByEnum
            }
        `,
		resolvers: {
			User: {
				userRoles: {
					action: "users.resolveUserRole",
					dataLoader: true,
					rootParams: {
						id: "user_id"
					}
				},
				permissions: {
					action: "users.resolveUserPermission",
					rootParams: {
						id: "user_id"
					}
				},
			},
			Role: {
				rolePermissions: {
					action: "users.resolveRolePermission",
					dataLoader: true,
					rootParams: {
						id: "role_id"
					}
				}
			},
			UserRole: {
				user: {
					action: "users.resolveUser",
					dataLoader: true,
					rootParams: {
						user_id: "id"
					}
				},
				role: {
					action: "users.resolveRole",
					dataLoader: true,
					rootParams: {
						role_id: "id"
					}
				}
			},
			RolePermission: {
				role: {
					action: "users.resolveRole",
					dataLoader: true,
					rootParams: {
						role_id: "id"
					}
				},
				permission: {
					action: "users.resolvePermission",
					dataLoader: true,
					rootParams: {
						permission_id: "id"
					}
				}
			}
		}
	}
};
