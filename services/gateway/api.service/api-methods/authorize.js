/* eslint-disable no-unused-vars */
const { config } = require(process.cwd() + "/utils/env");
const sessionRedis = require(process.cwd() + "/utils/sessionRedis");
/**
	* Authorize the request
	*
	* @param {Context} ctx
	* @param {Object} route
	* @param {IncomingRequest} req
	* @returns {Promise}
*/
async function authorize(ctx, route, req) {
	let token;
	if (ctx.meta.authorization) {
		let type = ctx.meta.authorization.split(" ")[0];
		if (type === "Token" || type === "Bearer")
			token = ctx.meta.authorization.split(" ")[1];
	}

	if (token === config("app.elastic_secret")) {
		ctx.meta.is_internal = true;
	}

	if (token) {
		let session = await sessionRedis.get(this.broker, token);
		if (session) {
			ctx.meta.user = session.user;
			ctx.meta.session = session;
			ctx.meta.token = token;
		} else {
			ctx.meta.user = null;
			ctx.meta.session = null;
		}
	} else {
		ctx.meta.token = null;
	}
}

module.exports = authorize;
