"use strict";

const ApiGateway = require("moleculer-web");
const settings = require("./settings");
const methods = require("./methods");
const ApolloMixin = require(process.cwd() + "/mixins/apollo.mixin");
const { ApolloService } = require("moleculer-apollo-server");

/**
 * @typedef {import('moleculer').ServiceSchema} ServiceSchema Moleculer's Service Schema
 * @typedef {import('moleculer').Context} Context Moleculer's Context
 * @typedef {import('http').IncomingMessage} IncomingRequest Incoming HTTP Request
 * @typedef {import('http').ServerResponse} ServerResponse HTTP Server Response
 * @typedef {import('moleculer-web').ApiSettingsSchema} ApiSettingsSchema API Setting Schema
 */

module.exports = {
	name: "api",
	mixins: [ApiGateway, ApolloService(ApolloMixin)],
	settings: settings,
	methods: methods
};

