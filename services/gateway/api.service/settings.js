const {
	loadFiles
} = require(process.cwd() + "/utils/env");
const routes = loadFiles(__dirname + "/routes", true);

module.exports = {
	// Exposed port
	port: process.env.PORT || 3000,

	// Exposed IP
	ip: "0.0.0.0",

	// Global CORS settings for all routes
	cors: {
		// Configures the Access-Control-Allow-Origin CORS header.
		origin: "*",
		// Configures the Access-Control-Allow-Methods CORS header.
		methods: ["GET", "OPTIONS", "POST", "PUT", "DELETE"],
		// Configures the Access-Control-Allow-Headers CORS header.

		allowedHeaders: [
			"Access-Control-Allow-Origin",
			"Access-Control-Allow-Headers",
			"Origin",
			"Accept",
			"X-Requested-With",
			"Content-Type",
			"Access-Control-Request-Method",
			"Access-Control-Request-Headers",
			"authorization",
			"client-security-token",
			"X-Custom-Header",
			"X-Response-Time",
			"X-Rate-Limiting"
		],
		// Configures the Access-Control-Expose-Headers CORS header.
		exposedHeaders: [
			"Access-Control-Allow-Headers",
			"Origin",
			"Accept",
			"X-Requested-With",
			"Content-Type",
			"Access-Control-Request-Method",
			"Access-Control-Request-Headers",
			"authorization",
			"client-security-token",
			"X-Custom-Header",
			"X-Response-Time"
		],
		// Configures the Access-Control-Allow-Credentials CORS header.
		credentials: true,
		// Configures the Access-Control-Max-Age CORS header.
		maxAge: 3600
	},

	// Global Express middlewares. More info: https://moleculer.services/docs/0.14/moleculer-web.html#Middlewares
	use: [],

	routes: routes,

	// Do not log client side errors (does not log an error response when the error.code is 400<=X<500)
	log4XXResponses: false,
	// Logging the request parameters. Set to any log level to enable it. E.g. "info"
	logRequestParams: null,
	// Logging the response data. Set to any log level to enable it. E.g. "info"
	logResponseData: null,


	// Serve assets from "public" folder. More info: https://moleculer.services/docs/0.14/moleculer-web.html#Serve-static-files
	assets: {
		folder: "public",

		// Options to `server-static` module
		options: {}
	}
};
