"use strict";
const Auth = require(process.cwd() + "/models").auth;
const Role = require(process.cwd() + "/models").role;
const AuthEmail = require(process.cwd() + "/models").auth_email;
const AuthPhone = require(process.cwd() + "/models").auth_phone;
const Permission = require(process.cwd() + "/models").permission;
const UserHasRole = require(process.cwd() + "/models").user_has_role;
const RoleHasPermission = require(process.cwd() + "/models").role_has_permission;
const { MoleculerClientError } = require("moleculer").Errors;

let role;
(async () => {
	try {
		role = await Role.findOne({
			name: "Super-Admin"
		});
		if (!role) {
			role = await Role.create({
				name: "Super-Admin"
			});
		}
		if (!role) throw new MoleculerClientError("Super Admin role not found.", 400, "name");
		const permissions = await Permission.findAll();
		for (let i = 0; i < permissions.length; i++) {
			await createRolePermission(role.id, permissions[i].dataValues.id);
		}
		if (!(process.env.npm_config_email || process.env.npm_config_phone))
			throw new MoleculerClientError("Please give Email or Phone to continue.", 400, "name");
		if (process.env.npm_config_email) {
			await assignSuperAdminEmail(process.env.npm_config_email);
		}
		if (process.env.npm_config_phone) {
			await assignSuperAdminPhone(process.env.npm_config_phone);
		}
	} catch (error) {
		console.log("\x1b[35m");
		console.log("\x1b[33mError:\x1b[31m", error.message, "\n\x1b[0m");
	}
	process.exit();
})();

async function assignSuperAdminEmail(email) {
	email = process.env.npm_config_email.trim();
	const authEmail = await AuthEmail.findOne({
		where: {
			email: email
		}
	});
	if (!authEmail) throw new MoleculerClientError("Email not found.", 400, "email");
	const auth = await getAuth("auth_email", authEmail.id);
	if (!auth) throw new MoleculerClientError("Email not found.", 400, "email");
	await createUserRole(auth.user_id, role.id);
	showSuccessMessage("Email", email);
}

async function assignSuperAdminPhone(phone) {
	phone = process.env.npm_config_phone.trim();
	const authPhone = await AuthPhone.findOne({
		where: {
			phone: phone
		}
	});
	if (!authPhone) throw new MoleculerClientError("Phone not found.", 400, "phone");
	const auth = await getAuth("auth_phone", authPhone.id);
	if (!auth) throw new MoleculerClientError("Phone not found.", 400, "phone");
	await createUserRole(auth.user_id, role.id);
	showSuccessMessage("Phone", phone);
}

async function getAuth(ref_type, ref_id) {
	return await Auth.findOne({
		where: {
			ref_type: ref_type,
			ref_id: ref_id
		}
	});
}

async function createUserRole(userId, roleId) {
	return await UserHasRole.create(
		{
			user_id: userId,
			role_id: roleId
		},
		{ updateOnDuplicate: ["user_id", "role_id"] }
	);
}

async function createRolePermission(roleId, permissionId) {
	return await RoleHasPermission.create(
		{
			role_id: roleId,
			permission_id: permissionId
		},
		{ updateOnDuplicate: ["role_id", "permission_id"] }
	);
}

function showSuccessMessage(key, value) {
	console.log(
		"\x1b[32m%s\x1b[0m",
		"Created Super Admin -",
		"\x1b[33m" + key + ":",
		value,
		"\n\x1b[0m"
	);
}
