const fs = require("fs");
const _ = require("lodash");
const path = require("path");
const ENV = require(process.cwd() + "/config/env.json");

const base_path = (dir="") => {
	return path.resolve(process.cwd())+ (dir ? "/" + dir : "");
};

const migration_path = (dir="") => {
	return base_path("/migrations" + (dir ? "/" + dir : ""));
};

const model_path = (dir="") => {
	return base_path("/models" + (dir ? "/" + dir : ""));
};

const public_path = (dir="") => {
	return base_path("/public" + (dir ? "/" + dir : ""));
};

const config = (key) => {
	return _.get(ENV, key) === undefined ? null : _.get(ENV, key);
};

const loadFiles = (dir="", array=false) => {
	const files = array ? [] : {};
	fs.readdirSync(dir).forEach(function(file) {
		const fileName = file.substr(0, file.length - 3);
		if (fileName != "index") {
			const req = require(dir +"/" + file);
			if (array) {
				files.push(req);
			} else {
				files[fileName] = req;
			}
		}
	});
	return files;
};

const getFileNames = (dir="", array=false) => {
	const files = array ? [] : {};
	fs.readdirSync(dir).forEach(function(file) {
		const fileName = file.substr(0, file.length - 3);
		if (fileName != "index") {
			if (array) {
				files.push(fileName);
			} else {
				files[fileName] = fileName;
			}
		}
	});
	return files;
};

module.exports = {
	base_path,
	migration_path,
	model_path,
	public_path,
	config,
	loadFiles,
	getFileNames
};