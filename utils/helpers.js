const { loadFiles } = require(process.cwd() + "/utils/env");

const User = require(process.cwd() + "/models").user;

module.exports = {
	hasPermission: async function (ctx, permissionName, userId) {
		const user_id = userId ? userId : ctx.meta.user.id;
		const user = await User.findOne({
			where: {
				id: user_id
			},
			attributes: ["userRoles.role.rolePermissions.permission.name"],
			include: [
				{
					all: true,
					nested: true
				}
			]
		});

		const permissions = await user.userRoles.map(function (userRole) {
			return userRole.role.rolePermissions.map(function (rolePermission) {
				return rolePermission.permission.name;
			});
		});

		if (!permissions[0]) return false;
		return permissions[0].includes(permissionName);
	},
	brokerBroadCast: async function (ctx, service, action, payload) {
		ctx.broker.broadcast(`${service}.${action}`, payload, [
			"dynamic-actions"
		]);
	},
	getUrlHash: async function (limit = [3, 3, 3]) {
		let arr = [];
		for (let i = 0; i < 3; i++) {
			arr.push(generateRandomString(limit[i]));
		}
		return stitchHashedArray(arr);
	},
	generateRandomString: generateRandomString,
	stitchHashedArray: stitchHashedArray,
	...loadFiles(__dirname + "/helpers"),
};

function stitchHashedArray(arr, delim = "-") {
	return arr.join(delim);
}
function generateRandomString(length = 5) {
	const characters = "abcdefghijklmnopqrstuvwxyz";
	let randomString = "";
	for (let i = 0; i < length; i++) {
		randomString += characters[parseInt(Math.random() * characters.length)];
	}
	return randomString;
}
