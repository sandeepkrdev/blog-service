const axios = require("axios");

const isValidUrl = urlString => {
	try {
		return Boolean(new URL(urlString));
	} catch (e) {
		return false;
	}
};

async function getFileContentChecksum(type, fileContentChecksum) {
	if (
		fileContentChecksum &&
		fileContentChecksum.fileContentChecksumUrl &&
		fileContentChecksum.fileContentChecksumUrl[`${type}_url`]
	) {
		if (isValidUrl(fileContentChecksum.fileContentChecksumUrl[`${type}_url`])) {
			const data = await axios.get(fileContentChecksum.fileContentChecksumUrl[`${type}_url`]);
			return JSON.stringify(data.data);
		}
		return fileContentChecksum.fileContentChecksumUrl[`${type}_url`];
	}
	return fileContentChecksum[type];
}

module.exports = getFileContentChecksum;
