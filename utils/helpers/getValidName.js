async function getValidName(name) {
	const newName = name
		.replace(/[^a-zA-Z ]/g, "")
		.normalize("NFD")
		.replace(/[\u0300-\u036f]/g, "");
	return newName;
}

module.exports = getValidName;
