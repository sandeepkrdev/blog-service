"use strict";

const fs = require("fs");
const path = require("path");
const Handlebars = require("handlebars");
const { UTF_8 } = require(process.cwd() + "/constants/misc");
const { config } = require(process.cwd() + "/utils/env");
let partialsDir = process.cwd() + "/assets/mail-templates/partials";
let fileNames = fs.readdirSync(partialsDir);

const TEMPLATE_FOLDER_PATH = process.cwd() + "/assets/mail-templates/";
const _MESSAGES = {
	INVALID_TEMPLATE_PATH: "Invalid Template Path.",
};

module.exports = {
	getCompiledTemplate: function (templateFileName, payload) {
		payload.website_url = config("frontend-url.website.url");
		payload.ceo_name = config("app.ceo.name");
		payload.ceo_twitter_url = config("app.ceo.twitter_url");
		payload.ceo_twitter_handle = config("app.ceo.twitter_handle");
		payload.app_name = config("app.name");
		payload.app_website = config("app.website");
		return new Promise((resolve, reject) => {
			const templatePath = path.join(
				TEMPLATE_FOLDER_PATH + templateFileName
			);
			//Check if the given template exists in the template folder.
			if (!fs.existsSync(templatePath))
				return reject(_MESSAGES.INVALID_TEMPLATE_PATH);

			const handlebarsTemplate = fs.readFileSync(templatePath, UTF_8);
			fileNames.forEach(function (fileName) {
				let name = fileName.split(".")[0];
				let template = fs.readFileSync(partialsDir + "/" + fileName, "utf8");
				Handlebars.registerPartial(name, template);
			});
			Handlebars.registerHelper("switch", function(value, options) {
				this.switch_value = value;
				return options.fn(this);
			});
			Handlebars.registerHelper("case", function(value, options) {
				if (value == this.switch_value) {
					return options.fn(this);
				}
			});
			Handlebars.registerHelper("ifEquals", function (arg1, arg2, options) {
				return arg1 == arg2 ? options.fn(this) : options.inverse(this);
			});
			const compiledHandlebarsTemplate = Handlebars.compile(
				handlebarsTemplate
			);
			const compiledTemplate = compiledHandlebarsTemplate(payload);

			return resolve(compiledTemplate);
		});
	},
	getCustomCompiledTemplate: function (template, payload) {
		return new Promise((resolve, reject) => {
			if (!template) {
				return reject("Template does not exist!");
			}
			Handlebars.registerHelper("switch", function (value, options) {
				this.switch_value = value;
				return options.fn(this);
			});
			Handlebars.registerHelper("case", function (value, options) {
				if (value == this.switch_value) {
					return options.fn(this);
				}
			});
			Handlebars.registerHelper(
				"ifEquals",
				function (arg1, arg2, options) {
					return arg1 == arg2
						? options.fn(this)
						: options.inverse(this);
				}
			);
			const compiledHandlebarsTemplate = Handlebars.compile(template);
			const compiledTemplate = compiledHandlebarsTemplate(payload);
			return resolve(compiledTemplate);
		});
	}

};
