const Minio = require("minio");
const { config } = require(process.cwd() + "/utils/env");

const minioClient = (key) => {
	return new Minio.Client({
		endPoint: config(`file-service.minio.${key}.endPoint`),
		port: config(`file-service.minio.${key}.port`),
		useSSL: config(`file-service.minio.${key}.use_ssl`),
		accessKey: config(`file-service.minio.${key}.access_key`),
		secretKey: config(`file-service.minio.${key}.secret_key`),
	});
};

module.exports = minioClient;