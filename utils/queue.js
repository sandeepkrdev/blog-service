const Queue = require("bull");
const BeeQueue = require("bee-queue");
const RSMQ = require("rsmq");
const { config } = require(process.cwd() + "/utils/env");
const redis = require("redis");

const processVideoQueue = new Queue(config("redis.queues.process-video"), {
	redis: {
		port: config("redis.port"),
		host: config("redis.host"),
		password: config("redis.password")
	}
});

const rsmq = new RSMQ({
	port: config("redis.port"),
	host: config("redis.host"),
	password: config("redis.password")
});

const getQueueByName = function (name) {
	return new Queue(name, {
		redis: {
			port: config("redis.port"),
			host: config("redis.host"),
			password: config("redis.password")
		}
	});
};

const getBeeQueueByName = function (name) {
	return new BeeQueue(name, {
		redis: {
			port: config("redis.port"),
			host: config("redis.host"),
			password: config("redis.password")
		}
	});
};

// const redisClient = redis.createClient({
// 	port: config("redis.port"),
// 	host: config("redis.host"),
// 	password: config("redis.password")
// });

const redisClient = function () {
	let client = redis.createClient({
		port: config("redis.port"),
		host: config("redis.host"),
		password: config("redis.password")
	});

	client.on("error", function (err) {
		console.log("Redis error: " + err);
	});

	return client;
};

module.exports = {
	processVideoQueue,
	rsmq,
	getQueueByName,
	getBeeQueueByName,
	redisClient
};
