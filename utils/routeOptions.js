module.exports = {
	onBeforeCall(ctx, route, req, res) {
		// Set request headers to context meta
		ctx.meta.authorization = req.headers["authorization"];
	},
};