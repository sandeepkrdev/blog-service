const _ = require("lodash");
const moment = require("moment");

async function getSession($broker, userId) {
	return await $broker.call("users.resolveSession", {
		user_id: userId
	});
}

function sessionIsExpired(session) {
	return new Date() > session.expiresAt;
}

async function getUser($broker, userId) {
	return await $broker.call("users.resolveUser", {
		id: userId
	});
}

async function getPermissionNames($broker, userId) {
	const permissions = await $broker.call("users.resolveUserPermission", {
		user_id: userId
	});
	return _.map(permissions, "name");
}

async function getSessionData($this, $broker, userId) {
	//session fetch and check
	const session = await getSession($broker, userId);
	if (!session) return null;
	if (sessionIsExpired(session)) {
		$this.remove(session.session_id);
		return null;
	}

	//user fetch and check
	const user = await getUser($broker, userId);
	if (!user) return null;

	//permissions fetch and check
	const permissionNames = await getPermissionNames($broker, userId);

	return {
		key: session.session_id,
		session: {
			user: user ? JSON.parse(JSON.stringify(user)) : null,
			permissions: permissionNames,
			createdAt: moment().unix()
		}
	};
}

async function getSessionByKeyOrBuild($this, $broker, key) {
	let session = await $broker.cacher.get($this.prefixed(key));
	return new Promise((resolve, reject) => {
		if (session) {
			//ALREADY BUILT:: session
			return resolve(session);
		}
		return $broker.call("users.resolveToken", {
			token: key
		}).then(
			user => {
				if (!user) return resolve(null);
				//BUILD:: session
				return $this.build($broker, user.id).then(
					ses => {
						return resolve(ses);
					});
			});
	});
}

class SessionRedis {
	constructor() {
		//
		this.prefix = "user_session_";
	}

	build($broker, userId) {
		const $this = this;
		return new Promise((resolve, reject) => {
			getSessionData($this, $broker, userId)
				.then(object => {
					if (!object) return resolve(null);
					$this.set($broker, object.key, object.session);
					return resolve(object.session);
				});
		});
	}

	get($broker, key) {
		const $this = this;
		return new Promise((resolve, reject) => {
			getSessionByKeyOrBuild($this, $broker, key)
				.then(session => {
					resolve(session);
				});
		});
	}

	//timeout in seconds
	set($broker, key, value, timeout = 15 * 60 * 60) {
		if ($broker.cacher)
			$broker.cacher.set(this.prefixed(key), value, timeout);
	}

	remove($broker, key) {
		if ($broker.cacher)
			$broker.cacher.del(this.prefixed(key));
	}

	prefixed(key) {
		return this.prefix + key;
	}
}

const sessionRedis = new SessionRedis();

module.exports = sessionRedis;
